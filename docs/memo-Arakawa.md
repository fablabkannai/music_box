# 荒川メモ

荒川のメモです

# Web操作画面

## インストール方法
1. gitをclone or pullして最新のソースコードを入手

初回のみ

> git clone git@gitlab.com:fablabkannai/music_box.git

更新時

> git pull

2. 必要なライブラリをインストール
> cd music_box/docs/code

> pip install -r requirements.txt

## 使い方
1. music_box/docs/codeで以下を実行

> ./run.sh

2. ブラウザで以下のURLを開く

http://(ラズパイのIP):8080/

以下の画面が表示される

![](img/web_page1.png)

3. midiファイルのアップロード

Webページ下部Upload new midi fileの下にある、ファイルを選択ボタンをクリックしてmidiファイルを選択、uploadボタンを押すとアップロードができる。

Select midifile以下にアップロードしたファイルが追加される。

4. midiファイルの演奏

midiファイルの隣にあるPLAYボタンをクリックすると演奏開始、STOPボタンを押すと演奏が停止する。

演奏が始まると下の鍵盤の対応する場所が光る

5. 音階の調整

音階が高すぎたり低過ぎたりすると鍵盤で表示されている音階をはみ出してしまう。
このような時は、OCTAVE UP(音階を上げる)ボタン、あるいは、OCTAVE DOWN(音階を下げる)ボタンで調整する。

6. スピードの調整

再生スピードを調整するには。
SPEED UPボタン、あるいは、SPEED DOWNボタンで調整する。
一回押すごとに20％加速、あるいは減速する。

7. オルゴールモーターの開始と停止

Webページにボタンは配置したが未接続。
今後のTODO

8. midiファイルの消去

midiファイルの隣にあるDELETEボタンをクリックするとmidiファイルを削除する。

9. 鍵盤キー
鍵盤キーを押すとその位置の音を鳴らすことができる

10. pianohat
ラズパイがわにpianohatが搭載されていれば、pianohatの鍵盤で音を鳴らすことも可能

## 技術的なこと
1. 構成要素
- Webサーバ 操作用のWebページを表示する為に使用、ボタンを押すとWebSocketにコマンドを送る
- WebSocketサーバ 操作用Webページから受け取った各種コマンド、pianohatから受け取ったコマンドをクライアントにブロードキャストする MQTTのブローカのような役目
-- ws://(IP):8080/midi 操作用Webページからmidifile制御用コマンドを受信、クライアントにブロードキャスト
-- ws://(IP):8080/musicbox 音階番号を受信、クライアントにブロードキャスト
- midi2musicbox.py midifileをパースして音階情報に変換する websocetクライアントとして動作する
- musicbox.py 音階情報をもとにオルゴールを鳴らす websocketクライアントとして動作する
- musicbox_dummy.py 上のダミー。オルゴールを鳴らす代わりにwaveファイルを再生する
- piano_hat.py pianohatの鍵盤を使って音階を入力する
- run.sh 全部一括で起動


2. Webサーバ app.py

今回は、pythonのtornadoライブラリを使用した。同じことをnode-redで実装することも可能。

起動するには以下を実行

> python app.py

midiファイルの管理はsqliteでライブラリを使用。webページからのuploadやdeleteの要求を受けてデータベースを更新する部分はpython tornadoで実装。

midiファイル再生、停止、音階調整、速度調整、モーター制御のコマンドは、json形式とした。

var json_data = {
    cmd: 'play',
    opt: 'midifile.mid'
}

cmdに入るコマンドは以下の通り

- cmd: 'play' midiファイル再生 このコマンドだけoptにmidiファイル名を入れる必要あり
- cmd: 'stop' midiファイル停止
- cmd: 'octave_up' 音階を１オクターブ上げる
- cmd: 'octaeve_down' 音階を１オクターブ下げる
- cmd: 'speed_up' 20％スピードアップ
- cmd: 'speed_down' 20%スピードダウン
- cmd: 'start_motor' オルゴールモータの開始
- cmd: 'stop_motor' オルゴールモーターの停止

3. WebSocketサーバ

こちらもpythonのtornadoライブラリを使用。tornadoを使うことで、WebサーバとWebSocketサーバを一つのソースファイルで管理できる。

受けたメッセージをそのままクライアントにブロードキャストしているだけ

4. midi2musicbox.py

ws://(IP):8080/midi を監視してmidiファイルの各種制御を行う。

制御コマンドはjson形式で与えられるので、パースして実行する。

midiファイル自体のパースは本間さんのコードを流用。

音階番号(0から24の数）に変換してws://(IP):8080/musicbox に送信する。

１オクターブあたり12音階（半音を除くと7音階)、今回作るオルゴールは2音階+１音なので25音階。

半音は除いても良かったのだが、pianohatとの整合性を維持する為、あて12音階全てを使う仕様とした。

5. musicbox.py

ws://(IP):8080/musicbox を監視してオルゴールを鳴らす

gpioは適当なので修正する必要あり

音階番号が0から24に収まっていなと変なことになるので、ここも修正する必要あり

6. musicbox_dummy.py

ws://(IP):8080/musicbox を監視してwaveファイルを再生する

起動時にHDMIケーブルに繋いでいるとHDMI経由で音を鳴らそうとするので注意

7. piano_hat.py

pianohatの鍵盤を使う為のスクリプト。pianohatのexamples以下にあったソースを修正したもの

8. run.sh

毎回上記スクリプトを一つずつ起動するのが面倒なので、一つにまとめたもの







# ペーパーオルゴールの仕組み
１５ノートペーパーオルゴールを分解して仕組みを解析

https://www.amazon.co.jp/gp/product/B082MQHYZF/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1

ペーパーオルゴールは、穴のあいた用紙に音階が記載されている。ハンドルを手で回すことでこの用紙が進み、櫛歯を弾く事で音が鳴る

![](img/paper_musicbox1.png)

ハンドルを回す事で与えられた回転は、ギアボックスを通じて、用紙送りローラーと爪戻し用ローラーの回転に変換される

![](img/paper_musicbox2.png)

爪は用紙に穴が空いていると引っかかって櫛歯を弾く。爪戻し用ローラーは爪が元の位置に戻す為に使われている

![](img/paper_musicbox3.png)

![](img/paper_musicbox4.png)

# ステッピングモータでオルゴールを回す
手動でハンドルを回す代わりに、ステッピングモーター28YJ-48を使って爪戻し用ローラーを直接回す

ステッピングモーターは以下のものを使用

https://www.amazon.co.jp/Ren-He-%E3%82%B9%E3%83%86%E3%83%83%E3%83%94%E3%83%B3%E3%82%B0%E3%83%A2%E3%83%BC%E3%82%BF-28BYJ-48-ULN2003%E3%83%89%E3%83%A9%E3%82%A4%E3%83%90%E3%83%BC%E3%83%9C%E3%83%BC%E3%83%89/dp/B07LBRNZB6/ref=pd_lpo_328_t_2/358-8326365-8337603?_encoding=UTF8&pd_rd_i=B07LBRNZB6&pd_rd_r=c79bd7df-8a34-495e-9abd-9e58c9b6e03d&pd_rd_w=0T0Mw&pd_rd_wg=K5ih3&pf_rd_p=4b55d259-ebf0-4306-905a-7762d1b93740&pf_rd_r=DMZPCHC342B0W9EEP2SE&psc=1&refRID=DMZPCHC342B0W9EEP2SE

![](img/gearbox.png)

ギアボックスを置き換える部品は、STLデータを用いて3Dプリンタで作成



爪戻し用ローラーとステッピングモーターの連結には以下のSTLデータを使用







