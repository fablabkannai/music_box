Eagle Library

ref.[ultralibrarian ImportGuide](https://app.ultralibrarian.com/content/help/?eagle.htm)

|Parts|Library|Device|site|
|--|--|--|---|
|RaspberryPi 2x20 header|[raspberrypi_bastelstube_v13.lbr](http://eagle.autodesk.com/eagle/libraries?utf8=%E2%9C%93&q%5Btitle_or_author_or_description_cont%5D=raspberrypi_bastelstube_v13&button=)|RASPI_BOARD_B+_F (RASPI_BOARD_B+)|Autodesk|
|8 Channel Capacitive Touch Sensor(Cap1188)|[CAP1188-1-CP-TR](https://app.ultralibrarian.com/details/e20db406-c6b2-11e9-ab3a-0a3560a4cccc/Microchip/CAP1188-1-CP-TR?uid=26a9eede67a2f96e)|CAP1188-1-CP-TRQFN24_4X4MC_MCH-M (CAP1188-1-CP-TR)|ultralibrarian|
|LED|[fab.lbr](https://gitlab.fabcloud.org/pub/libraries/eagle)|LEDFAB1206 (LED)|FabAcademy|
|USB connector A|[OPL_eagle_library](https://github.com/Seeed-Studio/OPL_Eagle_Library)|USB|Seed-Studio|
|USB connector micro|[OPL_eagle_library](https://github.com/Seeed-Studio/OPL_Eagle_Library)|USB|Seed-Studio|
|USB connector micro SMD|[fab.lbr](https://gitlab.fabcloud.org/pub/libraries/eagle)|CONN_MICRO-USB_1/64 (CONN_MICRO-USB)|FabAcademy|

## Schematic
### Cap1188
ref. [Adafruit CAP1188 Breakout](https://learn.adafruit.com/adafruit-cap1188-breakout/downloads)  
![](https://cdn-learn.adafruit.com/assets/assets/000/035/807/medium800/adafruit_products_schem.png?1474305404)  
[large img](./img/cap1188_sch.png)  
[datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/00001620C.pdf)  
![](./img/cap1188_i2c.png)




下記のリンクにファイルを置きました
https://gitlab.com/jh1cdv00/jh1cdv00/uploads/de749e56d3c2ae483a86637785965871/202012301335_fablab.zip
