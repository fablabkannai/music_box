<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="10" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="00_mylib201507" urn="urn:adsk.eagle:library:9922397">
<description>for AZPR EvBoard</description>
<packages>
<package name="HOLE_32" urn="urn:adsk.eagle:footprint:9922724/1" library_version="1">
<pad name="B3,2" x="0" y="0" drill="2.8" diameter="3.81"/>
</package>
</packages>
<packages3d>
<package3d name="HOLE_32" urn="urn:adsk.eagle:package:9923011/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="HOLE_32"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:9922572/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94" font="vector">&gt;NAME</text>
<text x="343.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="344.17" y="20.32" size="2.286" layer="94" font="vector">&gt;VALUE</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="HOLE" urn="urn:adsk.eagle:symbol:9922523/1" library_version="1">
<wire x1="0.254" y1="2.032" x2="2.032" y2="0.254" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="0.254" x2="-0.254" y2="2.032" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="-0.254" x2="-0.254" y2="-2.032" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<wire x1="0.254" y1="-2.032" x2="2.032" y2="-0.254" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<circle x="0" y="0" radius="1.524" width="0.0508" layer="94"/>
<text x="2.794" y="0.5842" size="1.778" layer="95">&gt;NAME</text>
<text x="2.794" y="-2.4638" size="1.778" layer="97">&gt;VALUE</text>
<pin name="MOUNT" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:9923417/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HOLE_32" urn="urn:adsk.eagle:component:9923342/1" prefix="H" library_version="1">
<description>Mounting PAD&lt;br&gt;
3.2mm</description>
<gates>
<gate name="G$1" symbol="HOLE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HOLE_32">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9923011/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun">
<packages>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016"/>
<pad name="3" x="5.08" y="0" drill="1.016"/>
<pad name="4" x="7.62" y="0" drill="1.016"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2"/>
<pad name="3" x="7" y="0" drill="1.2"/>
<pad name="4" x="10.5" y="0" drill="1.2"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="3.3V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings.</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector">
<packages>
<package name="8P-2.54">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0" layer="39"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0" layer="39"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0" layer="39"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0" layer="39"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="0.508" y="-3.81" size="1.27" layer="27" rot="R90">&gt;value</text>
<text x="-2.667" y="10.287" size="1.27" layer="25">&gt;name</text>
<text x="0.635" y="-1.905" size="0.8128" layer="33" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-55/35MIL">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.397" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.397"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.397"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.397"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.397"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.397"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.397"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.397"/>
<text x="-1.778" y="10.287" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-2.413" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0" y="-0.635" size="0.3048" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-80/40MIL">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="1.016" diameter="2.032" shape="square"/>
<pad name="2" x="0" y="6.35" drill="1.016" diameter="2.032"/>
<pad name="3" x="0" y="3.81" drill="1.016" diameter="2.032"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="2.032"/>
<pad name="5" x="0" y="-1.27" drill="1.016" diameter="2.032"/>
<pad name="6" x="0" y="-3.81" drill="1.016" diameter="2.032"/>
<pad name="7" x="0" y="-6.35" drill="1.016" diameter="2.032"/>
<pad name="8" x="0" y="-8.89" drill="1.016" diameter="2.032"/>
<text x="-3.683" y="10.414" size="1.778" layer="25">&gt;name</text>
<text x="0.508" y="-3.81" size="1.27" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0" y="-0.635" size="0.254" layer="33" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-SMD">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="1.27" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="3" x="1.27" y="3.81" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="2" x="-1.27" y="6.35" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="1" x="1.27" y="8.89" dx="3.048" dy="1.524" layer="1"/>
<smd name="5" x="1.27" y="-1.27" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="6" x="-1.27" y="-3.81" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="7" x="1.27" y="-6.35" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<smd name="8" x="-1.27" y="-8.89" dx="3.048" dy="1.524" layer="1" roundness="50"/>
<text x="-1.905" y="10.16" size="1.27" layer="25" ratio="10">&gt;name</text>
<text x="0" y="-1.27" size="0.889" layer="33" ratio="12" rot="R90">&gt;name</text>
<text x="0.889" y="-4.826" size="0.889" layer="27" ratio="10" rot="R90">&gt;value</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="8P-2.54-FEMALE-D90">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="-1.27" y="10.414" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-2.159" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0.254" y="-2.032" size="0.8128" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="10.16"/>
<vertex x="9.525" y="10.16" curve="-90"/>
<vertex x="10.16" y="9.525"/>
<vertex x="10.16" y="-9.525" curve="-90"/>
<vertex x="9.525" y="-10.16"/>
<vertex x="1.27" y="-10.16"/>
</polygon>
</package>
<package name="8P-2.54-MALE-D90">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.905" y1="8.89" x2="10.16" y2="8.89" width="0.6096" layer="21"/>
<wire x1="1.905" y1="6.35" x2="10.16" y2="6.35" width="0.6096" layer="21"/>
<wire x1="1.905" y1="3.81" x2="10.16" y2="3.81" width="0.6096" layer="21"/>
<wire x1="1.905" y1="1.27" x2="10.16" y2="1.27" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="10.16" y2="-1.27" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="10.16" y2="-3.81" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="10.16" y2="-6.35" width="0.6096" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="10.16" y2="-8.89" width="0.6096" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="-1.27" y="10.287" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-2.413" size="0.8128" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0.254" y="-2.032" size="0.8128" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="9.525"/>
<vertex x="1.905" y="10.16"/>
<vertex x="3.175" y="10.16"/>
<vertex x="3.81" y="9.525"/>
<vertex x="3.81" y="8.255"/>
<vertex x="3.175" y="7.62"/>
<vertex x="1.905" y="7.62"/>
<vertex x="1.27" y="8.255"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="6.985"/>
<vertex x="1.905" y="7.62"/>
<vertex x="3.175" y="7.62"/>
<vertex x="3.81" y="6.985"/>
<vertex x="3.81" y="5.715"/>
<vertex x="3.175" y="5.08"/>
<vertex x="1.905" y="5.08"/>
<vertex x="1.27" y="5.715"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="4.445"/>
<vertex x="1.905" y="5.08"/>
<vertex x="3.175" y="5.08"/>
<vertex x="3.81" y="4.445"/>
<vertex x="3.81" y="3.175"/>
<vertex x="3.175" y="2.54"/>
<vertex x="1.905" y="2.54"/>
<vertex x="1.27" y="3.175"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="1.905"/>
<vertex x="1.905" y="2.54"/>
<vertex x="3.175" y="2.54"/>
<vertex x="3.81" y="1.905"/>
<vertex x="3.81" y="0.635"/>
<vertex x="3.175" y="0"/>
<vertex x="1.905" y="0"/>
<vertex x="1.27" y="0.635"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-0.635"/>
<vertex x="1.905" y="0"/>
<vertex x="3.175" y="0"/>
<vertex x="3.81" y="-0.635"/>
<vertex x="3.81" y="-1.905"/>
<vertex x="3.175" y="-2.54"/>
<vertex x="1.905" y="-2.54"/>
<vertex x="1.27" y="-1.905"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-3.175"/>
<vertex x="1.905" y="-2.54"/>
<vertex x="3.175" y="-2.54"/>
<vertex x="3.81" y="-3.175"/>
<vertex x="3.81" y="-4.445"/>
<vertex x="3.175" y="-5.08"/>
<vertex x="1.905" y="-5.08"/>
<vertex x="1.27" y="-4.445"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-5.715"/>
<vertex x="1.905" y="-5.08"/>
<vertex x="3.175" y="-5.08"/>
<vertex x="3.81" y="-5.715"/>
<vertex x="3.81" y="-6.985"/>
<vertex x="3.175" y="-7.62"/>
<vertex x="1.905" y="-7.62"/>
<vertex x="1.27" y="-6.985"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="1.27" y="-8.255"/>
<vertex x="1.905" y="-7.62"/>
<vertex x="3.175" y="-7.62"/>
<vertex x="3.81" y="-8.255"/>
<vertex x="3.81" y="-9.525"/>
<vertex x="3.175" y="-10.16"/>
<vertex x="1.905" y="-10.16"/>
<vertex x="1.27" y="-9.525"/>
</polygon>
</package>
<package name="8P-2.54-65/35MIL">
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="-1.27" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="0" y="8.89" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="6.35" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="7" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<pad name="8" x="0" y="-8.89" drill="0.889" diameter="1.651"/>
<text x="-1.778" y="10.287" size="0.8128" layer="25" ratio="10">&gt;name</text>
<text x="0.254" y="-1.905" size="0.635" layer="27" ratio="10" rot="R90">&gt;value</text>
<text x="0.254" y="-1.524" size="0.635" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-10.16" x2="1.27" y2="10.16" layer="39"/>
</package>
<package name="4P-2.0">
<wire x1="-5" y1="1" x2="-5" y2="-1" width="0.127" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="-5" y1="2.2" x2="-5" y2="-2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="-2.2" x2="-3.2" y2="-2.2" width="0.254" layer="21"/>
<wire x1="3.2" y1="-2.2" x2="5" y2="-2.2" width="0.254" layer="21"/>
<wire x1="5" y1="-2.2" x2="5" y2="2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="2.2" x2="5" y2="2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="-2.2" x2="-5" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-5" y1="-2.8" x2="-3.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-3.2" y1="-2.8" x2="-3.2" y2="-2.2" width="0.254" layer="21"/>
<wire x1="-3.2" y1="-2.2" x2="3.2" y2="-2.2" width="0.254" layer="21"/>
<wire x1="3.2" y1="-2.2" x2="3.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.2" y1="-2.8" x2="5" y2="-2.8" width="0.254" layer="21"/>
<wire x1="5" y1="-2.8" x2="5" y2="-2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="2.2" x2="5" y2="2.2" width="0.254" layer="39"/>
<wire x1="5" y1="2.2" x2="5" y2="-2.8" width="0.254" layer="39"/>
<wire x1="-5" y1="-2.8" x2="-5" y2="2.2" width="0.254" layer="39"/>
<wire x1="-4.953" y1="-2.794" x2="-3.175" y2="-2.794" width="0.254" layer="39"/>
<wire x1="-3.175" y1="-2.794" x2="-3.175" y2="-2.159" width="0.254" layer="39"/>
<wire x1="-3.175" y1="-2.159" x2="-1.778" y2="-2.159" width="0.254" layer="39"/>
<wire x1="-1.778" y1="-2.159" x2="-1.778" y2="-4.191" width="0.254" layer="39"/>
<wire x1="-1.778" y1="-4.191" x2="1.778" y2="-4.191" width="0.254" layer="39"/>
<wire x1="1.778" y1="-4.191" x2="1.778" y2="-2.159" width="0.254" layer="39"/>
<wire x1="1.778" y1="-2.159" x2="3.175" y2="-2.159" width="0.254" layer="39"/>
<wire x1="3.175" y1="-2.159" x2="3.175" y2="-2.794" width="0.254" layer="39"/>
<wire x1="3.175" y1="-2.794" x2="4.953" y2="-2.794" width="0.254" layer="39"/>
<pad name="1" x="-3" y="0" drill="0.8" diameter="1.27" shape="square"/>
<pad name="2" x="-1" y="0" drill="0.8" diameter="1.27"/>
<pad name="3" x="1" y="0" drill="0.8" diameter="1.27"/>
<pad name="4" x="3" y="0" drill="0.8" diameter="1.27"/>
<text x="-5.08" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="0.635" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="4P-2.0-90D">
<wire x1="-5" y1="1" x2="-5" y2="-1" width="0.127" layer="21"/>
<wire x1="-5" y1="8.4" x2="-5" y2="1.6" width="0.254" layer="21"/>
<wire x1="-5" y1="1.6" x2="-5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="5" y1="1.6" x2="5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="5" y1="1.6" x2="5" y2="8.4" width="0.254" layer="21"/>
<wire x1="-5" y1="-1.5" x2="5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="-5" y1="8.5" x2="5" y2="8.5" width="0.254" layer="39"/>
<wire x1="5" y1="8.5" x2="5" y2="-1.4" width="0.254" layer="39"/>
<wire x1="5" y1="-1.4" x2="-5" y2="-1.4" width="0.254" layer="39"/>
<wire x1="-5" y1="-1.4" x2="-5" y2="8.5" width="0.254" layer="39"/>
<wire x1="-5" y1="1.6" x2="5" y2="1.6" width="0.254" layer="21"/>
<wire x1="-5" y1="8.4" x2="-2.8" y2="8.4" width="0.254" layer="21"/>
<wire x1="-2.8" y1="8.4" x2="-2.8" y2="7.4" width="0.254" layer="21"/>
<wire x1="-2.8" y1="7.4" x2="2.8" y2="7.4" width="0.254" layer="21"/>
<wire x1="2.8" y1="7.4" x2="2.8" y2="8.4" width="0.254" layer="21"/>
<wire x1="2.8" y1="8.4" x2="5" y2="8.4" width="0.254" layer="21"/>
<pad name="1" x="-3" y="0" drill="0.8" diameter="1.27" shape="square"/>
<pad name="2" x="-1" y="0" drill="0.8" diameter="1.27"/>
<pad name="3" x="1" y="0" drill="0.8" diameter="1.27"/>
<pad name="4" x="3" y="0" drill="0.8" diameter="1.27"/>
<text x="-3.667" y="-3.508" size="1.778" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.699" y="3.937" size="1.778" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CK_1X8">
<wire x1="-2.54" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;name</text>
<text x="1.27" y="-5.08" size="1.778" layer="96" rot="R90">&gt;value</text>
<pin name="2" x="-7.62" y="6.35" visible="pad" length="middle"/>
<pin name="3" x="-7.62" y="3.81" visible="pad" length="middle"/>
<pin name="4" x="-7.62" y="1.27" visible="pad" length="middle"/>
<pin name="5" x="-7.62" y="-1.27" visible="pad" length="middle"/>
<pin name="6" x="-7.62" y="-3.81" visible="pad" length="middle"/>
<pin name="7" x="-7.62" y="-6.35" visible="pad" length="middle"/>
<pin name="8" x="-7.62" y="-8.89" visible="pad" length="middle"/>
<pin name="1" x="-7.62" y="8.89" visible="pad" length="middle" function="dotclk"/>
</symbol>
<symbol name="TWIG_2.0">
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.254" layer="94"/>
<text x="-1.27" y="5.08" size="1.27" layer="95">&gt;name</text>
<text x="2.54" y="-3.81" size="1.27" layer="96" rot="R90">&gt;value</text>
<pin name="1" x="-3.81" y="3.81" visible="pad" length="middle" function="dotclk"/>
<pin name="2" x="-3.81" y="1.27" visible="pad" length="middle" function="dot"/>
<pin name="3" x="-3.81" y="-1.27" visible="pad" length="middle" function="dot"/>
<pin name="4" x="-3.81" y="-3.81" visible="pad" length="middle" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER-8P" prefix="J" uservalue="yes">
<gates>
<gate name="J" symbol="CK_1X8" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="8P-2.54">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-55/35MIL" package="8P-2.54-55/35MIL">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-80/40MIL" package="8P-2.54-80/40MIL">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.54-SMD" package="8P-2.54-SMD">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-F90" package="8P-2.54-FEMALE-D90">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M90" package="8P-2.54-MALE-D90">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-65/35MIL" package="8P-2.54-65/35MIL">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
<connect gate="J" pin="3" pad="3"/>
<connect gate="J" pin="4" pad="4"/>
<connect gate="J" pin="5" pad="5"/>
<connect gate="J" pin="6" pad="6"/>
<connect gate="J" pin="7" pad="7"/>
<connect gate="J" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TWIG-4P-2.0" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="TWIG_2.0" x="0" y="0"/>
</gates>
<devices>
<device name="-2.0" package="4P-2.0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'90D'" package="4P-2.0-90D">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1206FAB">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="C2220">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-3.743" y1="2.253" x2="3.743" y2="2.253" width="0.0508" layer="39"/>
<wire x1="3.743" y1="-2.253" x2="-3.743" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="-3.743" y1="-2.253" x2="-3.743" y2="2.253" width="0.0508" layer="39"/>
<wire x1="3.743" y1="2.253" x2="3.743" y2="-2.253" width="0.0508" layer="39"/>
<smd name="1" x="-2.794" y="0" dx="2.032" dy="5.334" layer="1"/>
<smd name="2" x="2.794" y="0" dx="2.032" dy="5.334" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.9718" y1="-0.8509" x2="-2.2217" y2="0.8491" layer="51"/>
<rectangle x1="2.2217" y1="-0.8491" x2="2.9718" y2="0.8509" layer="51"/>
</package>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206FAB">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="3.5MMTERM">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1" diameter="2.1844"/>
<pad name="2" x="-1.7" y="0" drill="1" diameter="2.1844"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
<package name="ED555DS-2DS">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1.2" diameter="2.1844"/>
<pad name="2" x="-1.7" y="0" drill="1.2" diameter="2.1844"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CAP-NONPOLARIZED">
<description>non-polarized capacitor</description>
<wire x1="-1.778" y1="1.524" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.524" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="1X2">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="5.715" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="4.445" size="1.4224" layer="97" ratio="9" rot="R180">1</text>
<text x="0" y="1.905" size="1.4224" layer="97" ratio="9" rot="R180">2</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" function="dot"/>
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP_UNPOLARIZED" prefix="C" uservalue="yes">
<gates>
<gate name="&gt;NAME" symbol="CAP-NONPOLARIZED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB" package="C1206FAB">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="C2220">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LD">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor (US Symbol)&lt;/b&gt;
&lt;p&gt;
Variants with postfix FAB are widened to allow the routing of internal traces</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206FAB" package="R1206FAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_02_TERM" prefix="J">
<description>3.5mm terminal block, 2 positions ED555-2DS as found in the fablab inventory. 
&lt;p&gt;Stolen from &lt;a href="http://www.ladyada.net/library/pcb/eaglelibrary.html"&gt;Adafruit&lt;/a&gt;, adapted by Zaerc.</description>
<gates>
<gate name="G$1" symbol="1X2" x="0" y="0"/>
</gates>
<devices>
<device name="-ADAFRUIT" package="3.5MMTERM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FABLAB" package="ED555DS-2DS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="OPL_Connector">
<description>&lt;b&gt;Seeed Open Parts Library (OPL) for the Seeed Fusion PCB Assembly Service
&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/opl.html" title="https://www.seeedstudio.com/opl.html"&gt;Seeed Fusion PCBA OPL&lt;/a&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/fusion_pcb.html"&gt;Order PCB/PCBA&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com"&gt;www.seeedstudio.com&lt;/a&gt;
&lt;br&gt;&lt;/b&gt;</description>
<packages>
<package name="USB4+2P-2.0-90D" urn="urn:adsk.eagle:footprint:8004462/1">
<description>&lt;b&gt;USB connector&lt;/b&gt;</description>
<wire x1="-7.4" y1="-10.19" x2="7.4" y2="-10.19" width="0.254" layer="21"/>
<wire x1="7.4" y1="-10.19" x2="7.4" y2="4.11" width="0.254" layer="51"/>
<wire x1="7.4" y1="4.11" x2="-7.4" y2="4.11" width="0.254" layer="51"/>
<wire x1="-7.4" y1="4.11" x2="-7.4" y2="-10.19" width="0.254" layer="51"/>
<wire x1="-5.08" y1="-2.87" x2="-3.81" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-8.72" x2="-2.54" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-8.72" x2="-1.27" y2="-2.87" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-2.87" x2="2.54" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-8.72" x2="3.81" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-8.72" x2="5.08" y2="-2.87" width="0.1524" layer="21"/>
<wire x1="-4.3" y1="4.1" x2="-7.4" y2="4.1" width="0.254" layer="21"/>
<wire x1="-7.4" y1="4.1" x2="-7.4" y2="1.8" width="0.254" layer="21"/>
<wire x1="-2.8" y1="4.1" x2="-1.8" y2="4.1" width="0.254" layer="21"/>
<wire x1="-0.3" y1="4.1" x2="0.3" y2="4.1" width="0.254" layer="21"/>
<wire x1="1.8" y1="4.1" x2="2.8" y2="4.1" width="0.254" layer="21"/>
<wire x1="4.3" y1="4.1" x2="7.4" y2="4.1" width="0.254" layer="21"/>
<wire x1="7.4" y1="4.1" x2="7.4" y2="1.9" width="0.254" layer="21"/>
<wire x1="7.4" y1="-1.9" x2="7.4" y2="-10.2" width="0.254" layer="21"/>
<wire x1="-7.4" y1="-10.2" x2="-7.4" y2="-1.9" width="0.254" layer="21"/>
<pad name="1" x="-3.5" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="2" x="-1" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="3" x="1" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="4" x="3.5" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="SG1" x="-6.57" y="0" drill="2.3" shape="octagon"/>
<pad name="SG2" x="6.57" y="0" drill="2.3" shape="octagon"/>
<text x="-1.905" y="4.445" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.064" y="1.016" size="1.016" layer="21" font="vector" ratio="15" rot="R270">VCC</text>
<text x="-1.524" y="1.016" size="1.016" layer="21" font="vector" ratio="15" rot="R270">D-</text>
<text x="0.508" y="1.016" size="1.016" layer="21" font="vector" ratio="15" rot="R270">D+</text>
<text x="3.048" y="1.016" size="1.016" layer="21" font="vector" ratio="15" rot="R270">GND</text>
</package>
</packages>
<packages3d>
<package3d name="USB4+2P-2.0-90D" urn="urn:adsk.eagle:package:8004520/1" type="box">
<description>&lt;b&gt;USB connector&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="USB4+2P-2.0-90D"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ST-USB-003A" urn="urn:adsk.eagle:symbol:8004413/1">
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short"/>
<text x="0" y="7.62" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="-6.35" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="2.54" layer="94" rot="R90">USB</text>
</symbol>
<symbol name="SGND" urn="urn:adsk.eagle:symbol:8004412/1">
<circle x="5.588" y="0" radius="0.508" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIP-USB-A-TYPE-FMAL(4+2P-2.0-90D)" urn="urn:adsk.eagle:component:8004611/1" prefix="JP" uservalue="yes">
<description>320010007</description>
<gates>
<gate name="G$1" symbol="ST-USB-003A" x="-2.54" y="0"/>
<gate name="G$2" symbol="SGND" x="7.62" y="5.08" addlevel="request"/>
<gate name="G$3" symbol="SGND" x="7.62" y="2.54" addlevel="request"/>
</gates>
<devices>
<device name="" package="USB4+2P-2.0-90D">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$2" pin="GND" pad="SG1"/>
<connect gate="G$3" pin="GND" pad="SG2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8004520/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="USB4-2.0-90D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Resistor">
<packages>
<package name="PR-D1.8*L3.3MM">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="6" x="2.54" y="0" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="5" x="-2.54" y="0" drill="0.8128" shape="octagon" rot="R90"/>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<text x="-1.905" y="1.143" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.651" y="-2.159" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RES">
<wire x1="-1.27" y1="0.508" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.254" layer="94"/>
<text x="-3.81" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" prefix="R" uservalue="yes">
<description>301020040</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PR-D1.8*L3.3MM">
<connects>
<connect gate="G$1" pin="1" pad="6"/>
<connect gate="G$1" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="FHCFR-1/6W-104J--" constant="no"/>
<attribute name="VALUE" value="100K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pimoroni">
<description>&lt;h3&gt;Pimoroni Eagle Bits&lt;/h3&gt;
Custom Eagle parts that we've created where needed. Please feel free to use, extend, and share!
&lt;br&gt;&lt;br&gt;
Find us at http://pimoroni.com
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes.</description>
<packages>
<package name="FCI_89898_313LF_40P_CONNECTOR_SMT_FLIPPED">
<smd name="P1_3V3" x="24.13" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P2_5V" x="24.13" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P3_GPIO2_SDA" x="21.59" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P4_5V" x="21.59" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P5_GPIO3_SCL" x="19.05" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P6_GND" x="19.05" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P7_GPIO4" x="16.51" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P8_GPIO14_UART0_TXD" x="16.51" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P9_GND" x="13.97" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P10_GPIO15_UART0_RXD" x="13.97" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P11_GPIO17" x="11.43" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P12_GPIO18" x="11.43" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P13_GPIO27" x="8.89" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P14_GND" x="8.89" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P15_GPIO22" x="6.35" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P16_GPIO23" x="6.35" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P17_3V3" x="3.81" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P18_GPIO24" x="3.81" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P19_GPIO10_SPI0_MOSI" x="1.27" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P20_GND" x="1.27" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P21_GPIO9_SPI0_MISO" x="-1.27" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P22_GPIO25" x="-1.27" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P23_GPIO11_SPI0_SCLK" x="-3.81" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P24_GPIO8_SPI0_CE0" x="-3.81" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P25_GND" x="-6.35" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P26_GPIO7_SPI0_CE1" x="-6.35" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P40_GPIO21" x="-24.13" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P39_GND" x="-24.13" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P38_GPIO20" x="-21.59" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P36_GPIO16" x="-19.05" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P34_GND" x="-16.51" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P32_GPIO12" x="-13.97" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P30_GND" x="-11.43" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P28_IDSC" x="-8.89" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P37_GPIO26" x="-21.59" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P35_GPIO19" x="-19.05" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P33_GPIO13" x="-16.51" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P31_GPIO6" x="-13.97" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P29_GPIO5" x="-11.43" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P27_IDSD" x="-8.89" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<polygon width="0.2" layer="41">
<vertex x="-24.73" y="1.27" curve="-90"/>
<vertex x="-24.13" y="1.87" curve="-90"/>
<vertex x="-23.53" y="1.27" curve="-90"/>
<vertex x="-24.13" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-24.73" y="-1.27" curve="-90"/>
<vertex x="-24.13" y="-0.67" curve="-90"/>
<vertex x="-23.53" y="-1.27" curve="-90"/>
<vertex x="-24.13" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-22.19" y="1.27" curve="-90"/>
<vertex x="-21.59" y="1.87" curve="-90"/>
<vertex x="-20.99" y="1.27" curve="-90"/>
<vertex x="-21.59" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-22.19" y="-1.27" curve="-90"/>
<vertex x="-21.59" y="-0.67" curve="-90"/>
<vertex x="-20.99" y="-1.27" curve="-90"/>
<vertex x="-21.59" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-19.65" y="1.27" curve="-90"/>
<vertex x="-19.05" y="1.87" curve="-90"/>
<vertex x="-18.45" y="1.27" curve="-90"/>
<vertex x="-19.05" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-19.65" y="-1.27" curve="-90"/>
<vertex x="-19.05" y="-0.67" curve="-90"/>
<vertex x="-18.45" y="-1.27" curve="-90"/>
<vertex x="-19.05" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-17.11" y="1.27" curve="-90"/>
<vertex x="-16.51" y="1.87" curve="-90"/>
<vertex x="-15.91" y="1.27" curve="-90"/>
<vertex x="-16.51" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-17.11" y="-1.27" curve="-90"/>
<vertex x="-16.51" y="-0.67" curve="-90"/>
<vertex x="-15.91" y="-1.27" curve="-90"/>
<vertex x="-16.51" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-14.57" y="1.27" curve="-90"/>
<vertex x="-13.97" y="1.87" curve="-90"/>
<vertex x="-13.37" y="1.27" curve="-90"/>
<vertex x="-13.97" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-14.57" y="-1.27" curve="-90"/>
<vertex x="-13.97" y="-0.67" curve="-90"/>
<vertex x="-13.37" y="-1.27" curve="-90"/>
<vertex x="-13.97" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-12.03" y="1.27" curve="-90"/>
<vertex x="-11.43" y="1.87" curve="-90"/>
<vertex x="-10.83" y="1.27" curve="-90"/>
<vertex x="-11.43" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-12.03" y="-1.27" curve="-90"/>
<vertex x="-11.43" y="-0.67" curve="-90"/>
<vertex x="-10.83" y="-1.27" curve="-90"/>
<vertex x="-11.43" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-9.49" y="1.27" curve="-90"/>
<vertex x="-8.89" y="1.87" curve="-90"/>
<vertex x="-8.29" y="1.27" curve="-90"/>
<vertex x="-8.89" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-9.49" y="-1.27" curve="-90"/>
<vertex x="-8.89" y="-0.67" curve="-90"/>
<vertex x="-8.29" y="-1.27" curve="-90"/>
<vertex x="-8.89" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-6.95" y="1.27" curve="-90"/>
<vertex x="-6.35" y="1.87" curve="-90"/>
<vertex x="-5.75" y="1.27" curve="-90"/>
<vertex x="-6.35" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-6.95" y="-1.27" curve="-90"/>
<vertex x="-6.35" y="-0.67" curve="-90"/>
<vertex x="-5.75" y="-1.27" curve="-90"/>
<vertex x="-6.35" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-4.41" y="1.27" curve="-90"/>
<vertex x="-3.81" y="1.87" curve="-90"/>
<vertex x="-3.21" y="1.27" curve="-90"/>
<vertex x="-3.81" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-4.41" y="-1.27" curve="-90"/>
<vertex x="-3.81" y="-0.67" curve="-90"/>
<vertex x="-3.21" y="-1.27" curve="-90"/>
<vertex x="-3.81" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-1.87" y="1.27" curve="-90"/>
<vertex x="-1.27" y="1.87" curve="-90"/>
<vertex x="-0.67" y="1.27" curve="-90"/>
<vertex x="-1.27" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-1.87" y="-1.27" curve="-90"/>
<vertex x="-1.27" y="-0.67" curve="-90"/>
<vertex x="-0.67" y="-1.27" curve="-90"/>
<vertex x="-1.27" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="0.67" y="1.27" curve="-90"/>
<vertex x="1.27" y="1.87" curve="-90"/>
<vertex x="1.87" y="1.27" curve="-90"/>
<vertex x="1.27" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="0.67" y="-1.27" curve="-90"/>
<vertex x="1.27" y="-0.67" curve="-90"/>
<vertex x="1.87" y="-1.27" curve="-90"/>
<vertex x="1.27" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="3.21" y="1.27" curve="-90"/>
<vertex x="3.81" y="1.87" curve="-90"/>
<vertex x="4.41" y="1.27" curve="-90"/>
<vertex x="3.81" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="3.21" y="-1.27" curve="-90"/>
<vertex x="3.81" y="-0.67" curve="-90"/>
<vertex x="4.41" y="-1.27" curve="-90"/>
<vertex x="3.81" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="5.75" y="1.27" curve="-90"/>
<vertex x="6.35" y="1.87" curve="-90"/>
<vertex x="6.95" y="1.27" curve="-90"/>
<vertex x="6.35" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="5.75" y="-1.27" curve="-90"/>
<vertex x="6.35" y="-0.67" curve="-90"/>
<vertex x="6.95" y="-1.27" curve="-90"/>
<vertex x="6.35" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="8.29" y="1.27" curve="-90"/>
<vertex x="8.89" y="1.87" curve="-90"/>
<vertex x="9.49" y="1.27" curve="-90"/>
<vertex x="8.89" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="8.29" y="-1.27" curve="-90"/>
<vertex x="8.89" y="-0.67" curve="-90"/>
<vertex x="9.49" y="-1.27" curve="-90"/>
<vertex x="8.89" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="10.83" y="1.27" curve="-90"/>
<vertex x="11.43" y="1.87" curve="-90"/>
<vertex x="12.03" y="1.27" curve="-90"/>
<vertex x="11.43" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="10.83" y="-1.27" curve="-90"/>
<vertex x="11.43" y="-0.67" curve="-90"/>
<vertex x="12.03" y="-1.27" curve="-90"/>
<vertex x="11.43" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="13.37" y="1.27" curve="-90"/>
<vertex x="13.97" y="1.87" curve="-90"/>
<vertex x="14.57" y="1.27" curve="-90"/>
<vertex x="13.97" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="13.37" y="-1.27" curve="-90"/>
<vertex x="13.97" y="-0.67" curve="-90"/>
<vertex x="14.57" y="-1.27" curve="-90"/>
<vertex x="13.97" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="15.91" y="1.27" curve="-90"/>
<vertex x="16.51" y="1.87" curve="-90"/>
<vertex x="17.11" y="1.27" curve="-90"/>
<vertex x="16.51" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="15.91" y="-1.27" curve="-90"/>
<vertex x="16.51" y="-0.67" curve="-90"/>
<vertex x="17.11" y="-1.27" curve="-90"/>
<vertex x="16.51" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="18.45" y="1.27" curve="-90"/>
<vertex x="19.05" y="1.87" curve="-90"/>
<vertex x="19.65" y="1.27" curve="-90"/>
<vertex x="19.05" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="18.45" y="-1.27" curve="-90"/>
<vertex x="19.05" y="-0.67" curve="-90"/>
<vertex x="19.65" y="-1.27" curve="-90"/>
<vertex x="19.05" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="20.99" y="1.27" curve="-90"/>
<vertex x="21.59" y="1.87" curve="-90"/>
<vertex x="22.19" y="1.27" curve="-90"/>
<vertex x="21.59" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="20.99" y="-1.27" curve="-90"/>
<vertex x="21.59" y="-0.67" curve="-90"/>
<vertex x="22.19" y="-1.27" curve="-90"/>
<vertex x="21.59" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="23.53" y="1.27" curve="-90"/>
<vertex x="24.13" y="1.87" curve="-90"/>
<vertex x="24.73" y="1.27" curve="-90"/>
<vertex x="24.13" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="23.53" y="-1.27" curve="-90"/>
<vertex x="24.13" y="-0.67" curve="-90"/>
<vertex x="24.73" y="-1.27" curve="-90"/>
<vertex x="24.13" y="-1.87" curve="-90"/>
</polygon>
</package>
<package name="FCI_89898_313LF_40P_CONNECTOR_SMT_FLIPPED_COMBO">
<smd name="P1_3V3" x="24.13" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P2_5V" x="24.13" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P3_GPIO2_SDA" x="21.59" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P4_5V" x="21.59" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P5_GPIO3_SCL" x="19.05" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P6_GND" x="19.05" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P7_GPIO4" x="16.51" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P8_GPIO14_UART0_TXD" x="16.51" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P9_GND" x="13.97" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P10_GPIO15_UART0_RXD" x="13.97" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P11_GPIO17" x="11.43" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P12_GPIO18" x="11.43" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P13_GPIO27" x="8.89" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P14_GND" x="8.89" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P15_GPIO22" x="6.35" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P16_GPIO23" x="6.35" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P17_3V3" x="3.81" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P18_GPIO24" x="3.81" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P19_GPIO10_SPI0_MOSI" x="1.27" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P20_GND" x="1.27" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P21_GPIO9_SPI0_MISO" x="-1.27" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P22_GPIO25" x="-1.27" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P23_GPIO11_SPI0_SCLK" x="-3.81" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P24_GPIO8_SPI0_CE0" x="-3.81" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P25_GND" x="-6.35" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P26_GPIO7_SPI0_CE1" x="-6.35" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P40_GPIO21" x="-24.13" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P39_GND" x="-24.13" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P38_GPIO20" x="-21.59" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P36_GPIO16" x="-19.05" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P34_GND" x="-16.51" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P32_GPIO12" x="-13.97" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P30_GND" x="-11.43" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P28_IDSC" x="-8.89" y="2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P37_GPIO26" x="-21.59" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P35_GPIO19" x="-19.05" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P33_GPIO13" x="-16.51" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P31_GPIO6" x="-13.97" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P29_GPIO5" x="-11.43" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<smd name="P27_IDSD" x="-8.89" y="-2.81" dx="1.57" dy="1.27" layer="1" rot="R90"/>
<polygon width="0.2" layer="41">
<vertex x="-24.73" y="1.27" curve="-90"/>
<vertex x="-24.13" y="1.87" curve="-90"/>
<vertex x="-23.53" y="1.27" curve="-90"/>
<vertex x="-24.13" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-24.73" y="-1.27" curve="-90"/>
<vertex x="-24.13" y="-0.67" curve="-90"/>
<vertex x="-23.53" y="-1.27" curve="-90"/>
<vertex x="-24.13" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-22.19" y="1.27" curve="-90"/>
<vertex x="-21.59" y="1.87" curve="-90"/>
<vertex x="-20.99" y="1.27" curve="-90"/>
<vertex x="-21.59" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-22.19" y="-1.27" curve="-90"/>
<vertex x="-21.59" y="-0.67" curve="-90"/>
<vertex x="-20.99" y="-1.27" curve="-90"/>
<vertex x="-21.59" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-19.65" y="1.27" curve="-90"/>
<vertex x="-19.05" y="1.87" curve="-90"/>
<vertex x="-18.45" y="1.27" curve="-90"/>
<vertex x="-19.05" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-19.65" y="-1.27" curve="-90"/>
<vertex x="-19.05" y="-0.67" curve="-90"/>
<vertex x="-18.45" y="-1.27" curve="-90"/>
<vertex x="-19.05" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-17.11" y="1.27" curve="-90"/>
<vertex x="-16.51" y="1.87" curve="-90"/>
<vertex x="-15.91" y="1.27" curve="-90"/>
<vertex x="-16.51" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-17.11" y="-1.27" curve="-90"/>
<vertex x="-16.51" y="-0.67" curve="-90"/>
<vertex x="-15.91" y="-1.27" curve="-90"/>
<vertex x="-16.51" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-14.57" y="1.27" curve="-90"/>
<vertex x="-13.97" y="1.87" curve="-90"/>
<vertex x="-13.37" y="1.27" curve="-90"/>
<vertex x="-13.97" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-14.57" y="-1.27" curve="-90"/>
<vertex x="-13.97" y="-0.67" curve="-90"/>
<vertex x="-13.37" y="-1.27" curve="-90"/>
<vertex x="-13.97" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-12.03" y="1.27" curve="-90"/>
<vertex x="-11.43" y="1.87" curve="-90"/>
<vertex x="-10.83" y="1.27" curve="-90"/>
<vertex x="-11.43" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-12.03" y="-1.27" curve="-90"/>
<vertex x="-11.43" y="-0.67" curve="-90"/>
<vertex x="-10.83" y="-1.27" curve="-90"/>
<vertex x="-11.43" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-9.49" y="1.27" curve="-90"/>
<vertex x="-8.89" y="1.87" curve="-90"/>
<vertex x="-8.29" y="1.27" curve="-90"/>
<vertex x="-8.89" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-9.49" y="-1.27" curve="-90"/>
<vertex x="-8.89" y="-0.67" curve="-90"/>
<vertex x="-8.29" y="-1.27" curve="-90"/>
<vertex x="-8.89" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-6.95" y="1.27" curve="-90"/>
<vertex x="-6.35" y="1.87" curve="-90"/>
<vertex x="-5.75" y="1.27" curve="-90"/>
<vertex x="-6.35" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-6.95" y="-1.27" curve="-90"/>
<vertex x="-6.35" y="-0.67" curve="-90"/>
<vertex x="-5.75" y="-1.27" curve="-90"/>
<vertex x="-6.35" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-4.41" y="1.27" curve="-90"/>
<vertex x="-3.81" y="1.87" curve="-90"/>
<vertex x="-3.21" y="1.27" curve="-90"/>
<vertex x="-3.81" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-4.41" y="-1.27" curve="-90"/>
<vertex x="-3.81" y="-0.67" curve="-90"/>
<vertex x="-3.21" y="-1.27" curve="-90"/>
<vertex x="-3.81" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-1.87" y="1.27" curve="-90"/>
<vertex x="-1.27" y="1.87" curve="-90"/>
<vertex x="-0.67" y="1.27" curve="-90"/>
<vertex x="-1.27" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-1.87" y="-1.27" curve="-90"/>
<vertex x="-1.27" y="-0.67" curve="-90"/>
<vertex x="-0.67" y="-1.27" curve="-90"/>
<vertex x="-1.27" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="0.67" y="1.27" curve="-90"/>
<vertex x="1.27" y="1.87" curve="-90"/>
<vertex x="1.87" y="1.27" curve="-90"/>
<vertex x="1.27" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="0.67" y="-1.27" curve="-90"/>
<vertex x="1.27" y="-0.67" curve="-90"/>
<vertex x="1.87" y="-1.27" curve="-90"/>
<vertex x="1.27" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="3.21" y="1.27" curve="-90"/>
<vertex x="3.81" y="1.87" curve="-90"/>
<vertex x="4.41" y="1.27" curve="-90"/>
<vertex x="3.81" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="3.21" y="-1.27" curve="-90"/>
<vertex x="3.81" y="-0.67" curve="-90"/>
<vertex x="4.41" y="-1.27" curve="-90"/>
<vertex x="3.81" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="5.75" y="1.27" curve="-90"/>
<vertex x="6.35" y="1.87" curve="-90"/>
<vertex x="6.95" y="1.27" curve="-90"/>
<vertex x="6.35" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="5.75" y="-1.27" curve="-90"/>
<vertex x="6.35" y="-0.67" curve="-90"/>
<vertex x="6.95" y="-1.27" curve="-90"/>
<vertex x="6.35" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="8.29" y="1.27" curve="-90"/>
<vertex x="8.89" y="1.87" curve="-90"/>
<vertex x="9.49" y="1.27" curve="-90"/>
<vertex x="8.89" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="8.29" y="-1.27" curve="-90"/>
<vertex x="8.89" y="-0.67" curve="-90"/>
<vertex x="9.49" y="-1.27" curve="-90"/>
<vertex x="8.89" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="10.83" y="1.27" curve="-90"/>
<vertex x="11.43" y="1.87" curve="-90"/>
<vertex x="12.03" y="1.27" curve="-90"/>
<vertex x="11.43" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="10.83" y="-1.27" curve="-90"/>
<vertex x="11.43" y="-0.67" curve="-90"/>
<vertex x="12.03" y="-1.27" curve="-90"/>
<vertex x="11.43" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="13.37" y="1.27" curve="-90"/>
<vertex x="13.97" y="1.87" curve="-90"/>
<vertex x="14.57" y="1.27" curve="-90"/>
<vertex x="13.97" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="13.37" y="-1.27" curve="-90"/>
<vertex x="13.97" y="-0.67" curve="-90"/>
<vertex x="14.57" y="-1.27" curve="-90"/>
<vertex x="13.97" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="15.91" y="1.27" curve="-90"/>
<vertex x="16.51" y="1.87" curve="-90"/>
<vertex x="17.11" y="1.27" curve="-90"/>
<vertex x="16.51" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="15.91" y="-1.27" curve="-90"/>
<vertex x="16.51" y="-0.67" curve="-90"/>
<vertex x="17.11" y="-1.27" curve="-90"/>
<vertex x="16.51" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="18.45" y="1.27" curve="-90"/>
<vertex x="19.05" y="1.87" curve="-90"/>
<vertex x="19.65" y="1.27" curve="-90"/>
<vertex x="19.05" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="18.45" y="-1.27" curve="-90"/>
<vertex x="19.05" y="-0.67" curve="-90"/>
<vertex x="19.65" y="-1.27" curve="-90"/>
<vertex x="19.05" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="20.99" y="1.27" curve="-90"/>
<vertex x="21.59" y="1.87" curve="-90"/>
<vertex x="22.19" y="1.27" curve="-90"/>
<vertex x="21.59" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="20.99" y="-1.27" curve="-90"/>
<vertex x="21.59" y="-0.67" curve="-90"/>
<vertex x="22.19" y="-1.27" curve="-90"/>
<vertex x="21.59" y="-1.87" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="23.53" y="1.27" curve="-90"/>
<vertex x="24.13" y="1.87" curve="-90"/>
<vertex x="24.73" y="1.27" curve="-90"/>
<vertex x="24.13" y="0.67" curve="-90"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="23.53" y="-1.27" curve="-90"/>
<vertex x="24.13" y="-0.67" curve="-90"/>
<vertex x="24.73" y="-1.27" curve="-90"/>
<vertex x="24.13" y="-1.87" curve="-90"/>
</polygon>
<pad name="1" x="24.13" y="-1.27" drill="1.016" diameter="1.778" shape="square" rot="R90"/>
<pad name="3" x="21.59" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="5" x="19.05" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="7" x="16.51" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="9" x="13.97" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="11" x="11.43" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="13" x="8.89" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="15" x="6.35" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="17" x="3.81" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="19" x="1.27" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="21" x="-1.27" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="23" x="-3.81" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="25" x="-6.35" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="27" x="-8.89" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="29" x="-11.43" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="31" x="-13.97" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="33" x="-16.51" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="35" x="-19.05" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="37" x="-21.59" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="39" x="-24.13" y="-1.27" drill="1.016" diameter="1.778" rot="R90"/>
<text x="25.4762" y="3.0988" size="1.27" layer="26" ratio="10" rot="MR0">&gt;NAME</text>
<text x="25.4" y="-4.445" size="1.27" layer="28" rot="MR0">&gt;VALUE</text>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="52" rot="R180"/>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="52" rot="R180"/>
<pad name="4" x="21.59" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="6" x="19.05" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="8" x="16.51" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="10" x="13.97" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="12" x="11.43" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="14" x="8.89" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="16" x="6.35" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="18" x="3.81" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="20" x="1.27" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="22" x="-1.27" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="24" x="-3.81" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="26" x="-6.35" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="28" x="-8.89" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="30" x="-11.43" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="32" x="-13.97" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="34" x="-16.51" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="36" x="-19.05" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="38" x="-21.59" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="40" x="-24.13" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="52" rot="R180"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="52" rot="R180"/>
<pad name="2" x="24.13" y="1.27" drill="1.016" diameter="1.778" rot="R90"/>
<wire x1="25.4" y1="2.54" x2="-25.4" y2="2.54" width="0.127" layer="52"/>
<wire x1="-25.4" y1="2.54" x2="-25.4" y2="-2.54" width="0.127" layer="52"/>
<wire x1="-25.4" y1="-2.54" x2="25.4" y2="-2.54" width="0.127" layer="52"/>
<wire x1="25.4" y1="-2.54" x2="25.4" y2="2.54" width="0.127" layer="52"/>
</package>
</packages>
<symbols>
<symbol name="FCI_89898_313LF_40P_CONNECTOR_SMT_FLIPPED">
<description>raspberry</description>
<pin name="3V3" x="-10.16" y="17.78" visible="pin" length="short" direction="pwr"/>
<wire x1="-7.62" y1="-38.1" x2="-7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="-7.62" y1="20.32" x2="12.7" y2="20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="20.32" x2="12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="12.7" y1="-38.1" x2="-7.62" y2="-38.1" width="0.254" layer="94"/>
<pin name="5V" x="15.24" y="17.78" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="2" x="-10.16" y="10.16" visible="pin" length="short"/>
<pin name="3" x="-10.16" y="7.62" visible="pin" length="short"/>
<pin name="4" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="14" x="15.24" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="15" x="15.24" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="17" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="18" x="15.24" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="27" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="22" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="23" x="15.24" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="24" x="15.24" y="0" visible="pin" length="short" rot="R180"/>
<pin name="10" x="-10.16" y="-7.62" visible="pin" length="short"/>
<pin name="GND" x="15.24" y="-35.56" visible="pin" length="short" direction="pas" rot="R180"/>
<pin name="9" x="-10.16" y="-10.16" visible="pin" length="short"/>
<pin name="25" x="15.24" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="11" x="-10.16" y="-12.7" visible="pin" length="short"/>
<pin name="8" x="15.24" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="7" x="15.24" y="-10.16" visible="pin" length="short" rot="R180"/>
<text x="-2.794" y="10.668" size="1.016" layer="94" distance="150" align="top-left">SDA
SCL</text>
<text x="6.604" y="13.208" size="1.016" layer="94" distance="150" align="top-right">TX
RX</text>
<pin name="ID_SD" x="-12.7" y="-17.78" visible="pin" length="middle"/>
<pin name="ID_SC" x="-12.7" y="-20.32" visible="pin" length="middle"/>
<pin name="5" x="-12.7" y="-25.4" visible="pin" length="middle"/>
<pin name="6" x="-12.7" y="-27.94" visible="pin" length="middle"/>
<pin name="13" x="-12.7" y="-30.48" visible="pin" length="middle"/>
<pin name="19" x="-12.7" y="-33.02" visible="pin" length="middle"/>
<pin name="26" x="-12.7" y="-35.56" visible="pin" length="middle"/>
<pin name="16" x="17.78" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="20" x="17.78" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="21" x="17.78" y="-22.86" visible="pin" length="middle" rot="R180"/>
<pin name="12" x="17.78" y="-15.24" visible="pin" length="middle" rot="R180"/>
<text x="7.366" y="-7.366" size="1.016" layer="94" distance="150" align="top-right">CE0
CE1</text>
<text x="-2.032" y="-7.112" size="1.016" layer="94" distance="150" align="top-left">MOSI
MISO
SCLK
</text>
<text x="-6.096" y="21.082" size="2.54" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RPIGPIOPINHD-2X20-SMD-FLIPPED-COMBO" prefix="P">
<gates>
<gate name="P$1" symbol="FCI_89898_313LF_40P_CONNECTOR_SMT_FLIPPED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FCI_89898_313LF_40P_CONNECTOR_SMT_FLIPPED">
<connects>
<connect gate="P$1" pin="10" pad="P19_GPIO10_SPI0_MOSI"/>
<connect gate="P$1" pin="11" pad="P23_GPIO11_SPI0_SCLK"/>
<connect gate="P$1" pin="12" pad="P32_GPIO12"/>
<connect gate="P$1" pin="13" pad="P33_GPIO13"/>
<connect gate="P$1" pin="14" pad="P8_GPIO14_UART0_TXD"/>
<connect gate="P$1" pin="15" pad="P10_GPIO15_UART0_RXD"/>
<connect gate="P$1" pin="16" pad="P36_GPIO16"/>
<connect gate="P$1" pin="17" pad="P11_GPIO17"/>
<connect gate="P$1" pin="18" pad="P12_GPIO18"/>
<connect gate="P$1" pin="19" pad="P35_GPIO19"/>
<connect gate="P$1" pin="2" pad="P3_GPIO2_SDA"/>
<connect gate="P$1" pin="20" pad="P38_GPIO20"/>
<connect gate="P$1" pin="21" pad="P40_GPIO21"/>
<connect gate="P$1" pin="22" pad="P15_GPIO22"/>
<connect gate="P$1" pin="23" pad="P16_GPIO23"/>
<connect gate="P$1" pin="24" pad="P18_GPIO24"/>
<connect gate="P$1" pin="25" pad="P22_GPIO25"/>
<connect gate="P$1" pin="26" pad="P37_GPIO26"/>
<connect gate="P$1" pin="27" pad="P13_GPIO27"/>
<connect gate="P$1" pin="3" pad="P5_GPIO3_SCL"/>
<connect gate="P$1" pin="3V3" pad="P1_3V3 P17_3V3"/>
<connect gate="P$1" pin="4" pad="P7_GPIO4"/>
<connect gate="P$1" pin="5" pad="P29_GPIO5"/>
<connect gate="P$1" pin="5V" pad="P2_5V P4_5V"/>
<connect gate="P$1" pin="6" pad="P31_GPIO6"/>
<connect gate="P$1" pin="7" pad="P26_GPIO7_SPI0_CE1"/>
<connect gate="P$1" pin="8" pad="P24_GPIO8_SPI0_CE0"/>
<connect gate="P$1" pin="9" pad="P21_GPIO9_SPI0_MISO"/>
<connect gate="P$1" pin="GND" pad="P6_GND P9_GND P14_GND P20_GND P25_GND P30_GND P34_GND P39_GND"/>
<connect gate="P$1" pin="ID_SC" pad="P28_IDSC"/>
<connect gate="P$1" pin="ID_SD" pad="P27_IDSD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="COMBO" package="FCI_89898_313LF_40P_CONNECTOR_SMT_FLIPPED_COMBO">
<connects>
<connect gate="P$1" pin="10" pad="19 P19_GPIO10_SPI0_MOSI" route="any"/>
<connect gate="P$1" pin="11" pad="23 P23_GPIO11_SPI0_SCLK" route="any"/>
<connect gate="P$1" pin="12" pad="32 P32_GPIO12" route="any"/>
<connect gate="P$1" pin="13" pad="33 P33_GPIO13" route="any"/>
<connect gate="P$1" pin="14" pad="8 P8_GPIO14_UART0_TXD" route="any"/>
<connect gate="P$1" pin="15" pad="10 P10_GPIO15_UART0_RXD" route="any"/>
<connect gate="P$1" pin="16" pad="36 P36_GPIO16" route="any"/>
<connect gate="P$1" pin="17" pad="11 P11_GPIO17" route="any"/>
<connect gate="P$1" pin="18" pad="12 P12_GPIO18" route="any"/>
<connect gate="P$1" pin="19" pad="35 P35_GPIO19" route="any"/>
<connect gate="P$1" pin="2" pad="3 P3_GPIO2_SDA" route="any"/>
<connect gate="P$1" pin="20" pad="38 P38_GPIO20" route="any"/>
<connect gate="P$1" pin="21" pad="40 P40_GPIO21" route="any"/>
<connect gate="P$1" pin="22" pad="15 P15_GPIO22" route="any"/>
<connect gate="P$1" pin="23" pad="16 P16_GPIO23" route="any"/>
<connect gate="P$1" pin="24" pad="18 P18_GPIO24" route="any"/>
<connect gate="P$1" pin="25" pad="22 P22_GPIO25" route="any"/>
<connect gate="P$1" pin="26" pad="37 P37_GPIO26" route="any"/>
<connect gate="P$1" pin="27" pad="13 P13_GPIO27" route="any"/>
<connect gate="P$1" pin="3" pad="5 P5_GPIO3_SCL" route="any"/>
<connect gate="P$1" pin="3V3" pad="1 17 P1_3V3 P17_3V3" route="any"/>
<connect gate="P$1" pin="4" pad="7 P7_GPIO4" route="any"/>
<connect gate="P$1" pin="5" pad="29 P29_GPIO5" route="any"/>
<connect gate="P$1" pin="5V" pad="2 4 P2_5V P4_5V" route="any"/>
<connect gate="P$1" pin="6" pad="31 P31_GPIO6" route="any"/>
<connect gate="P$1" pin="7" pad="26 P26_GPIO7_SPI0_CE1" route="any"/>
<connect gate="P$1" pin="8" pad="24 P24_GPIO8_SPI0_CE0" route="any"/>
<connect gate="P$1" pin="9" pad="21 P21_GPIO9_SPI0_MISO" route="any"/>
<connect gate="P$1" pin="GND" pad="6 9 14 20 25 30 34 39 P6_GND P9_GND P14_GND P20_GND P25_GND P30_GND P34_GND P39_GND" route="any"/>
<connect gate="P$1" pin="ID_SC" pad="28 P28_IDSC" route="any"/>
<connect gate="P$1" pin="ID_SD" pad="27 P27_IDSD" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="P" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CAP1188-1-CP-TR">
<description>&lt;Capacitive Touch Sensors 8 Channel Capacitive Touch Sensor 2 LED&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFN50P400X400X100-25N">
<description>&lt;b&gt;24 Pin QFN&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.95" y="1.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="2" x="-1.95" y="0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="3" x="-1.95" y="0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="4" x="-1.95" y="-0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="5" x="-1.95" y="-0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="6" x="-1.95" y="-1.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="7" x="-1.25" y="-1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="-1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-0.25" y="-1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0.25" y="-1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="0.75" y="-1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="1.25" y="-1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="1.95" y="-1.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="14" x="1.95" y="-0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="15" x="1.95" y="-0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="16" x="1.95" y="0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="17" x="1.95" y="0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="18" x="1.95" y="1.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="19" x="1.25" y="1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="0.75" y="1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="0.25" y="1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="-0.25" y="1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="-0.75" y="1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="-1.25" y="1.95" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="0" y="0" dx="2.6" dy="2.6" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.625" y1="2.625" x2="2.625" y2="2.625" width="0.05" layer="51"/>
<wire x1="2.625" y1="2.625" x2="2.625" y2="-2.625" width="0.05" layer="51"/>
<wire x1="2.625" y1="-2.625" x2="-2.625" y2="-2.625" width="0.05" layer="51"/>
<wire x1="-2.625" y1="-2.625" x2="-2.625" y2="2.625" width="0.05" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.1" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.1" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.1" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.1" layer="51"/>
<wire x1="-2" y1="1.5" x2="-1.5" y2="2" width="0.1" layer="51"/>
<circle x="-2.375" y="2" radius="0.125" width="0.25" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="CAP1188-1-CP-TR">
<wire x1="5.08" y1="12.7" x2="71.12" y2="12.7" width="0.254" layer="94"/>
<wire x1="71.12" y1="-22.86" x2="71.12" y2="12.7" width="0.254" layer="94"/>
<wire x1="71.12" y1="-22.86" x2="5.08" y2="-22.86" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="5.08" y2="-22.86" width="0.254" layer="94"/>
<text x="72.39" y="17.78" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="72.39" y="15.24" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="SPI_CS#" x="0" y="0" length="middle" direction="in"/>
<pin name="WAKE/SPI_MOSI" x="0" y="-2.54" length="middle"/>
<pin name="SMDATA/BC_DATA/SPI_MSIO/SPI_MISO" x="0" y="-5.08" length="middle"/>
<pin name="SMCLK/BC_CLK/SPI_CLK" x="0" y="-7.62" length="middle" direction="in"/>
<pin name="LED1" x="0" y="-10.16" length="middle"/>
<pin name="LED2" x="0" y="-12.7" length="middle"/>
<pin name="LED3" x="30.48" y="-27.94" length="middle" rot="R90"/>
<pin name="LED4" x="33.02" y="-27.94" length="middle" rot="R90"/>
<pin name="LED5" x="35.56" y="-27.94" length="middle" rot="R90"/>
<pin name="LED6" x="38.1" y="-27.94" length="middle" rot="R90"/>
<pin name="LED7" x="40.64" y="-27.94" length="middle" rot="R90"/>
<pin name="LED8" x="43.18" y="-27.94" length="middle" rot="R90"/>
<pin name="CS5" x="76.2" y="0" length="middle" direction="in" rot="R180"/>
<pin name="CS6" x="76.2" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="CS7" x="76.2" y="-5.08" length="middle" direction="in" rot="R180"/>
<pin name="CS8" x="76.2" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="ADDR_COMM" x="76.2" y="-10.16" length="middle" rot="R180"/>
<pin name="ALERT#/BC_IRQ#" x="76.2" y="-12.7" length="middle" rot="R180"/>
<pin name="EP_GND" x="30.48" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="RESET" x="33.02" y="17.78" length="middle" rot="R270"/>
<pin name="VDD" x="35.56" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="CS1" x="38.1" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="CS2" x="40.64" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="CS3" x="43.18" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="CS4" x="45.72" y="17.78" length="middle" direction="in" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP1188-1-CP-TR" prefix="IC">
<description>&lt;b&gt;Capacitive Touch Sensors 8 Channel Capacitive Touch Sensor 2 LED&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://ww1.microchip.com/downloads/en/DeviceDoc/00001620C.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CAP1188-1-CP-TR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P400X400X100-25N">
<connects>
<connect gate="G$1" pin="ADDR_COMM" pad="14"/>
<connect gate="G$1" pin="ALERT#/BC_IRQ#" pad="13"/>
<connect gate="G$1" pin="CS1" pad="22"/>
<connect gate="G$1" pin="CS2" pad="21"/>
<connect gate="G$1" pin="CS3" pad="20"/>
<connect gate="G$1" pin="CS4" pad="19"/>
<connect gate="G$1" pin="CS5" pad="18"/>
<connect gate="G$1" pin="CS6" pad="17"/>
<connect gate="G$1" pin="CS7" pad="16"/>
<connect gate="G$1" pin="CS8" pad="15"/>
<connect gate="G$1" pin="EP_GND" pad="25"/>
<connect gate="G$1" pin="LED1" pad="5"/>
<connect gate="G$1" pin="LED2" pad="6"/>
<connect gate="G$1" pin="LED3" pad="7"/>
<connect gate="G$1" pin="LED4" pad="8"/>
<connect gate="G$1" pin="LED5" pad="9"/>
<connect gate="G$1" pin="LED6" pad="10"/>
<connect gate="G$1" pin="LED7" pad="11"/>
<connect gate="G$1" pin="LED8" pad="12"/>
<connect gate="G$1" pin="RESET" pad="24"/>
<connect gate="G$1" pin="SMCLK/BC_CLK/SPI_CLK" pad="4"/>
<connect gate="G$1" pin="SMDATA/BC_DATA/SPI_MSIO/SPI_MISO" pad="3"/>
<connect gate="G$1" pin="SPI_CS#" pad="1"/>
<connect gate="G$1" pin="VDD" pad="23"/>
<connect gate="G$1" pin="WAKE/SPI_MOSI" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Capacitive Touch Sensors 8 Channel Capacitive Touch Sensor 2 LED" constant="no"/>
<attribute name="HEIGHT" value="1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CAP1188-1-CP-TR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="579-CAP1188-1-CP-TR" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Microchip-Technology/CAP1188-1-CP-TR?qs=iGJE2gFRJMNwyYIOex1Vgw%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="00_mylib201507">
<description>for AZPR EvBoard</description>
<packages>
<package name="TOUCH">
<rectangle x1="-2.4" y1="-10.1" x2="2.6" y2="-0.1" layer="1"/>
<pad name="P$1" x="0" y="0" drill="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="TOUCH">
<description>touch</description>
<rectangle x1="-12.7" y1="-2.54" x2="0" y2="2.54" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="93"/>
<text x="2.54" y="2.54" size="1.27" layer="95">touch</text>
<pin name="P$1" x="0" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TOUCH" prefix="TOUCH" uservalue="yes">
<gates>
<gate name="G$1" symbol="TOUCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TOUCH">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name="">
<attribute name="TOUCH" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="0805">
<description>0805 (2012 Metric)</description>
<wire x1="-1.873" y1="0.883" x2="1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.873" y1="-0.883" x2="-1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.873" y1="-0.883" x2="-1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.873" y1="0.883" x2="1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="1.85" y1="1" x2="1.85" y2="-1" width="0.2032" layer="21"/>
<wire x1="1.85" y1="-1" x2="-1.85" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="-1" x2="-1.85" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="1" x2="1.85" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="2.032" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.032" y="-0.762" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="1206">
<description>1206 (3216 Metric)</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="1.15" x2="2.45" y2="1.15" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.15" x2="2.45" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.15" x2="-2.45" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.15" x2="-2.45" y2="1.15" width="0.2032" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="2.54" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.54" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0603">
<description>0603 (1608 Metric)</description>
<wire x1="-1.473" y1="0.729" x2="1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.729" x2="1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.729" x2="-1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.729" x2="-1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.65" y1="0.75" x2="1.65" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.65" y1="0.75" x2="1.65" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-0.75" x2="-1.65" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="-0.75" x2="-1.65" y2="0.75" width="0.2032" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="1.778" y="-0.127" size="0.8128" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="1.778" y="-0.762" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.2032" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.2032" layer="51"/>
<wire x1="-1.346" y1="0.483" x2="1.346" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.346" y1="0.483" x2="1.346" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.346" y1="-0.483" x2="-1.346" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.346" y1="-0.483" x2="-1.346" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.25" y1="-0.7" x2="-1.25" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="0.7" x2="1.25" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.25" y1="0.7" x2="1.25" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="1.25" y1="-0.7" x2="-1.25" y2="-0.7" width="0.2032" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="1.397" y="-0.1905" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.397" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0603-MINI">
<description>0603-Mini
&lt;p&gt;Mini footprint for dense boards&lt;/p&gt;</description>
<wire x1="-1.346" y1="0.583" x2="1.346" y2="0.583" width="0.0508" layer="39"/>
<wire x1="1.346" y1="0.583" x2="1.346" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.346" y1="-0.583" x2="-1.346" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.346" y1="-0.583" x2="-1.346" y2="0.583" width="0.0508" layer="39"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.45" y1="0.7" x2="1.45" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.45" y1="0.7" x2="1.45" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="1.45" y1="-0.7" x2="-1.45" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.75" y="0" dx="0.9" dy="0.9" layer="1"/>
<smd name="2" x="0.75" y="0" dx="0.9" dy="0.9" layer="1"/>
<text x="1.524" y="-0.0635" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.524" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
</package>
<package name="2012">
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-3.302" y1="1.524" x2="3.302" y2="1.524" width="0.2032" layer="21"/>
<wire x1="3.302" y1="1.524" x2="3.302" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.524" x2="-3.302" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="-1.524" x2="-3.302" y2="1.524" width="0.2032" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.8415" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.667" y="-2.159" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805_NOTHERMALS">
<wire x1="-1.873" y1="0.883" x2="1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.873" y1="-0.883" x2="-1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.873" y1="-0.883" x2="-1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.873" y1="0.883" x2="1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="1.85" y1="1" x2="1.85" y2="-1" width="0.2032" layer="21"/>
<wire x1="1.85" y1="-1" x2="-1.85" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="-1" x2="-1.85" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="1" x2="1.85" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1" thermals="no"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1" thermals="no"/>
<text x="2.032" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.032" y="-0.762" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
</package>
<package name="2512">
<description>&lt;b&gt;RESISTOR 2512 (Metric 6432)&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-3.683" y="1.905" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.556" y="-2.286" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="_0402">
<description>&lt;b&gt; 0402&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.0573" y1="0.5557" x2="1.0573" y2="0.5557" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="0.5557" x2="1.0573" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="-0.5556" x2="-1.0573" y2="-0.5557" width="0.2032" layer="21"/>
<wire x1="-1.0573" y1="-0.5557" x2="-1.0573" y2="0.5557" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="-0.9525" y="0.7939" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-1.3336" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.0794" y1="-0.2381" x2="0.0794" y2="0.2381" layer="35"/>
<rectangle x1="0.25" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.25" y2="0.25" layer="51"/>
</package>
<package name="_0402MP">
<description>&lt;b&gt;0402 MicroPitch&lt;p&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="0" y1="0.127" x2="0" y2="-0.127" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-0.635" y="0.4763" size="0.6096" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.635" y="-0.7938" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="_0603">
<description>&lt;b&gt;0603&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.4605" y1="0.635" x2="1.4605" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="-0.635" x2="-1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.4605" y1="-0.635" x2="-1.4605" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.9" dy="0.8" layer="1"/>
<text x="-1.27" y="0.9525" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.4923" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8382" y2="0.4" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="_0603MP">
<description>&lt;b&gt;0603 MicroPitch&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.9525" y="0.635" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-0.9525" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="_0805">
<description>&lt;b&gt;0805&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.585" x2="0.41" y2="0.585" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.585" x2="0.41" y2="-0.585" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="0.889" x2="1.905" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.889" x2="1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-0.889" x2="-1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="-1.905" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.5875" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5874" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1.0564" y2="0.65" layer="51"/>
<rectangle x1="-1.0668" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="_0805MP">
<description>&lt;b&gt;0805 MicroPitch&lt;/b&gt;</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.5875" y="0.9525" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5875" y="-1.27" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="0805-NO">
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="2.032" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.032" y="-0.762" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.3048" layer="21"/>
</package>
<package name="TSSOP28">
<wire x1="-4.4646" y1="-2.2828" x2="4.4646" y2="-2.2828" width="0.2032" layer="51"/>
<wire x1="4.4646" y1="2.2828" x2="4.4646" y2="-2.2828" width="0.2032" layer="21"/>
<wire x1="4.4646" y1="2.2828" x2="-4.4646" y2="2.2828" width="0.2032" layer="51"/>
<wire x1="-4.4646" y1="-2.2828" x2="-4.4646" y2="2.2828" width="0.2032" layer="21"/>
<circle x="-3.5756" y="-1.2192" radius="0.4572" width="0.2032" layer="21"/>
<smd name="1" x="-4.225" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="21" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="22" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="23" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="24" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="25" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="26" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="27" x="-3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="28" x="-4.225" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="4.225" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="4.225" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-4.8456" y="-2.0828" size="0.8128" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="5.0742" y="-2.0828" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.3266" y1="-3.121" x2="-4.1234" y2="-2.2828" layer="51"/>
<rectangle x1="-3.6766" y1="-3.121" x2="-3.4734" y2="-2.2828" layer="51"/>
<rectangle x1="-3.0266" y1="-3.121" x2="-2.8234" y2="-2.2828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<rectangle x1="-3.0266" y1="2.2828" x2="-2.8234" y2="3.121" layer="51"/>
<rectangle x1="-3.6766" y1="2.2828" x2="-3.4734" y2="3.121" layer="51"/>
<rectangle x1="-4.3266" y1="2.2828" x2="-4.1234" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="-3.121" x2="3.0266" y2="-2.2828" layer="51"/>
<rectangle x1="3.4734" y1="-3.121" x2="3.6766" y2="-2.2828" layer="51"/>
<rectangle x1="4.1234" y1="-3.121" x2="4.3266" y2="-2.2828" layer="51"/>
<rectangle x1="4.1234" y1="2.2828" x2="4.3266" y2="3.121" layer="51"/>
<rectangle x1="3.4734" y1="2.2828" x2="3.6766" y2="3.121" layer="51"/>
<rectangle x1="2.8234" y1="2.2828" x2="3.0266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
</package>
<package name="SOLDERJUMPER_REFLOW">
<description>&lt;b&gt;Solder Jumper&lt;/b&gt; - Reflow</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SOLDERJUMPER_WAVE">
<description>&lt;b&gt;Solder Jumper&lt;/b&gt; - Wave</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.2032" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.2032" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.2032" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.2032" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.2032" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.2032" layer="51" curve="180"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
</package>
<package name="SOLDERJUMPER_CLOSEDWIRE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="WIRE" x="0" y="0" dx="0.635" dy="0.2032" layer="1" cream="no"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.254" layer="29">
<vertex x="-1.27" y="0.762"/>
<vertex x="1.27" y="0.762"/>
<vertex x="1.27" y="-0.762"/>
<vertex x="-1.27" y="-0.762"/>
</polygon>
</package>
<package name="SOLDERJUMPER_REFLOW_NOPASTE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SOLDERJUMPER_ARROW_NOPASTE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<smd name="2" x="1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.651" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.0508" layer="1">
<vertex x="-1.143" y="0.7366"/>
<vertex x="-0.5715" y="0.7366"/>
<vertex x="0.1651" y="0"/>
<vertex x="-0.5715" y="-0.7366"/>
<vertex x="-1.143" y="-0.7366"/>
</polygon>
<polygon width="0.0508" layer="1">
<vertex x="-0.127" y="0.7366"/>
<vertex x="0.5842" y="0"/>
<vertex x="-0.127" y="-0.7366"/>
<vertex x="1.143" y="-0.7366"/>
<vertex x="1.143" y="0.7366"/>
</polygon>
<rectangle x1="-1.4605" y1="-0.8255" x2="1.4605" y2="0.8255" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="2.032" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="PCA9865">
<wire x1="-17.78" y1="17.78" x2="17.78" y2="17.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="-17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-30.48" x2="-17.78" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-25.4" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="17.78" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-25.4" x2="17.78" y2="-25.4" width="0.254" layer="94" style="shortdash"/>
<text x="-5.08" y="30.48" size="1.27" layer="94">PCA9685</text>
<text x="-12.7" y="27.94" size="1.27" layer="94">16 Channel 12-Bit PWM</text>
<text x="-15.24" y="22.86" size="1.27" layer="94">VDD: 2.3-5.5V</text>
<text x="5.08" y="22.86" size="1.27" layer="94">IO: 5V Safe</text>
<text x="-15.24" y="20.32" size="1.27" layer="94">Output: 25mA Each/400mA Total</text>
<text x="-15.24" y="-27.94" size="1.27" layer="94">GND EXTCLK when not in use!</text>
<text x="-17.78" y="35.56" size="1.27" layer="95">&gt;NAME</text>
<text x="-17.78" y="-33.02" size="1.27" layer="96">&gt;VALUE</text>
<text x="-13.97" y="25.4" size="1.27" layer="94">I2C Address: 1[A5..A0][R/W]</text>
<pin name="PWM0" x="20.32" y="15.24" length="short" direction="out" rot="R180"/>
<pin name="PWM1" x="20.32" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="PWM2" x="20.32" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="PWM3" x="20.32" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="PWM4" x="20.32" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="PWM5" x="20.32" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="PWM6" x="20.32" y="0" length="short" direction="out" rot="R180"/>
<pin name="PWM7" x="20.32" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="PWM8" x="20.32" y="-5.08" length="short" direction="out" rot="R180"/>
<pin name="PWM9" x="20.32" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="PWM10" x="20.32" y="-10.16" length="short" direction="out" rot="R180"/>
<pin name="PWM11" x="20.32" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="PWM12" x="20.32" y="-15.24" length="short" direction="out" rot="R180"/>
<pin name="PWM13" x="20.32" y="-17.78" length="short" direction="out" rot="R180"/>
<pin name="PWM14" x="20.32" y="-20.32" length="short" direction="out" rot="R180"/>
<pin name="PWM15" x="20.32" y="-22.86" length="short" direction="out" rot="R180"/>
<pin name="GND" x="-20.32" y="-22.86" length="short" direction="pwr"/>
<pin name="SDA" x="-20.32" y="10.16" length="short"/>
<pin name="SCL" x="-20.32" y="7.62" length="short"/>
<pin name="#OE" x="-20.32" y="2.54" length="short" direction="in"/>
<pin name="EXTCLK" x="-20.32" y="-2.54" length="short" direction="in"/>
<pin name="A0" x="-20.32" y="-5.08" length="short"/>
<pin name="A1" x="-20.32" y="-7.62" length="short"/>
<pin name="A2" x="-20.32" y="-10.16" length="short"/>
<pin name="A3" x="-20.32" y="-12.7" length="short"/>
<pin name="A4" x="-20.32" y="-15.24" length="short"/>
<pin name="A5" x="-20.32" y="-17.78" length="short"/>
<pin name="VDD" x="-20.32" y="15.24" length="short" direction="pwr"/>
</symbol>
<symbol name="SOLDERJUMPER">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;Resistors&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;For new designs, use the packages preceded by an '_' character since they are more reliable:&lt;/p&gt;
&lt;p&gt;The following footprints should be used on most boards:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;_0402&lt;/b&gt; - Standard footprint for regular board layouts&lt;/li&gt;
&lt;li&gt;&lt;b&gt;_0603&lt;/b&gt; - Standard footprint for regular board layouts&lt;/li&gt;
&lt;li&gt;&lt;b&gt;_0805&lt;/b&gt; - Standard footprint for regular board layouts&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;For extremely tight-pitch boards where space is at a premium, the following 'micro-pitch' footprints can be used (smaller pads, no silkscreen outline, etc.):&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;_0402MP&lt;/b&gt; - Micro-pitch footprint for very dense/compact boards&lt;/li&gt;
&lt;li&gt;&lt;b&gt;_0603MP&lt;/b&gt; - Micro-pitch footprint for very dense/compact boards&lt;/li&gt;
&lt;li&gt;&lt;b&gt;_0805MP&lt;/b&gt; - Micro-pitch footprint for very dense/compact boards&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603MINI" package="0603-MINI">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_NOTHERMALS" package="0805_NOTHERMALS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402" package="_0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402MP" package="_0402MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="_0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603MP" package="_0603MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="_0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805MP" package="_0805MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_NOOUTLINE" package="0805-NO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PCA9685" prefix="U" uservalue="yes">
<description>&lt;b&gt;PCA9685&lt;/b&gt; - 16 Channel 12-Bit I2C PWM Controller
&lt;p&gt;5.0V tolerant 16 channel, 12-bit I2C PWM controller with 25mA per output (max. 400mA total)&lt;/p&gt;
&lt;p&gt;Digikey: 568-5931-1-ND&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="PCA9865" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP28">
<connects>
<connect gate="G$1" pin="#OE" pad="23"/>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="A3" pad="4"/>
<connect gate="G$1" pin="A4" pad="5"/>
<connect gate="G$1" pin="A5" pad="24"/>
<connect gate="G$1" pin="EXTCLK" pad="25"/>
<connect gate="G$1" pin="GND" pad="14"/>
<connect gate="G$1" pin="PWM0" pad="6"/>
<connect gate="G$1" pin="PWM1" pad="7"/>
<connect gate="G$1" pin="PWM10" pad="17"/>
<connect gate="G$1" pin="PWM11" pad="18"/>
<connect gate="G$1" pin="PWM12" pad="19"/>
<connect gate="G$1" pin="PWM13" pad="20"/>
<connect gate="G$1" pin="PWM14" pad="21"/>
<connect gate="G$1" pin="PWM15" pad="22"/>
<connect gate="G$1" pin="PWM2" pad="8"/>
<connect gate="G$1" pin="PWM3" pad="9"/>
<connect gate="G$1" pin="PWM4" pad="10"/>
<connect gate="G$1" pin="PWM5" pad="11"/>
<connect gate="G$1" pin="PWM6" pad="12"/>
<connect gate="G$1" pin="PWM7" pad="13"/>
<connect gate="G$1" pin="PWM8" pad="15"/>
<connect gate="G$1" pin="PWM9" pad="16"/>
<connect gate="G$1" pin="SCL" pad="26"/>
<connect gate="G$1" pin="SDA" pad="27"/>
<connect gate="G$1" pin="VDD" pad="28"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDERJUMPER" prefix="SJ" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;SMD Solder JUMPER&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;Solder the two pads together to create a connection, or remove the solder to break it.&lt;/p&gt;
&lt;b&gt;REFLOW&lt;/b&gt; - Use this footprint for solder paste and reflow ovens.&lt;br/&gt;
&lt;b&gt;WAVE&lt;/b&gt; - Use this footprint for hand-soldering (larger pads).
&lt;p&gt;&lt;b&gt;CLOSED&lt;/b&gt; - Has a trace between the two pads to ensure it is closed by default.  The trace needs to be cut to disable the jumper, and can be closed again by creating a solder bridge between the two pads.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="SOLDERJUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="REFLOW" package="SOLDERJUMPER_REFLOW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WAVE" package="SOLDERJUMPER_WAVE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CLOSED" package="SOLDERJUMPER_CLOSEDWIRE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="REFLOW_NOPASTE" package="SOLDERJUMPER_REFLOW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SOLDERJUMPER_ARROW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="OPL_Resistor">
<description>&lt;b&gt;Seeed Open Parts Library (OPL) for the Seeed Fusion PCB Assembly Service
&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/opl.html" title="https://www.seeedstudio.com/opl.html"&gt;Seeed Fusion PCBA OPL&lt;/a&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/fusion_pcb.html"&gt;Order PCB/PCBA&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com"&gt;www.seeedstudio.com&lt;/a&gt;
&lt;br&gt;&lt;/b&gt;</description>
<packages>
<package name="ADJR3-7.0X7.0X5.0MM" urn="urn:adsk.eagle:footprint:8004920/1">
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-0.25" drill="0.8" diameter="1.35"/>
<pad name="2" x="2.54" y="-0.25" drill="0.8" diameter="1.35"/>
<pad name="3" x="0" y="2.25" drill="0.8" diameter="1.35" shape="square"/>
<text x="-3.175" y="3.683" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-3.175" y="-4.572" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-3.556" x2="3.429" y2="3.556" layer="39"/>
</package>
</packages>
<packages3d>
<package3d name="ADJR3-7.0X7.0X5.0MM" urn="urn:adsk.eagle:package:8004938/1" type="box">
<packageinstances>
<packageinstance name="ADJR3-7.0X7.0X5.0MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RES-VARISTOR-3P" urn="urn:adsk.eagle:symbol:8004929/1">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="2.667" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<text x="-5.08" y="2.032" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="1.778" y="-3.048" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="2.54"/>
<vertex x="2.667" y="3.81"/>
<vertex x="1.778" y="2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIP-VR-TOP-ADJ-1K-3362P(3P-7.0X7.0X5.0MM)" urn="urn:adsk.eagle:component:8005020/1" prefix="R" uservalue="yes">
<description>301040014</description>
<gates>
<gate name="G$1" symbol="RES-VARISTOR-3P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADJR3-7.0X7.0X5.0MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8004938/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MPN" value="SX3362P" constant="no"/>
<attribute name="VALUE" value="1K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="3522200RFT">
<description>&lt;200 2512 Thick Film SMD Resistor +/-1% 3W - 3522200RFT&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="RESC6432X120N">
<description>&lt;b&gt;3522200RFT&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.4" y="0" dx="3.4" dy="2.4" layer="1" rot="R90"/>
<smd name="2" x="2.4" y="0" dx="3.4" dy="2.4" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.85" y1="1.95" x2="3.85" y2="1.95" width="0.05" layer="51"/>
<wire x1="3.85" y1="1.95" x2="3.85" y2="-1.95" width="0.05" layer="51"/>
<wire x1="3.85" y1="-1.95" x2="-3.85" y2="-1.95" width="0.05" layer="51"/>
<wire x1="-3.85" y1="-1.95" x2="-3.85" y2="1.95" width="0.05" layer="51"/>
<wire x1="-3.175" y1="1.6" x2="3.175" y2="1.6" width="0.1" layer="51"/>
<wire x1="3.175" y1="1.6" x2="3.175" y2="-1.6" width="0.1" layer="51"/>
<wire x1="3.175" y1="-1.6" x2="-3.175" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-3.175" y1="-1.6" x2="-3.175" y2="1.6" width="0.1" layer="51"/>
<wire x1="0" y1="1.5" x2="0" y2="-1.5" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="3522200RFT">
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="13.97" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="13.97" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="17.78" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="3522200RFT" prefix="R">
<description>&lt;b&gt;200 2512 Thick Film SMD Resistor +/-1% 3W - 3522200RFT&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&amp;DocNm=9-1773463-7&amp;DocType=DS&amp;DocLang=English"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="3522200RFT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC6432X120N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="200 2512 Thick Film SMD Resistor +/-1% 3W - 3522200RFT" constant="no"/>
<attribute name="HEIGHT" value="1.2mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TE Connectivity" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="3522200RFT" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="279-3522200RFT" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/TE-Connectivity-Holsworthy/3522200RFT?qs=Rv6LVDxB0ZqJHWqrjvZeFw%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X05" urn="urn:adsk.eagle:footprint:22354/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.4262" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
</package>
<package name="1X05/90" urn="urn:adsk.eagle:footprint:22355/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-6.985" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="8.255" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.461" y1="0.635" x2="-4.699" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="-5.461" y1="-2.921" x2="-4.699" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="4.699" y1="-2.921" x2="5.461" y2="-1.905" layer="21"/>
</package>
<package name="1_05X2MM" urn="urn:adsk.eagle:footprint:22356/1" library_version="3">
<description>CON-M-1X5-200</description>
<text x="-4.5" y="1.5" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5" y1="0.5" x2="-4.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="1" x2="-3.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-3.5" y1="1" x2="-3" y2="0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="-0.5" x2="-3.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-3.5" y1="-1" x2="-4.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-1" x2="-5" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-5" y1="0.5" x2="-5" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="0.5" x2="-2.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="1" x2="-1.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="1" x2="-1" y2="0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-1" x2="-2.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="-1" x2="-3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="0.5" x2="-3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.5" x2="-0.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="1" x2="0.5" y2="1" width="0.1524" layer="21"/>
<wire x1="0.5" y1="1" x2="1" y2="0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="-0.5" x2="0.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-1" x2="-0.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="-1" x2="-1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="0.5" x2="1.5" y2="1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="1" x2="2.5" y2="1" width="0.1524" layer="21"/>
<wire x1="2.5" y1="1" x2="3" y2="0.5" width="0.1524" layer="21"/>
<wire x1="3" y1="-0.5" x2="2.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="2.5" y1="-1" x2="1.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-1" x2="1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="3" y1="0.5" x2="3.5" y2="1" width="0.1524" layer="21"/>
<wire x1="3.5" y1="1" x2="4.5" y2="1" width="0.1524" layer="21"/>
<wire x1="4.5" y1="1" x2="5" y2="0.5" width="0.1524" layer="21"/>
<wire x1="5" y1="0.5" x2="5" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="5" y1="-0.5" x2="4.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-1" x2="3.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="3.5" y1="-1" x2="3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="3" y1="0.5" x2="3" y2="-0.5" width="0.1524" layer="21"/>
<pad name="1" x="-4" y="0" drill="1.016" diameter="1.3" shape="square" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<pad name="2" x="-2" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<pad name="4" x="2" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<pad name="5" x="4" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="1X05" urn="urn:adsk.eagle:package:22469/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X05"/>
</packageinstances>
</package3d>
<package3d name="1X05/90" urn="urn:adsk.eagle:package:22467/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X05/90"/>
</packageinstances>
</package3d>
<package3d name="1_05X2MM" urn="urn:adsk.eagle:package:22466/2" type="model" library_version="3">
<description>CON-M-1X5-200</description>
<packageinstances>
<packageinstance name="1_05X2MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD5" urn="urn:adsk.eagle:symbol:22353/1" library_version="3">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X5" urn="urn:adsk.eagle:component:22529/4" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22469/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X05/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22467/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5X2MM" package="1_05X2MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22466/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="uln-udn" urn="urn:adsk.eagle:library:407">
<description>&lt;b&gt;Driver Arrays&lt;/b&gt;&lt;p&gt;
ULN and UDN Series&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16" urn="urn:adsk.eagle:footprint:917/1" library_version="2">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16" urn="urn:adsk.eagle:footprint:918/1" library_version="2">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="DIL16" urn="urn:adsk.eagle:package:922/2" type="model" library_version="2">
<description>Dual In Line Package</description>
<packageinstances>
<packageinstance name="DIL16"/>
</packageinstances>
</package3d>
<package3d name="SO16" urn="urn:adsk.eagle:package:923/2" type="model" library_version="2">
<description>Small Outline Package</description>
<packageinstances>
<packageinstance name="SO16"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="2001A" urn="urn:adsk.eagle:symbol:30225/1" library_version="2">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I1" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="I2" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="I3" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="I4" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="I5" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="I6" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="I7" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="O1" x="12.7" y="7.62" length="middle" direction="oc" rot="R180"/>
<pin name="O2" x="12.7" y="5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O3" x="12.7" y="2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O4" x="12.7" y="0" length="middle" direction="oc" rot="R180"/>
<pin name="O5" x="12.7" y="-2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O6" x="12.7" y="-5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O7" x="12.7" y="-7.62" length="middle" direction="oc" rot="R180"/>
<pin name="CD+" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="-12.7" y="-10.16" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ULN2003A" urn="urn:adsk.eagle:component:30250/2" prefix="IC" library_version="2">
<description>&lt;b&gt;DRIVER ARRAY&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="2001A" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="DIL16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:922/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:923/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="00_mylib201507" library_urn="urn:adsk.eagle:library:9922397" deviceset="A3L-LOC" device=""/>
<part name="H3" library="00_mylib201507" library_urn="urn:adsk.eagle:library:9922397" deviceset="HOLE_32" device="" package3d_urn="urn:adsk.eagle:package:9923011/1"/>
<part name="H4" library="00_mylib201507" library_urn="urn:adsk.eagle:library:9922397" deviceset="HOLE_32" device="" package3d_urn="urn:adsk.eagle:package:9923011/1"/>
<part name="H2" library="00_mylib201507" library_urn="urn:adsk.eagle:library:9922397" deviceset="HOLE_32" device="" package3d_urn="urn:adsk.eagle:package:9923011/1"/>
<part name="H1" library="00_mylib201507" library_urn="urn:adsk.eagle:library:9922397" deviceset="HOLE_32" device="" package3d_urn="urn:adsk.eagle:package:9923011/1"/>
<part name="GND11" library="SparkFun" deviceset="GND" device=""/>
<part name="GND14" library="SparkFun" deviceset="GND" device=""/>
<part name="GND10" library="SparkFun" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun" deviceset="GND" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C1" library="fab" deviceset="CAP_UNPOLARIZED" device="FAB" value="10uF"/>
<part name="J8" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="GND30" library="SparkFun" deviceset="GND" device=""/>
<part name="P+19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="J9" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="GND31" library="SparkFun" deviceset="GND" device=""/>
<part name="J11" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="P+18" library="SparkFun" deviceset="3.3V" device=""/>
<part name="JP11" library="OPL_Connector" deviceset="DIP-USB-A-TYPE-FMAL(4+2P-2.0-90D)" device="" package3d_urn="urn:adsk.eagle:package:8004520/1" value="USB4-2.0-90D"/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P1" library="pimoroni" deviceset="RPIGPIOPINHD-2X20-SMD-FLIPPED-COMBO" device="COMBO"/>
<part name="GND3" library="SparkFun" deviceset="GND" device=""/>
<part name="P+3" library="SparkFun" deviceset="3.3V" device=""/>
<part name="JP12" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP7" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP3" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="JP13" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP8" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP4" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="JP14" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP9" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP5" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="JP15" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="JP10" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="JP6" library="SparkFun" deviceset="M04" device="PTH" value="AIN"/>
<part name="J10" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="J12" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DNP"/>
<part name="J13" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DNP"/>
<part name="J14" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="J15" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="R39" library="Seeed-Resistor" deviceset="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" device="" value="0.23"/>
<part name="R40" library="Seeed-Resistor" deviceset="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" device="" value="0.23"/>
<part name="GND21" library="SparkFun" deviceset="GND" device=""/>
<part name="GND16" library="SparkFun" deviceset="GND" device=""/>
<part name="GND22" library="SparkFun" deviceset="GND" device=""/>
<part name="GND17" library="SparkFun" deviceset="GND" device=""/>
<part name="GND27" library="SparkFun" deviceset="GND" device=""/>
<part name="GND28" library="SparkFun" deviceset="GND" device=""/>
<part name="P+16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="IC1" library="CAP1188-1-CP-TR" deviceset="CAP1188-1-CP-TR" device=""/>
<part name="IC2" library="CAP1188-1-CP-TR" deviceset="CAP1188-1-CP-TR" device=""/>
<part name="TOUCH1" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH2" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH3" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH4" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH5" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH6" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH7" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH8" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH9" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH10" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH11" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH12" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH13" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH14" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH15" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="TOUCH16" library="00_mylib201507" deviceset="TOUCH" device=""/>
<part name="LD1" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R1" library="fab" deviceset="R" device="1206FAB"/>
<part name="R25" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="10k"/>
<part name="R24" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="10k"/>
<part name="R23" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="10k"/>
<part name="SJ8" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="SJ9" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="U1" library="microbuilder" deviceset="PCA9685" device="" value="PCA9685"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="R26" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="10k"/>
<part name="R31" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="10k"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="P+10" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+9" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R35" library="Seeed-Resistor" deviceset="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" device="" value="0.23"/>
<part name="R36" library="Seeed-Resistor" deviceset="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" device="" value="0.23"/>
<part name="GND15" library="SparkFun" deviceset="GND" device=""/>
<part name="P+13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="J16" library="fab" deviceset="CONN_02_TERM" device="-FABLAB"/>
<part name="GND29" library="SparkFun" deviceset="GND" device=""/>
<part name="SJ10" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="LD2" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R2" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD3" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R3" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD4" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R4" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD5" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R5" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD6" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R6" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD7" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R7" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD8" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R8" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD9" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R9" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD10" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R10" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD11" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R11" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD12" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R12" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD13" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R13" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD14" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R14" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD15" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R15" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD16" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R16" library="fab" deviceset="R" device="1206FAB"/>
<part name="P+1" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+2" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+5" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R18" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="100k"/>
<part name="R17" library="microbuilder" deviceset="RESISTOR" device="0805_NOOUTLINE" value="100k"/>
<part name="SJ6" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="SJ7" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="P+7" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+8" library="SparkFun" deviceset="3.3V" device=""/>
<part name="SJ1" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="SJ2" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="SJ3" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="SJ4" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="R20" library="Seeed-Resistor" deviceset="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" device="" value="50k"/>
<part name="R22" library="Seeed-Resistor" deviceset="DIP-RES-100K-5%-1/6W(PR-D1.8XL3.3MM)" device="" value="50k"/>
<part name="R34" library="OPL_Resistor" deviceset="DIP-VR-TOP-ADJ-1K-3362P(3P-7.0X7.0X5.0MM)" device="" package3d_urn="urn:adsk.eagle:package:8004938/1" value="1K"/>
<part name="J1" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DNP"/>
<part name="J2" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DNP"/>
<part name="J6" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="GND19" library="SparkFun" deviceset="GND" device=""/>
<part name="J7" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="P+15" library="SparkFun" deviceset="3.3V" device=""/>
<part name="SJ5" library="microbuilder" deviceset="SOLDERJUMPER" device="REFLOW_NOPASTE"/>
<part name="R21" library="OPL_Resistor" deviceset="DIP-VR-TOP-ADJ-1K-3362P(3P-7.0X7.0X5.0MM)" device="" package3d_urn="urn:adsk.eagle:package:8004938/1" value="100k"/>
<part name="R19" library="OPL_Resistor" deviceset="DIP-VR-TOP-ADJ-1K-3362P(3P-7.0X7.0X5.0MM)" device="" package3d_urn="urn:adsk.eagle:package:8004938/1" value="100k"/>
<part name="R37" library="3522200RFT" deviceset="3522200RFT" device=""/>
<part name="R38" library="3522200RFT" deviceset="3522200RFT" device=""/>
<part name="J3" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DNP"/>
<part name="J4" library="Connector" deviceset="HEADER-8P" device="-2.54" value="DNP"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="P+11" library="SparkFun" deviceset="3.3V" device=""/>
<part name="J5" library="Connector" deviceset="TWIG-4P-2.0" device="-2.0" value="D0"/>
<part name="IC3" library="uln-udn" library_urn="urn:adsk.eagle:library:407" deviceset="ULN2003A" device="N" package3d_urn="urn:adsk.eagle:package:922/2"/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X5" device="" package3d_urn="urn:adsk.eagle:package:22469/2"/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="JP2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X5" device="" package3d_urn="urn:adsk.eagle:package:22469/2"/>
<part name="LD27" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R41" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD25" library="fab" deviceset="LED" device="FAB1206"/>
<part name="LD28" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R42" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD26" library="fab" deviceset="LED" device="FAB1206"/>
<part name="LD23" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R32" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD21" library="fab" deviceset="LED" device="FAB1206"/>
<part name="LD24" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R33" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD22" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R27" library="fab" deviceset="R" device="1206FAB"/>
<part name="LD17" library="fab" deviceset="LED" device="FAB1206"/>
<part name="LD18" library="fab" deviceset="LED" device="FAB1206"/>
<part name="LD19" library="fab" deviceset="LED" device="FAB1206"/>
<part name="LD20" library="fab" deviceset="LED" device="FAB1206"/>
<part name="R28" library="fab" deviceset="R" device="1206FAB"/>
<part name="R29" library="fab" deviceset="R" device="1206FAB"/>
<part name="R30" library="fab" deviceset="R" device="1206FAB"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="83.82" y="43.18" size="1.778" layer="91">A4988</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="-144.78" y="-63.5" smashed="yes">
<attribute name="DRAWING_NAME" x="199.39" y="-48.26" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="199.39" y="-53.34" size="2.286" layer="94" font="vector"/>
<attribute name="VALUE" x="199.39" y="-43.18" size="2.286" layer="94" font="vector"/>
</instance>
<instance part="H3" gate="G$1" x="127" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="126.4158" y="-42.926" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="129.4638" y="-42.926" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="H4" gate="G$1" x="137.16" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="136.5758" y="-42.926" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="139.6238" y="-42.926" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="H2" gate="G$1" x="116.84" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="116.2558" y="-42.926" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="119.3038" y="-42.926" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="H1" gate="G$1" x="106.68" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="106.0958" y="-42.926" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="109.1438" y="-42.926" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="GND11" gate="1" x="116.84" y="-53.34" smashed="yes" rot="MR0">
<attribute name="VALUE" x="119.38" y="-55.88" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND14" gate="1" x="137.16" y="-53.34" smashed="yes" rot="MR0">
<attribute name="VALUE" x="139.7" y="-55.88" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND10" gate="1" x="106.68" y="-53.34" smashed="yes" rot="MR0">
<attribute name="VALUE" x="109.22" y="-55.88" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND13" gate="1" x="127" y="-53.34" smashed="yes" rot="MR0">
<attribute name="VALUE" x="129.54" y="-55.88" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND18" gate="1" x="172.72" y="73.66" smashed="yes" rot="MR0">
<attribute name="VALUE" x="175.26" y="71.12" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C1" gate="&gt;NAME" x="172.72" y="83.82" smashed="yes" rot="MR90">
<attribute name="NAME" x="175.26" y="80.01" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="168.91" y="80.01" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="J8" gate="G$1" x="191.77" y="-13.97" smashed="yes" rot="MR180">
<attribute name="NAME" x="190.5" y="-19.05" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="194.31" y="-10.16" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="GND30" gate="1" x="218.44" y="-10.16" smashed="yes" rot="MR270">
<attribute name="VALUE" x="220.98" y="-7.62" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="P+19" gate="1" x="215.9" y="-12.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="218.44" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="J9" gate="G$1" x="191.77" y="-29.21" smashed="yes" rot="MR180">
<attribute name="NAME" x="190.5" y="-34.29" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="194.31" y="-25.4" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="GND31" gate="1" x="218.44" y="-25.4" smashed="yes" rot="MR270">
<attribute name="VALUE" x="220.98" y="-22.86" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="J11" gate="G$1" x="201.93" y="-29.21" smashed="yes" rot="MR180">
<attribute name="NAME" x="200.66" y="-34.29" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="204.47" y="-25.4" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="P+18" gate="G$1" x="212.09" y="-27.94" smashed="yes" rot="R270">
<attribute name="VALUE" x="215.646" y="-26.924" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="JP11" gate="G$1" x="200.66" y="83.82" smashed="yes">
<attribute name="NAME" x="200.66" y="91.44" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="200.66" y="77.47" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND20" gate="1" x="193.04" y="81.28" smashed="yes" rot="R270">
<attribute name="VALUE" x="190.5" y="83.82" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P1" gate="P$1" x="0" y="5.08" smashed="yes">
<attribute name="NAME" x="-6.096" y="26.162" size="2.54" layer="95"/>
</instance>
<instance part="GND3" gate="1" x="20.32" y="-30.48" smashed="yes" rot="MR270">
<attribute name="VALUE" x="22.86" y="-27.94" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="P+3" gate="G$1" x="-12.7" y="22.86" smashed="yes" rot="MR270">
<attribute name="VALUE" x="-16.256" y="23.876" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="JP12" gate="G$1" x="205.74" y="175.26" smashed="yes" rot="R180">
<attribute name="VALUE" x="210.82" y="182.88" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="210.82" y="166.878" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP7" gate="G$1" x="187.96" y="175.26" smashed="yes" rot="R180">
<attribute name="VALUE" x="193.04" y="182.88" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="193.04" y="166.878" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP3" gate="G$1" x="172.72" y="175.26" smashed="yes" rot="R180">
<attribute name="VALUE" x="177.8" y="182.88" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="177.8" y="166.878" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND23" gate="1" x="198.12" y="165.1" smashed="yes" rot="MR0">
<attribute name="VALUE" x="198.12" y="162.56" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP13" gate="G$1" x="205.74" y="152.4" smashed="yes" rot="R180">
<attribute name="VALUE" x="210.82" y="160.02" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="210.82" y="144.018" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP8" gate="G$1" x="187.96" y="152.4" smashed="yes" rot="R180">
<attribute name="VALUE" x="193.04" y="160.02" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="193.04" y="144.018" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP4" gate="G$1" x="172.72" y="152.4" smashed="yes" rot="R180">
<attribute name="VALUE" x="177.8" y="160.02" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="177.8" y="144.018" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND24" gate="1" x="198.12" y="142.24" smashed="yes" rot="MR0">
<attribute name="VALUE" x="200.66" y="139.7" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP14" gate="G$1" x="205.74" y="129.54" smashed="yes" rot="R180">
<attribute name="VALUE" x="210.82" y="137.16" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="210.82" y="121.158" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP9" gate="G$1" x="187.96" y="129.54" smashed="yes" rot="R180">
<attribute name="VALUE" x="193.04" y="137.16" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="193.04" y="121.158" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP5" gate="G$1" x="172.72" y="129.54" smashed="yes" rot="R180">
<attribute name="VALUE" x="177.8" y="137.16" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="177.8" y="121.158" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND25" gate="1" x="198.12" y="119.38" smashed="yes" rot="MR0">
<attribute name="VALUE" x="200.66" y="114.3" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP15" gate="G$1" x="205.74" y="106.68" smashed="yes" rot="R180">
<attribute name="VALUE" x="210.82" y="114.3" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="210.82" y="98.298" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP10" gate="G$1" x="187.96" y="106.68" smashed="yes" rot="R180">
<attribute name="VALUE" x="193.04" y="114.3" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="193.04" y="98.298" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND26" gate="1" x="198.12" y="96.52" smashed="yes" rot="MR0">
<attribute name="VALUE" x="200.66" y="93.98" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP6" gate="G$1" x="172.72" y="106.68" smashed="yes" rot="R180">
<attribute name="VALUE" x="177.8" y="114.3" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="177.8" y="98.298" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J10" gate="G$1" x="201.93" y="-13.97" smashed="yes" rot="MR180">
<attribute name="NAME" x="200.66" y="-19.05" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="204.47" y="-10.16" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="J12" gate="J" x="203.2" y="31.75" smashed="yes" rot="MR180">
<attribute name="NAME" x="198.12" y="21.59" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="204.47" y="36.83" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="J13" gate="J" x="203.2" y="6.35" smashed="yes" rot="MR180">
<attribute name="NAME" x="198.12" y="-3.81" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="204.47" y="11.43" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="J14" gate="G$1" x="214.63" y="41.91" smashed="yes" rot="MR180">
<attribute name="NAME" x="213.36" y="36.83" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="217.17" y="45.72" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="J15" gate="G$1" x="217.17" y="16.51" smashed="yes" rot="MR180">
<attribute name="NAME" x="215.9" y="11.43" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="219.71" y="20.32" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R39" gate="G$1" x="179.07" y="38.1" smashed="yes">
<attribute name="NAME" x="175.26" y="39.37" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="175.26" y="35.56" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R40" gate="G$1" x="179.07" y="12.7" smashed="yes">
<attribute name="NAME" x="175.26" y="13.97" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="175.26" y="10.16" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND21" gate="1" x="193.04" y="22.86" smashed="yes" rot="R270">
<attribute name="VALUE" x="190.5" y="25.4" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND16" gate="1" x="170.18" y="38.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="167.64" y="40.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND22" gate="1" x="193.04" y="-2.54" smashed="yes" rot="R270">
<attribute name="VALUE" x="190.5" y="0" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND17" gate="1" x="170.18" y="12.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="167.64" y="15.24" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND27" gate="1" x="205.74" y="45.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="203.2" y="48.26" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND28" gate="1" x="208.28" y="20.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="205.74" y="22.86" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+16" gate="1" x="205.74" y="43.18" smashed="yes" rot="MR270">
<attribute name="VALUE" x="205.232" y="43.942" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+17" gate="1" x="210.82" y="17.78" smashed="yes" rot="MR270">
<attribute name="VALUE" x="210.312" y="18.542" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+6" gate="1" x="33.02" y="22.86" smashed="yes" rot="R270">
<attribute name="VALUE" x="35.56" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="P+14" gate="1" x="162.56" y="88.9" smashed="yes" rot="R90">
<attribute name="VALUE" x="160.02" y="91.44" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="IC1" gate="G$1" x="-45.72" y="147.32" smashed="yes">
<attribute name="NAME" x="26.67" y="165.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="26.67" y="162.56" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC2" gate="G$1" x="-45.72" y="78.74" smashed="yes">
<attribute name="NAME" x="26.67" y="96.52" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="26.67" y="93.98" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="TOUCH1" gate="G$1" x="-109.22" y="180.34" smashed="yes"/>
<instance part="TOUCH2" gate="G$1" x="-109.22" y="167.64" smashed="yes"/>
<instance part="TOUCH3" gate="G$1" x="-109.22" y="154.94" smashed="yes"/>
<instance part="TOUCH4" gate="G$1" x="-109.22" y="142.24" smashed="yes"/>
<instance part="TOUCH5" gate="G$1" x="-109.22" y="129.54" smashed="yes"/>
<instance part="TOUCH6" gate="G$1" x="-109.22" y="116.84" smashed="yes"/>
<instance part="TOUCH7" gate="G$1" x="-109.22" y="104.14" smashed="yes"/>
<instance part="TOUCH8" gate="G$1" x="-109.22" y="91.44" smashed="yes"/>
<instance part="TOUCH9" gate="G$1" x="-109.22" y="66.04" smashed="yes"/>
<instance part="TOUCH10" gate="G$1" x="-109.22" y="53.34" smashed="yes"/>
<instance part="TOUCH11" gate="G$1" x="-109.22" y="40.64" smashed="yes"/>
<instance part="TOUCH12" gate="G$1" x="-109.22" y="27.94" smashed="yes"/>
<instance part="TOUCH13" gate="G$1" x="-109.22" y="15.24" smashed="yes"/>
<instance part="TOUCH14" gate="G$1" x="-109.22" y="2.54" smashed="yes"/>
<instance part="TOUCH15" gate="G$1" x="-109.22" y="-10.16" smashed="yes"/>
<instance part="TOUCH16" gate="G$1" x="-109.22" y="-22.86" smashed="yes"/>
<instance part="LD1" gate="G$1" x="-116.84" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="188.976" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="191.135" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="-109.22" y="185.42" smashed="yes">
<attribute name="NAME" x="-113.03" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="119.38" y="86.36" smashed="yes" rot="MR90">
<attribute name="NAME" x="121.412" y="83.82" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="116.205" y="83.82" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="R24" gate="G$1" x="114.3" y="81.28" smashed="yes" rot="MR90">
<attribute name="NAME" x="116.332" y="78.74" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="111.125" y="78.74" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="R23" gate="G$1" x="109.22" y="76.2" smashed="yes" rot="MR90">
<attribute name="NAME" x="111.252" y="73.66" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="106.045" y="73.66" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="SJ8" gate="1" x="96.52" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="93.98" y="78.74" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="100.33" y="78.74" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SJ9" gate="1" x="104.14" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="101.6" y="78.74" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="107.95" y="78.74" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U1" gate="G$1" x="119.38" y="149.86" smashed="yes">
<attribute name="NAME" x="101.6" y="185.42" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.6" y="116.84" size="1.27" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="96.52" y="152.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="93.98" y="154.94" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND8" gate="1" x="96.52" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="93.98" y="129.54" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R26" gate="G$1" x="124.46" y="81.28" smashed="yes" rot="MR90">
<attribute name="NAME" x="126.492" y="78.74" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="121.285" y="78.74" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="R31" gate="G$1" x="129.54" y="76.2" smashed="yes" rot="MR90">
<attribute name="NAME" x="131.572" y="73.66" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="126.365" y="73.66" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="GND12" gate="1" x="119.38" y="66.04" smashed="yes" rot="MR0">
<attribute name="VALUE" x="121.92" y="63.5" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+10" gate="G$1" x="93.98" y="165.1" smashed="yes" rot="MR270">
<attribute name="VALUE" x="90.424" y="166.116" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="P+9" gate="G$1" x="91.44" y="76.2" smashed="yes" rot="MR270">
<attribute name="VALUE" x="87.884" y="77.216" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="R35" gate="G$1" x="157.48" y="8.89" smashed="yes" rot="R90">
<attribute name="NAME" x="156.21" y="5.08" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="160.02" y="5.08" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R36" gate="G$1" x="157.48" y="-3.81" smashed="yes" rot="R90">
<attribute name="NAME" x="156.21" y="-7.62" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="160.02" y="-7.62" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND15" gate="1" x="157.48" y="-12.7" smashed="yes">
<attribute name="VALUE" x="154.94" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="P+13" gate="1" x="157.48" y="17.78" smashed="yes" rot="MR0">
<attribute name="VALUE" x="158.242" y="18.288" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="J16" gate="G$1" x="226.06" y="60.96" smashed="yes">
<attribute name="NAME" x="223.52" y="66.675" size="1.778" layer="95"/>
</instance>
<instance part="GND29" gate="1" x="218.44" y="58.42" smashed="yes">
<attribute name="VALUE" x="215.9" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="SJ10" gate="1" x="182.88" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="180.34" y="68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="186.69" y="68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LD2" gate="G$1" x="-116.84" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="176.276" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="178.435" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R2" gate="G$1" x="-109.22" y="172.72" smashed="yes">
<attribute name="NAME" x="-113.03" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="169.418" size="1.778" layer="96"/>
</instance>
<instance part="LD3" gate="G$1" x="-116.84" y="160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="163.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="165.735" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="-109.22" y="160.02" smashed="yes">
<attribute name="NAME" x="-113.03" y="161.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="156.718" size="1.778" layer="96"/>
</instance>
<instance part="LD4" gate="G$1" x="-116.84" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="150.876" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="153.035" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R4" gate="G$1" x="-109.22" y="147.32" smashed="yes">
<attribute name="NAME" x="-113.03" y="148.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="144.018" size="1.778" layer="96"/>
</instance>
<instance part="LD5" gate="G$1" x="-116.84" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="138.176" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="140.335" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R5" gate="G$1" x="-109.22" y="134.62" smashed="yes">
<attribute name="NAME" x="-113.03" y="136.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="131.318" size="1.778" layer="96"/>
</instance>
<instance part="LD6" gate="G$1" x="-116.84" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="125.476" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="127.635" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="-109.22" y="121.92" smashed="yes">
<attribute name="NAME" x="-113.03" y="123.4186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="118.618" size="1.778" layer="96"/>
</instance>
<instance part="LD7" gate="G$1" x="-116.84" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="112.776" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="114.935" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R7" gate="G$1" x="-109.22" y="109.22" smashed="yes">
<attribute name="NAME" x="-113.03" y="110.7186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="105.918" size="1.778" layer="96"/>
</instance>
<instance part="LD8" gate="G$1" x="-116.84" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="100.076" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="102.235" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R8" gate="G$1" x="-109.22" y="96.52" smashed="yes">
<attribute name="NAME" x="-113.03" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="LD9" gate="G$1" x="-116.84" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="74.676" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="76.835" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R9" gate="G$1" x="-109.22" y="71.12" smashed="yes">
<attribute name="NAME" x="-113.03" y="72.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="67.818" size="1.778" layer="96"/>
</instance>
<instance part="LD10" gate="G$1" x="-116.84" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="61.976" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="64.135" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R10" gate="G$1" x="-109.22" y="58.42" smashed="yes">
<attribute name="NAME" x="-113.03" y="59.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="55.118" size="1.778" layer="96"/>
</instance>
<instance part="LD11" gate="G$1" x="-116.84" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="49.276" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="51.435" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R11" gate="G$1" x="-109.22" y="45.72" smashed="yes">
<attribute name="NAME" x="-113.03" y="47.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="42.418" size="1.778" layer="96"/>
</instance>
<instance part="LD12" gate="G$1" x="-116.84" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="36.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="38.735" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R12" gate="G$1" x="-109.22" y="33.02" smashed="yes">
<attribute name="NAME" x="-113.03" y="34.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="29.718" size="1.778" layer="96"/>
</instance>
<instance part="LD13" gate="G$1" x="-116.84" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="23.876" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="26.035" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R13" gate="G$1" x="-109.22" y="20.32" smashed="yes">
<attribute name="NAME" x="-113.03" y="21.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="17.018" size="1.778" layer="96"/>
</instance>
<instance part="LD14" gate="G$1" x="-116.84" y="7.62" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="11.176" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="13.335" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R14" gate="G$1" x="-109.22" y="7.62" smashed="yes">
<attribute name="NAME" x="-113.03" y="9.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="4.318" size="1.778" layer="96"/>
</instance>
<instance part="LD15" gate="G$1" x="-116.84" y="-5.08" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="-1.524" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="0.635" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R15" gate="G$1" x="-109.22" y="-5.08" smashed="yes">
<attribute name="NAME" x="-113.03" y="-3.5814" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="-8.382" size="1.778" layer="96"/>
</instance>
<instance part="LD16" gate="G$1" x="-116.84" y="-17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="-114.808" y="-14.224" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-114.808" y="-12.065" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R16" gate="G$1" x="-109.22" y="-17.78" smashed="yes">
<attribute name="NAME" x="-113.03" y="-16.2814" size="1.778" layer="95"/>
<attribute name="VALUE" x="-113.03" y="-21.082" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="G$1" x="-134.62" y="185.42" smashed="yes" rot="MR270">
<attribute name="VALUE" x="-138.176" y="186.436" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="P+2" gate="G$1" x="-134.62" y="71.12" smashed="yes" rot="MR270">
<attribute name="VALUE" x="-138.176" y="72.136" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="GND1" gate="1" x="-15.24" y="170.18" smashed="yes" rot="R180">
<attribute name="VALUE" x="-12.7" y="172.72" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="-15.24" y="101.6" smashed="yes" rot="R180">
<attribute name="VALUE" x="-12.7" y="104.14" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+4" gate="G$1" x="-10.16" y="170.18" smashed="yes" rot="MR0">
<attribute name="VALUE" x="-9.144" y="173.736" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+5" gate="G$1" x="-10.16" y="101.6" smashed="yes" rot="MR0">
<attribute name="VALUE" x="-9.144" y="105.156" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R18" gate="G$1" x="50.8" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="47.752" y="53.34" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="47.625" y="58.42" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="R17" gate="G$1" x="48.26" y="129.54" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.292" y="127" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="45.085" y="127" size="1.27" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="SJ6" gate="1" x="48.26" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="139.7" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="52.07" y="139.7" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SJ7" gate="1" x="50.8" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="48.26" y="73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="54.61" y="73.66" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="50.8" y="50.8" smashed="yes" rot="MR0">
<attribute name="VALUE" x="53.34" y="48.26" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND4" gate="1" x="48.26" y="119.38" smashed="yes" rot="MR0">
<attribute name="VALUE" x="50.8" y="116.84" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+7" gate="G$1" x="48.26" y="149.86" smashed="yes" rot="MR0">
<attribute name="VALUE" x="49.276" y="153.416" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+8" gate="G$1" x="50.8" y="86.36" smashed="yes" rot="MR0">
<attribute name="VALUE" x="51.816" y="89.916" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SJ1" gate="1" x="-55.88" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="-61.976" y="148.336" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-53.34" y="151.13" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SJ2" gate="1" x="-55.88" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="-61.722" y="145.542" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-53.34" y="148.59" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SJ3" gate="1" x="-55.88" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="-61.468" y="79.756" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-53.34" y="82.55" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SJ4" gate="1" x="-55.88" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="-61.468" y="76.962" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-53.34" y="80.01" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R20" gate="G$1" x="55.88" y="128.27" smashed="yes" rot="R90">
<attribute name="NAME" x="54.61" y="124.46" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="58.42" y="124.46" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R22" gate="G$1" x="58.42" y="57.15" smashed="yes" rot="R90">
<attribute name="NAME" x="57.15" y="53.34" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="60.96" y="55.88" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R34" gate="G$1" x="149.86" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="147.828" y="-2.54" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="152.908" y="4.318" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J1" gate="J" x="-64.77" y="109.22" smashed="yes" rot="MR270">
<attribute name="NAME" x="-74.93" y="114.3" size="1.778" layer="95" rot="MR270"/>
<attribute name="VALUE" x="-59.69" y="107.95" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="J2" gate="J" x="-62.23" y="40.64" smashed="yes" rot="MR270">
<attribute name="NAME" x="-72.39" y="45.72" size="1.778" layer="95" rot="MR270"/>
<attribute name="VALUE" x="-57.15" y="39.37" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="J6" gate="G$1" x="153.67" y="-29.21" smashed="yes" rot="MR180">
<attribute name="NAME" x="152.4" y="-34.29" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="156.21" y="-25.4" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="GND19" gate="1" x="180.34" y="-25.4" smashed="yes" rot="MR270">
<attribute name="VALUE" x="182.88" y="-22.86" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="J7" gate="G$1" x="163.83" y="-29.21" smashed="yes" rot="MR180">
<attribute name="NAME" x="162.56" y="-34.29" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="166.37" y="-25.4" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="P+15" gate="G$1" x="173.99" y="-27.94" smashed="yes" rot="R270">
<attribute name="VALUE" x="177.546" y="-26.924" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SJ5" gate="1" x="22.86" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="25.4" y="20.32" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="25.4" y="26.67" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R21" gate="G$1" x="58.42" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="60.452" y="73.66" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="62.992" y="69.342" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R19" gate="G$1" x="55.88" y="137.16" smashed="yes" rot="R270">
<attribute name="NAME" x="57.912" y="142.24" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="60.452" y="137.922" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R37" gate="G$1" x="172.72" y="40.64" smashed="yes">
<attribute name="NAME" x="186.69" y="46.99" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="186.69" y="44.45" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="R38" gate="G$1" x="172.72" y="15.24" smashed="yes">
<attribute name="NAME" x="186.69" y="21.59" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="186.69" y="19.05" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="J3" gate="J" x="83.82" y="29.21" smashed="yes" rot="MR180">
<attribute name="NAME" x="78.74" y="19.05" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="85.09" y="34.29" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="J4" gate="J" x="93.98" y="29.21" smashed="yes" rot="R180">
<attribute name="NAME" x="99.06" y="19.05" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="92.71" y="34.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND9" gate="1" x="104.14" y="17.78" smashed="yes" rot="MR0">
<attribute name="VALUE" x="106.68" y="15.24" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+11" gate="G$1" x="109.22" y="22.86" smashed="yes" rot="MR90">
<attribute name="VALUE" x="112.776" y="21.844" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="J5" gate="G$1" x="115.57" y="31.75" smashed="yes" rot="MR180">
<attribute name="NAME" x="114.3" y="26.67" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="118.11" y="35.56" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="IC3" gate="A" x="83.82" y="-7.62" smashed="yes">
<attribute name="NAME" x="76.2" y="3.302" size="1.778" layer="95"/>
<attribute name="VALUE" x="76.2" y="-22.86" size="1.778" layer="96"/>
</instance>
<instance part="JP1" gate="A" x="106.68" y="-5.08" smashed="yes">
<attribute name="NAME" x="100.33" y="3.175" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.33" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="P+12" gate="1" x="142.24" y="-17.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="144.78" y="-20.32" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="68.58" y="-20.32" smashed="yes" rot="MR0">
<attribute name="VALUE" x="71.12" y="-22.86" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP2" gate="A" x="129.54" y="30.48" smashed="yes">
<attribute name="NAME" x="123.19" y="38.735" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.19" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="LD27" gate="G$1" x="228.6" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="230.632" y="46.736" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="230.632" y="48.895" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R41" gate="G$1" x="228.6" y="33.02" smashed="yes">
<attribute name="NAME" x="224.79" y="34.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="29.718" size="1.778" layer="96"/>
</instance>
<instance part="LD25" gate="G$1" x="226.06" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="224.028" y="34.544" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.028" y="32.385" size="1.778" layer="96"/>
</instance>
<instance part="LD28" gate="G$1" x="228.6" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="230.632" y="21.336" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="230.632" y="23.495" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R42" gate="G$1" x="228.6" y="7.62" smashed="yes">
<attribute name="NAME" x="224.79" y="9.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="4.318" size="1.778" layer="96"/>
</instance>
<instance part="LD26" gate="G$1" x="226.06" y="12.7" smashed="yes" rot="R270">
<attribute name="NAME" x="224.028" y="9.144" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.028" y="6.985" size="1.778" layer="96"/>
</instance>
<instance part="LD23" gate="G$1" x="142.24" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="144.272" y="44.196" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="144.272" y="46.355" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R32" gate="G$1" x="149.86" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="153.67" y="36.6014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="153.67" y="41.402" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LD21" gate="G$1" x="139.7" y="35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="137.668" y="32.004" size="1.778" layer="95"/>
<attribute name="VALUE" x="137.668" y="29.845" size="1.778" layer="96"/>
</instance>
<instance part="LD24" gate="G$1" x="142.24" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="144.272" y="34.036" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="144.272" y="36.195" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R33" gate="G$1" x="149.86" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="153.67" y="26.4414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="153.67" y="31.242" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LD22" gate="G$1" x="139.7" y="25.4" smashed="yes" rot="R270">
<attribute name="NAME" x="137.668" y="21.844" size="1.778" layer="95"/>
<attribute name="VALUE" x="137.668" y="19.685" size="1.778" layer="96"/>
</instance>
<instance part="R27" gate="G$1" x="127" y="0" smashed="yes" rot="R180">
<attribute name="NAME" x="130.81" y="-1.4986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="130.81" y="3.302" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LD17" gate="G$1" x="114.3" y="0" smashed="yes" rot="R270">
<attribute name="NAME" x="112.268" y="-3.556" size="1.778" layer="95"/>
<attribute name="VALUE" x="112.268" y="-5.715" size="1.778" layer="96"/>
</instance>
<instance part="LD18" gate="G$1" x="114.3" y="-2.54" smashed="yes" rot="R270">
<attribute name="NAME" x="112.268" y="-6.096" size="1.778" layer="95"/>
<attribute name="VALUE" x="112.268" y="-8.255" size="1.778" layer="96"/>
</instance>
<instance part="LD19" gate="G$1" x="114.3" y="-5.08" smashed="yes" rot="R270">
<attribute name="NAME" x="112.268" y="-8.636" size="1.778" layer="95"/>
<attribute name="VALUE" x="112.268" y="-10.795" size="1.778" layer="96"/>
</instance>
<instance part="LD20" gate="G$1" x="114.3" y="-7.62" smashed="yes" rot="R270">
<attribute name="NAME" x="112.268" y="-11.176" size="1.778" layer="95"/>
<attribute name="VALUE" x="112.268" y="-13.335" size="1.778" layer="96"/>
</instance>
<instance part="R28" gate="G$1" x="127" y="-2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="130.81" y="-4.0386" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="130.81" y="0.762" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R29" gate="G$1" x="127" y="-5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="130.81" y="-6.5786" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="130.81" y="-1.778" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R30" gate="G$1" x="127" y="-7.62" smashed="yes" rot="R180">
<attribute name="NAME" x="130.81" y="-9.1186" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="130.81" y="-4.318" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
<bus name="PWM[0..15]">
<segment>
<wire x1="144.78" y1="165.1" x2="144.78" y2="127" width="0.762" layer="92"/>
<label x="147.32" y="144.78" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="154.94" y1="177.8" x2="154.94" y2="101.6" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="A[0..4]">
<segment>
<wire x1="91.44" y1="144.78" x2="91.44" y2="134.62" width="0.762" layer="92"/>
<label x="88.9" y="144.78" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="109.22" y1="114.3" x2="129.54" y2="114.3" width="0.762" layer="92"/>
<label x="121.92" y="116.84" size="1.778" layer="95" rot="R180"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="H2" gate="G$1" pin="MOUNT"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="116.84" y1="-50.8" x2="116.84" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="137.16" y1="-48.26" x2="137.16" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="H4" gate="G$1" pin="MOUNT"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="H1" gate="G$1" pin="MOUNT"/>
<wire x1="106.68" y1="-50.8" x2="106.68" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="H3" gate="G$1" pin="MOUNT"/>
<wire x1="127" y1="-50.8" x2="127" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="172.72" y1="78.74" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="C1" gate="&gt;NAME" pin="1"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="215.9" y1="-10.16" x2="198.12" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="4"/>
<wire x1="198.12" y1="-10.16" x2="187.96" y2="-10.16" width="0.1524" layer="91"/>
<junction x="198.12" y="-10.16"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="215.9" y1="-25.4" x2="198.12" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="4"/>
<wire x1="198.12" y1="-25.4" x2="187.96" y2="-25.4" width="0.1524" layer="91"/>
<junction x="198.12" y="-25.4"/>
</segment>
<segment>
<pinref part="JP11" gate="G$1" pin="4"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="198.12" y1="81.28" x2="195.58" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="15.24" y1="-30.48" x2="17.78" y2="-30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J12" gate="J" pin="1"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="175.26" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="172.72" y1="38.1" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="38.1"/>
</segment>
<segment>
<pinref part="J13" gate="J" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="175.26" y1="12.7" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="172.72" y1="15.24" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
<junction x="172.72" y="12.7"/>
</segment>
<segment>
<pinref part="J14" gate="G$1" pin="4"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="210.82" y1="45.72" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J15" gate="G$1" pin="4"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="210.82" y1="20.32" x2="213.36" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP12" gate="G$1" pin="1"/>
<wire x1="200.66" y1="177.8" x2="198.12" y2="177.8" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="198.12" y1="177.8" x2="198.12" y2="175.26" width="0.1524" layer="91"/>
<pinref part="JP12" gate="G$1" pin="4"/>
<wire x1="198.12" y1="175.26" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<wire x1="198.12" y1="172.72" x2="198.12" y2="170.18" width="0.1524" layer="91"/>
<wire x1="198.12" y1="170.18" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<wire x1="200.66" y1="170.18" x2="198.12" y2="170.18" width="0.1524" layer="91"/>
<junction x="198.12" y="170.18"/>
<pinref part="JP12" gate="G$1" pin="3"/>
<wire x1="200.66" y1="172.72" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<junction x="198.12" y="172.72"/>
<pinref part="JP12" gate="G$1" pin="2"/>
<wire x1="200.66" y1="175.26" x2="198.12" y2="175.26" width="0.1524" layer="91"/>
<junction x="198.12" y="175.26"/>
</segment>
<segment>
<pinref part="JP13" gate="G$1" pin="1"/>
<wire x1="200.66" y1="154.94" x2="198.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="198.12" y1="154.94" x2="198.12" y2="152.4" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="JP13" gate="G$1" pin="2"/>
<wire x1="198.12" y1="152.4" x2="198.12" y2="149.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="149.86" x2="198.12" y2="147.32" width="0.1524" layer="91"/>
<wire x1="198.12" y1="147.32" x2="198.12" y2="144.78" width="0.1524" layer="91"/>
<wire x1="200.66" y1="152.4" x2="198.12" y2="152.4" width="0.1524" layer="91"/>
<junction x="198.12" y="152.4"/>
<pinref part="JP13" gate="G$1" pin="3"/>
<wire x1="200.66" y1="149.86" x2="198.12" y2="149.86" width="0.1524" layer="91"/>
<junction x="198.12" y="149.86"/>
<pinref part="JP13" gate="G$1" pin="4"/>
<wire x1="200.66" y1="147.32" x2="198.12" y2="147.32" width="0.1524" layer="91"/>
<junction x="198.12" y="147.32"/>
</segment>
<segment>
<pinref part="JP14" gate="G$1" pin="1"/>
<wire x1="200.66" y1="132.08" x2="198.12" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="198.12" y1="132.08" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
<pinref part="JP14" gate="G$1" pin="2"/>
<wire x1="198.12" y1="129.54" x2="198.12" y2="127" width="0.1524" layer="91"/>
<wire x1="198.12" y1="127" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="198.12" y1="124.46" x2="198.12" y2="121.92" width="0.1524" layer="91"/>
<wire x1="200.66" y1="129.54" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
<junction x="198.12" y="129.54"/>
<pinref part="JP14" gate="G$1" pin="3"/>
<wire x1="200.66" y1="127" x2="198.12" y2="127" width="0.1524" layer="91"/>
<junction x="198.12" y="127"/>
<pinref part="JP14" gate="G$1" pin="4"/>
<wire x1="200.66" y1="124.46" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<junction x="198.12" y="124.46"/>
</segment>
<segment>
<pinref part="JP15" gate="G$1" pin="1"/>
<wire x1="200.66" y1="109.22" x2="198.12" y2="109.22" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="198.12" y1="109.22" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<pinref part="JP15" gate="G$1" pin="2"/>
<wire x1="198.12" y1="106.68" x2="198.12" y2="104.14" width="0.1524" layer="91"/>
<wire x1="198.12" y1="104.14" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<wire x1="198.12" y1="101.6" x2="198.12" y2="99.06" width="0.1524" layer="91"/>
<wire x1="200.66" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<junction x="198.12" y="106.68"/>
<pinref part="JP15" gate="G$1" pin="3"/>
<wire x1="200.66" y1="104.14" x2="198.12" y2="104.14" width="0.1524" layer="91"/>
<junction x="198.12" y="104.14"/>
<pinref part="JP15" gate="G$1" pin="4"/>
<wire x1="200.66" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<junction x="198.12" y="101.6"/>
</segment>
<segment>
<wire x1="109.22" y1="71.12" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="114.3" y1="76.2" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<wire x1="114.3" y1="68.58" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="119.38" y1="81.28" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<wire x1="119.38" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<junction x="114.3" y="68.58"/>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="119.38" y1="68.58" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
<junction x="119.38" y="68.58"/>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="124.46" y1="76.2" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
<wire x1="124.46" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="129.54" y1="68.58" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="124.46" y="68.58"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="#OE"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="EXTCLK"/>
<wire x1="99.06" y1="147.32" x2="99.06" y2="152.4" width="0.1524" layer="91"/>
<junction x="99.06" y="152.4"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="157.48" y1="-10.16" x2="157.48" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="149.86" y1="-2.54" x2="149.86" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-10.16" x2="157.48" y2="-10.16" width="0.1524" layer="91"/>
<junction x="157.48" y="-10.16"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="J16" gate="G$1" pin="2"/>
<wire x1="218.44" y1="60.96" x2="226.06" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="IC1" gate="G$1" pin="EP_GND"/>
<wire x1="-15.24" y1="167.64" x2="-15.24" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="IC2" gate="G$1" pin="EP_GND"/>
<wire x1="-15.24" y1="99.06" x2="-15.24" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="48.26" y1="124.46" x2="48.26" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="55.88" y1="124.46" x2="48.26" y2="124.46" width="0.1524" layer="91"/>
<junction x="48.26" y="124.46"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="50.8" y1="55.88" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="58.42" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<junction x="50.8" y="53.34"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="J6" gate="G$1" pin="4"/>
<wire x1="177.8" y1="-25.4" x2="160.02" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="160.02" y1="-25.4" x2="149.86" y2="-25.4" width="0.1524" layer="91"/>
<junction x="160.02" y="-25.4"/>
</segment>
<segment>
<pinref part="J4" gate="J" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="101.6" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<pinref part="J4" gate="J" pin="7"/>
<wire x1="101.6" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="35.56" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<junction x="104.14" y="20.32"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="GND"/>
<wire x1="68.58" y1="-17.78" x2="71.12" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+19" gate="1" pin="+5V"/>
<pinref part="J10" gate="G$1" pin="3"/>
<wire x1="213.36" y1="-12.7" x2="198.12" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="3"/>
<wire x1="198.12" y1="-12.7" x2="187.96" y2="-12.7" width="0.1524" layer="91"/>
<junction x="198.12" y="-12.7"/>
</segment>
<segment>
<pinref part="J14" gate="G$1" pin="3"/>
<pinref part="P+16" gate="1" pin="+5V"/>
<wire x1="210.82" y1="43.18" x2="208.28" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J15" gate="G$1" pin="3"/>
<pinref part="P+17" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="C1" gate="&gt;NAME" pin="2"/>
<wire x1="172.72" y1="86.36" x2="172.72" y2="88.9" width="0.1524" layer="91"/>
<pinref part="JP11" gate="G$1" pin="1"/>
<wire x1="172.72" y1="88.9" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
<pinref part="JP10" gate="G$1" pin="4"/>
<pinref part="JP10" gate="G$1" pin="3"/>
<wire x1="182.88" y1="88.9" x2="198.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="182.88" y1="101.6" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP10" gate="G$1" pin="2"/>
<wire x1="182.88" y1="104.14" x2="182.88" y2="106.68" width="0.1524" layer="91"/>
<junction x="182.88" y="104.14"/>
<pinref part="JP10" gate="G$1" pin="1"/>
<wire x1="182.88" y1="106.68" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<junction x="182.88" y="106.68"/>
<wire x1="182.88" y1="88.9" x2="182.88" y2="101.6" width="0.1524" layer="91"/>
<junction x="182.88" y="88.9"/>
<junction x="182.88" y="101.6"/>
<pinref part="JP9" gate="G$1" pin="4"/>
<pinref part="JP9" gate="G$1" pin="3"/>
<wire x1="182.88" y1="124.46" x2="182.88" y2="127" width="0.1524" layer="91"/>
<pinref part="JP9" gate="G$1" pin="2"/>
<wire x1="182.88" y1="127" x2="182.88" y2="129.54" width="0.1524" layer="91"/>
<junction x="182.88" y="127"/>
<pinref part="JP9" gate="G$1" pin="1"/>
<wire x1="182.88" y1="129.54" x2="182.88" y2="132.08" width="0.1524" layer="91"/>
<junction x="182.88" y="129.54"/>
<wire x1="182.88" y1="109.22" x2="182.88" y2="124.46" width="0.1524" layer="91"/>
<junction x="182.88" y="109.22"/>
<junction x="182.88" y="124.46"/>
<pinref part="JP8" gate="G$1" pin="4"/>
<pinref part="JP8" gate="G$1" pin="3"/>
<wire x1="182.88" y1="147.32" x2="182.88" y2="149.86" width="0.1524" layer="91"/>
<pinref part="JP8" gate="G$1" pin="2"/>
<wire x1="182.88" y1="149.86" x2="182.88" y2="152.4" width="0.1524" layer="91"/>
<junction x="182.88" y="149.86"/>
<pinref part="JP8" gate="G$1" pin="1"/>
<wire x1="182.88" y1="152.4" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<junction x="182.88" y="152.4"/>
<wire x1="182.88" y1="132.08" x2="182.88" y2="147.32" width="0.1524" layer="91"/>
<junction x="182.88" y="132.08"/>
<junction x="182.88" y="147.32"/>
<pinref part="JP7" gate="G$1" pin="4"/>
<pinref part="JP7" gate="G$1" pin="3"/>
<wire x1="182.88" y1="170.18" x2="182.88" y2="172.72" width="0.1524" layer="91"/>
<pinref part="JP7" gate="G$1" pin="2"/>
<wire x1="182.88" y1="172.72" x2="182.88" y2="175.26" width="0.1524" layer="91"/>
<junction x="182.88" y="172.72"/>
<pinref part="JP7" gate="G$1" pin="1"/>
<wire x1="182.88" y1="175.26" x2="182.88" y2="177.8" width="0.1524" layer="91"/>
<junction x="182.88" y="175.26"/>
<wire x1="182.88" y1="154.94" x2="182.88" y2="170.18" width="0.1524" layer="91"/>
<junction x="182.88" y="154.94"/>
<junction x="182.88" y="170.18"/>
<pinref part="P+14" gate="1" pin="+5V"/>
<wire x1="165.1" y1="88.9" x2="172.72" y2="88.9" width="0.1524" layer="91"/>
<junction x="172.72" y="88.9"/>
<pinref part="SJ10" gate="1" pin="2"/>
<wire x1="182.88" y1="76.2" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+13" gate="1" pin="+5V"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="157.48" y1="15.24" x2="157.48" y2="12.7" width="0.1524" layer="91"/>
<wire x1="157.48" y1="12.7" x2="149.86" y2="12.7" width="0.1524" layer="91"/>
<junction x="157.48" y="12.7"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="149.86" y1="12.7" x2="149.86" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+6" gate="1" pin="+5V"/>
<pinref part="SJ5" gate="1" pin="1"/>
<wire x1="30.48" y1="22.86" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<label x="27.94" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="CD+"/>
<wire x1="96.52" y1="-17.78" x2="104.14" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="P+12" gate="1" pin="+5V"/>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="104.14" y1="-17.78" x2="134.62" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-17.78" x2="139.7" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-10.16" x2="104.14" y2="-17.78" width="0.1524" layer="91"/>
<junction x="104.14" y="-17.78"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="132.08" y1="0" x2="134.62" y2="0" width="0.1524" layer="91"/>
<wire x1="134.62" y1="0" x2="134.62" y2="-2.54" width="0.1524" layer="91"/>
<junction x="134.62" y="-17.78"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="134.62" y1="-2.54" x2="134.62" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-5.08" x2="134.62" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-7.62" x2="134.62" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-2.54" x2="134.62" y2="-2.54" width="0.1524" layer="91"/>
<junction x="134.62" y="-2.54"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="132.08" y1="-5.08" x2="134.62" y2="-5.08" width="0.1524" layer="91"/>
<junction x="134.62" y="-5.08"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="132.08" y1="-7.62" x2="134.62" y2="-7.62" width="0.1524" layer="91"/>
<junction x="134.62" y="-7.62"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="P+18" gate="G$1" pin="3.3V"/>
<wire x1="212.09" y1="-27.94" x2="213.36" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="3"/>
<pinref part="J11" gate="G$1" pin="3"/>
<wire x1="198.12" y1="-27.94" x2="187.96" y2="-27.94" width="0.1524" layer="91"/>
<label x="187.96" y="-27.94" size="1.778" layer="95"/>
<wire x1="212.09" y1="-27.94" x2="198.12" y2="-27.94" width="0.1524" layer="91"/>
<junction x="212.09" y="-27.94"/>
<junction x="198.12" y="-27.94"/>
</segment>
<segment>
<pinref part="P+3" gate="G$1" pin="3.3V"/>
<pinref part="P1" gate="P$1" pin="3V3"/>
<wire x1="-12.7" y1="22.86" x2="-10.16" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+10" gate="G$1" pin="3.3V"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="93.98" y1="165.1" x2="99.06" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SJ8" gate="1" pin="1"/>
<wire x1="104.14" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<pinref part="SJ9" gate="1" pin="1"/>
<pinref part="P+9" gate="G$1" pin="3.3V"/>
<wire x1="91.44" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<junction x="96.52" y="76.2"/>
</segment>
<segment>
<pinref part="P+1" gate="G$1" pin="3.3V"/>
<pinref part="LD1" gate="G$1" pin="A"/>
<wire x1="-134.62" y1="185.42" x2="-132.08" y2="185.42" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="185.42" x2="-121.92" y2="185.42" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="185.42" x2="-132.08" y2="172.72" width="0.1524" layer="91"/>
<junction x="-132.08" y="185.42"/>
<pinref part="LD2" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="172.72" x2="-121.92" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="172.72" x2="-132.08" y2="160.02" width="0.1524" layer="91"/>
<junction x="-132.08" y="172.72"/>
<pinref part="LD3" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="160.02" x2="-121.92" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="160.02" x2="-132.08" y2="147.32" width="0.1524" layer="91"/>
<junction x="-132.08" y="160.02"/>
<pinref part="LD4" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="147.32" x2="-121.92" y2="147.32" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="147.32" x2="-132.08" y2="134.62" width="0.1524" layer="91"/>
<junction x="-132.08" y="147.32"/>
<pinref part="LD5" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="134.62" x2="-121.92" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="134.62" x2="-132.08" y2="121.92" width="0.1524" layer="91"/>
<junction x="-132.08" y="134.62"/>
<pinref part="LD6" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="121.92" x2="-121.92" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="121.92" x2="-132.08" y2="109.22" width="0.1524" layer="91"/>
<junction x="-132.08" y="121.92"/>
<pinref part="LD7" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="109.22" x2="-121.92" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="109.22" x2="-132.08" y2="96.52" width="0.1524" layer="91"/>
<junction x="-132.08" y="109.22"/>
<pinref part="LD8" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="96.52" x2="-121.92" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+2" gate="G$1" pin="3.3V"/>
<pinref part="LD9" gate="G$1" pin="A"/>
<wire x1="-134.62" y1="71.12" x2="-132.08" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="71.12" x2="-121.92" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="71.12" x2="-132.08" y2="58.42" width="0.1524" layer="91"/>
<junction x="-132.08" y="71.12"/>
<pinref part="LD10" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="58.42" x2="-121.92" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="58.42" x2="-132.08" y2="45.72" width="0.1524" layer="91"/>
<junction x="-132.08" y="58.42"/>
<pinref part="LD11" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="45.72" x2="-121.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="45.72" x2="-132.08" y2="33.02" width="0.1524" layer="91"/>
<junction x="-132.08" y="45.72"/>
<pinref part="LD12" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="33.02" x2="-121.92" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="33.02" x2="-132.08" y2="20.32" width="0.1524" layer="91"/>
<junction x="-132.08" y="33.02"/>
<pinref part="LD13" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="20.32" x2="-121.92" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="20.32" x2="-132.08" y2="7.62" width="0.1524" layer="91"/>
<junction x="-132.08" y="20.32"/>
<pinref part="LD14" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="7.62" x2="-121.92" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="7.62" x2="-132.08" y2="-5.08" width="0.1524" layer="91"/>
<junction x="-132.08" y="7.62"/>
<pinref part="LD15" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="-5.08" x2="-121.92" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="-5.08" x2="-132.08" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-132.08" y="-5.08"/>
<pinref part="LD16" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="-17.78" x2="-121.92" y2="-17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+4" gate="G$1" pin="3.3V"/>
<pinref part="IC1" gate="G$1" pin="VDD"/>
<wire x1="-10.16" y1="170.18" x2="-10.16" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+5" gate="G$1" pin="3.3V"/>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<wire x1="-10.16" y1="101.6" x2="-10.16" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="G$1" pin="3.3V"/>
<pinref part="SJ6" gate="1" pin="2"/>
<wire x1="48.26" y1="149.86" x2="48.26" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+8" gate="G$1" pin="3.3V"/>
<pinref part="SJ7" gate="1" pin="2"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+15" gate="G$1" pin="3.3V"/>
<wire x1="173.99" y1="-27.94" x2="175.26" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="3"/>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="160.02" y1="-27.94" x2="149.86" y2="-27.94" width="0.1524" layer="91"/>
<label x="149.86" y="-27.94" size="1.778" layer="95"/>
<wire x1="173.99" y1="-27.94" x2="160.02" y2="-27.94" width="0.1524" layer="91"/>
<junction x="173.99" y="-27.94"/>
<junction x="160.02" y="-27.94"/>
</segment>
<segment>
<pinref part="J4" gate="J" pin="2"/>
<pinref part="P+11" gate="G$1" pin="3.3V"/>
<wire x1="101.6" y1="22.86" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="175.26" y1="-33.02" x2="187.96" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="198.12" y1="-33.02" x2="187.96" y2="-33.02" width="0.1524" layer="91"/>
<junction x="187.96" y="-33.02"/>
<label x="177.8" y="-33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="175.26" y1="-17.78" x2="187.96" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-17.78" x2="187.96" y2="-17.78" width="0.1524" layer="91"/>
<junction x="187.96" y="-17.78"/>
<label x="175.26" y="-17.78" size="1.778" layer="95"/>
<pinref part="J10" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="3"/>
<wire x1="-10.16" y1="12.7" x2="-22.86" y2="12.7" width="0.1524" layer="91"/>
<label x="-20.32" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="SCL"/>
<wire x1="99.06" y1="157.48" x2="78.74" y2="157.48" width="0.1524" layer="91"/>
<label x="78.74" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SMCLK/BC_CLK/SPI_CLK"/>
<wire x1="-45.72" y1="139.7" x2="-58.42" y2="139.7" width="0.1524" layer="91"/>
<label x="-58.42" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SMCLK/BC_CLK/SPI_CLK"/>
<wire x1="-45.72" y1="71.12" x2="-58.42" y2="71.12" width="0.1524" layer="91"/>
<label x="-55.88" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="137.16" y1="-33.02" x2="149.86" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="160.02" y1="-33.02" x2="149.86" y2="-33.02" width="0.1524" layer="91"/>
<junction x="149.86" y="-33.02"/>
<label x="139.7" y="-33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO20" class="0">
<segment>
<pinref part="J13" gate="J" pin="3"/>
<wire x1="195.58" y1="2.54" x2="182.88" y2="2.54" width="0.1524" layer="91"/>
<label x="185.42" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="20"/>
<wire x1="17.78" y1="-15.24" x2="30.48" y2="-15.24" width="0.1524" layer="91"/>
<label x="20.32" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO26" class="0">
<segment>
<pinref part="J12" gate="J" pin="2"/>
<wire x1="195.58" y1="25.4" x2="177.8" y2="25.4" width="0.1524" layer="91"/>
<label x="180.34" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="26"/>
<wire x1="-12.7" y1="-30.48" x2="-25.4" y2="-30.48" width="0.1524" layer="91"/>
<label x="-22.86" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO19" class="0">
<segment>
<pinref part="J12" gate="J" pin="3"/>
<wire x1="195.58" y1="27.94" x2="177.8" y2="27.94" width="0.1524" layer="91"/>
<label x="180.34" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="19"/>
<wire x1="-12.7" y1="-27.94" x2="-25.4" y2="-27.94" width="0.1524" layer="91"/>
<label x="-22.86" y="-27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="175.26" y1="-30.48" x2="187.96" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="198.12" y1="-30.48" x2="187.96" y2="-30.48" width="0.1524" layer="91"/>
<junction x="187.96" y="-30.48"/>
<label x="177.8" y="-30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="172.72" y1="-15.24" x2="187.96" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-15.24" x2="187.96" y2="-15.24" width="0.1524" layer="91"/>
<junction x="187.96" y="-15.24"/>
<label x="175.26" y="-15.24" size="1.778" layer="95"/>
<pinref part="J10" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="2"/>
<wire x1="-10.16" y1="15.24" x2="-22.86" y2="15.24" width="0.1524" layer="91"/>
<label x="-20.32" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="SDA"/>
<wire x1="99.06" y1="160.02" x2="78.74" y2="160.02" width="0.1524" layer="91"/>
<label x="78.74" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SMDATA/BC_DATA/SPI_MSIO/SPI_MISO"/>
<wire x1="-45.72" y1="142.24" x2="-58.42" y2="142.24" width="0.1524" layer="91"/>
<label x="-58.42" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SMDATA/BC_DATA/SPI_MSIO/SPI_MISO"/>
<wire x1="-45.72" y1="73.66" x2="-58.42" y2="73.66" width="0.1524" layer="91"/>
<label x="-55.88" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="137.16" y1="-30.48" x2="149.86" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="160.02" y1="-30.48" x2="149.86" y2="-30.48" width="0.1524" layer="91"/>
<junction x="149.86" y="-30.48"/>
<label x="139.7" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO21" class="0">
<segment>
<pinref part="J13" gate="J" pin="2"/>
<wire x1="195.58" y1="0" x2="185.42" y2="0" width="0.1524" layer="91"/>
<label x="185.42" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P$1" pin="21"/>
<wire x1="17.78" y1="-17.78" x2="30.48" y2="-17.78" width="0.1524" layer="91"/>
<label x="20.32" y="-17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J14" gate="G$1" pin="2"/>
<pinref part="J12" gate="J" pin="8"/>
<wire x1="210.82" y1="40.64" x2="195.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="210.82" y1="40.64" x2="223.52" y2="40.64" width="0.1524" layer="91"/>
<junction x="210.82" y="40.64"/>
<pinref part="LD27" gate="G$1" pin="A"/>
<wire x1="223.52" y1="40.64" x2="223.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="LD25" gate="G$1" pin="C"/>
<wire x1="223.52" y1="40.64" x2="223.52" y2="38.1" width="0.1524" layer="91"/>
<junction x="223.52" y="40.64"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="J15" gate="G$1" pin="2"/>
<pinref part="J13" gate="J" pin="8"/>
<wire x1="213.36" y1="15.24" x2="195.58" y2="15.24" width="0.1524" layer="91"/>
<wire x1="213.36" y1="15.24" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<pinref part="LD28" gate="G$1" pin="A"/>
<wire x1="223.52" y1="15.24" x2="223.52" y2="17.78" width="0.1524" layer="91"/>
<pinref part="LD26" gate="G$1" pin="C"/>
<wire x1="223.52" y1="15.24" x2="223.52" y2="12.7" width="0.1524" layer="91"/>
<junction x="223.52" y="15.24"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="J14" gate="G$1" pin="1"/>
<wire x1="210.82" y1="38.1" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<pinref part="J12" gate="J" pin="6"/>
<wire x1="210.82" y1="35.56" x2="195.58" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="223.52" y1="33.02" x2="210.82" y2="33.02" width="0.1524" layer="91"/>
<wire x1="210.82" y1="33.02" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<junction x="210.82" y="35.56"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="J15" gate="G$1" pin="1"/>
<wire x1="213.36" y1="12.7" x2="213.36" y2="10.16" width="0.1524" layer="91"/>
<pinref part="J13" gate="J" pin="6"/>
<wire x1="213.36" y1="10.16" x2="195.58" y2="10.16" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="223.52" y1="7.62" x2="213.36" y2="7.62" width="0.1524" layer="91"/>
<wire x1="213.36" y1="10.16" x2="213.36" y2="7.62" width="0.1524" layer="91"/>
<junction x="213.36" y="10.16"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J12" gate="J" pin="7"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="195.58" y1="38.1" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="190.5" y1="38.1" x2="182.88" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="190.5" y1="40.64" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<junction x="190.5" y="38.1"/>
</segment>
</net>
<net name="PWM0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM0"/>
<wire x1="139.7" y1="165.1" x2="144.78" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="167.64" y1="177.8" x2="190.5" y2="177.8" width="0.1524" layer="91"/>
<wire x1="154.94" y1="177.8" x2="167.64" y2="177.8" width="0.1524" layer="91"/>
<junction x="167.64" y="177.8"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM1"/>
<wire x1="139.7" y1="162.56" x2="144.78" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="154.94" y1="175.26" x2="167.64" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM2"/>
<wire x1="139.7" y1="160.02" x2="144.78" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="3"/>
<wire x1="154.94" y1="172.72" x2="167.64" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM3"/>
<wire x1="139.7" y1="157.48" x2="144.78" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="4"/>
<wire x1="154.94" y1="170.18" x2="167.64" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM4"/>
<wire x1="139.7" y1="154.94" x2="144.78" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="154.94" y1="154.94" x2="167.64" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM5"/>
<wire x1="139.7" y1="152.4" x2="144.78" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="2"/>
<wire x1="154.94" y1="152.4" x2="167.64" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM6"/>
<wire x1="139.7" y1="149.86" x2="144.78" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="3"/>
<wire x1="154.94" y1="149.86" x2="167.64" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM7"/>
<wire x1="139.7" y1="147.32" x2="144.78" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="4"/>
<wire x1="154.94" y1="147.32" x2="167.64" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM8"/>
<wire x1="139.7" y1="144.78" x2="144.78" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="1"/>
<wire x1="154.94" y1="132.08" x2="167.64" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM9" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM9"/>
<wire x1="139.7" y1="142.24" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="2"/>
<wire x1="154.94" y1="129.54" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM10" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM10"/>
<wire x1="139.7" y1="139.7" x2="144.78" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="3"/>
<wire x1="154.94" y1="127" x2="167.64" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM11" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM11"/>
<wire x1="139.7" y1="137.16" x2="144.78" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="4"/>
<wire x1="154.94" y1="124.46" x2="167.64" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM12" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM12"/>
<wire x1="139.7" y1="134.62" x2="144.78" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="1"/>
<wire x1="154.94" y1="109.22" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM13"/>
<wire x1="139.7" y1="132.08" x2="144.78" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="2"/>
<wire x1="154.94" y1="106.68" x2="167.64" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM14" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM14"/>
<wire x1="139.7" y1="129.54" x2="144.78" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="3"/>
<wire x1="154.94" y1="104.14" x2="167.64" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM15"/>
<wire x1="139.7" y1="127" x2="144.78" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="4"/>
<wire x1="154.94" y1="101.6" x2="167.64" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A0"/>
<wire x1="91.44" y1="144.78" x2="99.06" y2="144.78" width="0.1524" layer="91"/>
<label x="93.98" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="119.38" y1="96.52" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="96.52" y1="86.36" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="119.38" y1="91.44" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="119.38" y1="96.52" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<junction x="119.38" y="91.44"/>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="SJ8" gate="1" pin="2"/>
<label x="119.38" y="104.14" size="1.778" layer="95" rot="R270"/>
<junction x="119.38" y="96.52"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A1"/>
<wire x1="91.44" y1="142.24" x2="99.06" y2="142.24" width="0.1524" layer="91"/>
<label x="93.98" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="114.3" y1="86.36" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<wire x1="104.14" y1="86.36" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<junction x="114.3" y="91.44"/>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="SJ9" gate="1" pin="2"/>
<label x="114.3" y="104.14" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A2"/>
<wire x1="91.44" y1="139.7" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<label x="93.98" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="109.22" y1="81.28" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<label x="109.22" y="104.14" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A3"/>
<wire x1="91.44" y1="137.16" x2="99.06" y2="137.16" width="0.1524" layer="91"/>
<label x="93.98" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<label x="124.46" y="101.6" size="1.778" layer="95" rot="R90"/>
<wire x1="124.46" y1="86.36" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A4"/>
<wire x1="91.44" y1="134.62" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
<label x="93.98" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<label x="129.54" y="101.6" size="1.778" layer="95" rot="R90"/>
<wire x1="129.54" y1="81.28" x2="129.54" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A5"/>
<wire x1="86.36" y1="132.08" x2="99.06" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J13" gate="J" pin="5"/>
<wire x1="195.58" y1="7.62" x2="175.26" y2="7.62" width="0.1524" layer="91"/>
<wire x1="175.26" y1="7.62" x2="175.26" y2="5.08" width="0.1524" layer="91"/>
<wire x1="175.26" y1="5.08" x2="162.56" y2="5.08" width="0.1524" layer="91"/>
<pinref part="J12" gate="J" pin="5"/>
<wire x1="195.58" y1="33.02" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<wire x1="162.56" y1="5.08" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<wire x1="162.56" y1="33.02" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<junction x="162.56" y="33.02"/>
<pinref part="J16" gate="G$1" pin="1"/>
<wire x1="162.56" y1="63.5" x2="182.88" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SJ10" gate="1" pin="1"/>
<wire x1="182.88" y1="63.5" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<wire x1="182.88" y1="66.04" x2="182.88" y2="63.5" width="0.1524" layer="91"/>
<junction x="182.88" y="63.5"/>
<pinref part="J4" gate="J" pin="8"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<wire x1="101.6" y1="63.5" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<junction x="162.56" y="63.5"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="J13" gate="J" pin="7"/>
<wire x1="195.58" y1="12.7" x2="190.5" y2="12.7" width="0.1524" layer="91"/>
<wire x1="190.5" y1="12.7" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="185.42" y1="12.7" x2="182.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="172.72" y1="7.62" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<wire x1="172.72" y1="10.16" x2="185.42" y2="10.16" width="0.1524" layer="91"/>
<wire x1="185.42" y1="10.16" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
<junction x="185.42" y="12.7"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="190.5" y1="15.24" x2="190.5" y2="12.7" width="0.1524" layer="91"/>
<junction x="190.5" y="12.7"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="C"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="1_LED1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED1"/>
<wire x1="-45.72" y1="137.16" x2="-58.42" y2="137.16" width="0.1524" layer="91"/>
<label x="-58.42" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="185.42" x2="-91.44" y2="185.42" width="0.1524" layer="91"/>
<label x="-101.6" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_LED2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED2"/>
<wire x1="-45.72" y1="134.62" x2="-58.42" y2="134.62" width="0.1524" layer="91"/>
<label x="-58.42" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="172.72" x2="-91.44" y2="172.72" width="0.1524" layer="91"/>
<label x="-101.6" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_CS1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS1"/>
<wire x1="-7.62" y1="165.1" x2="-7.62" y2="175.26" width="0.1524" layer="91"/>
<label x="-7.62" y="167.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH1" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="180.34" x2="-91.44" y2="180.34" width="0.1524" layer="91"/>
<label x="-101.6" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="1"/>
<wire x1="-73.66" y1="116.84" x2="-73.66" y2="124.46" width="0.1524" layer="91"/>
<label x="-73.66" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS2"/>
<wire x1="-5.08" y1="165.1" x2="-5.08" y2="175.26" width="0.1524" layer="91"/>
<label x="-5.08" y="167.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH2" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="167.64" x2="-91.44" y2="167.64" width="0.1524" layer="91"/>
<label x="-101.6" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="2"/>
<wire x1="-71.12" y1="116.84" x2="-71.12" y2="124.46" width="0.1524" layer="91"/>
<label x="-71.12" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS3"/>
<wire x1="-2.54" y1="165.1" x2="-2.54" y2="175.26" width="0.1524" layer="91"/>
<label x="-2.54" y="167.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH3" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="154.94" x2="-91.44" y2="154.94" width="0.1524" layer="91"/>
<label x="-104.14" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="3"/>
<wire x1="-68.58" y1="116.84" x2="-68.58" y2="124.46" width="0.1524" layer="91"/>
<label x="-68.58" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS4"/>
<wire x1="0" y1="165.1" x2="0" y2="175.26" width="0.1524" layer="91"/>
<label x="0" y="167.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH4" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="142.24" x2="-91.44" y2="142.24" width="0.1524" layer="91"/>
<label x="-101.6" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="4"/>
<wire x1="-66.04" y1="116.84" x2="-66.04" y2="124.46" width="0.1524" layer="91"/>
<label x="-66.04" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS5"/>
<wire x1="30.48" y1="147.32" x2="40.64" y2="147.32" width="0.1524" layer="91"/>
<label x="33.02" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH5" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="129.54" x2="-91.44" y2="129.54" width="0.1524" layer="91"/>
<label x="-104.14" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="5"/>
<wire x1="-63.5" y1="116.84" x2="-63.5" y2="124.46" width="0.1524" layer="91"/>
<label x="-63.5" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS6"/>
<wire x1="30.48" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<label x="33.02" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH6" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="116.84" x2="-91.44" y2="116.84" width="0.1524" layer="91"/>
<label x="-101.6" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="6"/>
<wire x1="-60.96" y1="116.84" x2="-60.96" y2="124.46" width="0.1524" layer="91"/>
<label x="-60.96" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS7"/>
<wire x1="30.48" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<label x="33.02" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH7" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="104.14" x2="-91.44" y2="104.14" width="0.1524" layer="91"/>
<label x="-101.6" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="7"/>
<wire x1="-58.42" y1="116.84" x2="-58.42" y2="124.46" width="0.1524" layer="91"/>
<label x="-58.42" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_CS8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CS8"/>
<wire x1="30.48" y1="139.7" x2="40.64" y2="139.7" width="0.1524" layer="91"/>
<label x="33.02" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH8" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="91.44" x2="-91.44" y2="91.44" width="0.1524" layer="91"/>
<label x="-101.6" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J" pin="8"/>
<wire x1="-55.88" y1="116.84" x2="-55.88" y2="124.46" width="0.1524" layer="91"/>
<label x="-55.88" y="116.84" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="1_LED3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED3"/>
<wire x1="-15.24" y1="119.38" x2="-15.24" y2="111.76" width="0.1524" layer="91"/>
<label x="-15.24" y="111.76" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="160.02" x2="-91.44" y2="160.02" width="0.1524" layer="91"/>
<label x="-101.6" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_LED4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED4"/>
<wire x1="-12.7" y1="119.38" x2="-12.7" y2="111.76" width="0.1524" layer="91"/>
<label x="-12.7" y="111.76" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="147.32" x2="-91.44" y2="147.32" width="0.1524" layer="91"/>
<label x="-101.6" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_LED5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED5"/>
<wire x1="-10.16" y1="119.38" x2="-10.16" y2="111.76" width="0.1524" layer="91"/>
<label x="-10.16" y="111.76" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="134.62" x2="-91.44" y2="134.62" width="0.1524" layer="91"/>
<label x="-101.6" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_LED6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED6"/>
<wire x1="-7.62" y1="119.38" x2="-7.62" y2="111.76" width="0.1524" layer="91"/>
<label x="-7.62" y="111.76" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="121.92" x2="-91.44" y2="121.92" width="0.1524" layer="91"/>
<label x="-101.6" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_LED7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED7"/>
<wire x1="-5.08" y1="119.38" x2="-5.08" y2="111.76" width="0.1524" layer="91"/>
<label x="-5.08" y="111.76" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="109.22" x2="-91.44" y2="109.22" width="0.1524" layer="91"/>
<label x="-101.6" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="1_LED8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED8"/>
<wire x1="-2.54" y1="119.38" x2="-2.54" y2="111.76" width="0.1524" layer="91"/>
<label x="-2.54" y="111.76" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="96.52" x2="-91.44" y2="96.52" width="0.1524" layer="91"/>
<label x="-101.6" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="LD2" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="LD3" gate="G$1" pin="C"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="LD4" gate="G$1" pin="C"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="LD5" gate="G$1" pin="C"/>
<pinref part="R5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="LD6" gate="G$1" pin="C"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="LD7" gate="G$1" pin="C"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="LD8" gate="G$1" pin="C"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="LD9" gate="G$1" pin="C"/>
<pinref part="R9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="LD10" gate="G$1" pin="C"/>
<pinref part="R10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="LD11" gate="G$1" pin="C"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="LD12" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="LD13" gate="G$1" pin="C"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="LD14" gate="G$1" pin="C"/>
<pinref part="R14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="LD15" gate="G$1" pin="C"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LD16" gate="G$1" pin="C"/>
<pinref part="R16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="2_LED1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED1"/>
<wire x1="-45.72" y1="68.58" x2="-58.42" y2="68.58" width="0.1524" layer="91"/>
<label x="-55.88" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="71.12" x2="-91.44" y2="71.12" width="0.1524" layer="91"/>
<label x="-101.6" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED2" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED2"/>
<wire x1="-45.72" y1="66.04" x2="-58.42" y2="66.04" width="0.1524" layer="91"/>
<label x="-55.88" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="58.42" x2="-91.44" y2="58.42" width="0.1524" layer="91"/>
<label x="-101.6" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED3"/>
<wire x1="-15.24" y1="50.8" x2="-15.24" y2="38.1" width="0.1524" layer="91"/>
<label x="-15.24" y="43.18" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="45.72" x2="-91.44" y2="45.72" width="0.1524" layer="91"/>
<label x="-101.6" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED4" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED4"/>
<wire x1="-12.7" y1="50.8" x2="-12.7" y2="38.1" width="0.1524" layer="91"/>
<label x="-12.7" y="43.18" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="33.02" x2="-91.44" y2="33.02" width="0.1524" layer="91"/>
<label x="-101.6" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED5" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED5"/>
<wire x1="-10.16" y1="50.8" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<label x="-10.16" y="43.18" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="20.32" x2="-91.44" y2="20.32" width="0.1524" layer="91"/>
<label x="-101.6" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED6" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED6"/>
<wire x1="-7.62" y1="50.8" x2="-7.62" y2="38.1" width="0.1524" layer="91"/>
<label x="-7.62" y="43.18" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="7.62" x2="-91.44" y2="7.62" width="0.1524" layer="91"/>
<label x="-101.6" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED7" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED7"/>
<wire x1="-5.08" y1="50.8" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<label x="-5.08" y="43.18" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="-5.08" x2="-91.44" y2="-5.08" width="0.1524" layer="91"/>
<label x="-101.6" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_LED8" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LED8"/>
<wire x1="-2.54" y1="50.8" x2="-2.54" y2="38.1" width="0.1524" layer="91"/>
<label x="-2.54" y="43.18" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="-17.78" x2="-91.44" y2="-17.78" width="0.1524" layer="91"/>
<label x="-101.6" y="-17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="2_CS1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS1"/>
<wire x1="-7.62" y1="96.52" x2="-7.62" y2="104.14" width="0.1524" layer="91"/>
<label x="-7.62" y="96.52" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH9" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="66.04" x2="-91.44" y2="66.04" width="0.1524" layer="91"/>
<label x="-101.6" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="1"/>
<wire x1="-71.12" y1="48.26" x2="-71.12" y2="55.88" width="0.1524" layer="91"/>
<label x="-71.12" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS2" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS2"/>
<wire x1="-5.08" y1="96.52" x2="-5.08" y2="104.14" width="0.1524" layer="91"/>
<label x="-5.08" y="96.52" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH10" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="53.34" x2="-91.44" y2="53.34" width="0.1524" layer="91"/>
<label x="-101.6" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="2"/>
<wire x1="-68.58" y1="48.26" x2="-68.58" y2="55.88" width="0.1524" layer="91"/>
<label x="-68.58" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS3"/>
<wire x1="-2.54" y1="96.52" x2="-2.54" y2="104.14" width="0.1524" layer="91"/>
<label x="-2.54" y="96.52" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH11" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="40.64" x2="-91.44" y2="40.64" width="0.1524" layer="91"/>
<label x="-101.6" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="3"/>
<wire x1="-66.04" y1="48.26" x2="-66.04" y2="55.88" width="0.1524" layer="91"/>
<label x="-66.04" y="50.8" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS4" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS4"/>
<wire x1="0" y1="96.52" x2="0" y2="104.14" width="0.1524" layer="91"/>
<label x="0" y="96.52" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="TOUCH12" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="27.94" x2="-91.44" y2="27.94" width="0.1524" layer="91"/>
<label x="-101.6" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="4"/>
<wire x1="-63.5" y1="48.26" x2="-63.5" y2="55.88" width="0.1524" layer="91"/>
<label x="-63.5" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS5" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS5"/>
<wire x1="30.48" y1="78.74" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<label x="33.02" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH13" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="15.24" x2="-91.44" y2="15.24" width="0.1524" layer="91"/>
<label x="-101.6" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="5"/>
<wire x1="-60.96" y1="48.26" x2="-60.96" y2="55.88" width="0.1524" layer="91"/>
<label x="-60.96" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS6" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS6"/>
<wire x1="30.48" y1="76.2" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<label x="33.02" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH14" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="2.54" x2="-91.44" y2="2.54" width="0.1524" layer="91"/>
<label x="-101.6" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="6"/>
<wire x1="-58.42" y1="48.26" x2="-58.42" y2="55.88" width="0.1524" layer="91"/>
<label x="-58.42" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS7" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS7"/>
<wire x1="30.48" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<label x="33.02" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH15" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="-10.16" x2="-91.44" y2="-10.16" width="0.1524" layer="91"/>
<label x="-101.6" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="7"/>
<wire x1="-55.88" y1="48.26" x2="-55.88" y2="55.88" width="0.1524" layer="91"/>
<label x="-55.88" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="2_CS8" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS8"/>
<wire x1="30.48" y1="71.12" x2="40.64" y2="71.12" width="0.1524" layer="91"/>
<label x="33.02" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TOUCH16" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="-22.86" x2="-91.44" y2="-22.86" width="0.1524" layer="91"/>
<label x="-101.6" y="-22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="J" pin="8"/>
<wire x1="-53.34" y1="48.26" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<label x="-53.34" y="48.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="SJ6" gate="1" pin="1"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="48.26" y1="137.16" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="ADDR_COMM"/>
<wire x1="30.48" y1="137.16" x2="48.26" y2="137.16" width="0.1524" layer="91"/>
<junction x="48.26" y="137.16"/>
<pinref part="R19" gate="G$1" pin="3"/>
<wire x1="50.8" y1="137.16" x2="48.26" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="SJ7" gate="1" pin="1"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="ADDR_COMM"/>
<wire x1="50.8" y1="68.58" x2="50.8" y2="66.04" width="0.1524" layer="91"/>
<wire x1="30.48" y1="68.58" x2="50.8" y2="68.58" width="0.1524" layer="91"/>
<junction x="50.8" y="68.58"/>
<pinref part="R21" gate="G$1" pin="3"/>
<wire x1="53.34" y1="68.58" x2="50.8" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALERT_A" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="4"/>
<wire x1="-10.16" y1="10.16" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<label x="-20.32" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="ALERT#/BC_IRQ#"/>
<wire x1="30.48" y1="134.62" x2="40.64" y2="134.62" width="0.1524" layer="91"/>
<label x="33.02" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET_A" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="17"/>
<wire x1="-10.16" y1="5.08" x2="-22.86" y2="5.08" width="0.1524" layer="91"/>
<label x="-20.32" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="RESET"/>
<wire x1="-12.7" y1="165.1" x2="-12.7" y2="177.8" width="0.1524" layer="91"/>
<label x="-12.7" y="175.26" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ALERT_B" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="27"/>
<wire x1="-10.16" y1="2.54" x2="-22.86" y2="2.54" width="0.1524" layer="91"/>
<label x="-20.32" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="ALERT#/BC_IRQ#"/>
<wire x1="30.48" y1="66.04" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<label x="33.02" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET_B" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="22"/>
<wire x1="-10.16" y1="0" x2="-22.86" y2="0" width="0.1524" layer="91"/>
<label x="-20.32" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="RESET"/>
<wire x1="-12.7" y1="96.52" x2="-12.7" y2="106.68" width="0.1524" layer="91"/>
<label x="-12.7" y="104.14" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SPI_CS#"/>
<pinref part="SJ1" gate="1" pin="1"/>
<wire x1="-45.72" y1="147.32" x2="-50.8" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="WAKE/SPI_MOSI"/>
<pinref part="SJ2" gate="1" pin="1"/>
<wire x1="-45.72" y1="144.78" x2="-50.8" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SPI_CS#"/>
<pinref part="SJ3" gate="1" pin="1"/>
<wire x1="-45.72" y1="78.74" x2="-50.8" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="WAKE/SPI_MOSI"/>
<pinref part="SJ4" gate="1" pin="1"/>
<wire x1="-45.72" y1="76.2" x2="-50.8" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="5V"/>
<pinref part="SJ5" gate="1" pin="2"/>
<wire x1="17.78" y1="22.86" x2="15.24" y2="22.86" width="0.1524" layer="91"/>
<label x="15.24" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="58.42" y1="60.96" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="J12" gate="J" pin="4"/>
<wire x1="195.58" y1="30.48" x2="165.1" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="157.48" y1="0" x2="157.48" y2="2.54" width="0.1524" layer="91"/>
<wire x1="157.48" y1="2.54" x2="157.48" y2="5.08" width="0.1524" layer="91"/>
<junction x="157.48" y="2.54"/>
<pinref part="R34" gate="G$1" pin="3"/>
<wire x1="154.94" y1="2.54" x2="157.48" y2="2.54" width="0.1524" layer="91"/>
<pinref part="J13" gate="J" pin="4"/>
<wire x1="195.58" y1="5.08" x2="177.8" y2="5.08" width="0.1524" layer="91"/>
<wire x1="177.8" y1="5.08" x2="177.8" y2="2.54" width="0.1524" layer="91"/>
<wire x1="177.8" y1="2.54" x2="177.8" y2="0" width="0.1524" layer="91"/>
<wire x1="157.48" y1="2.54" x2="165.1" y2="2.54" width="0.1524" layer="91"/>
<junction x="177.8" y="2.54"/>
<wire x1="165.1" y1="2.54" x2="177.8" y2="2.54" width="0.1524" layer="91"/>
<wire x1="165.1" y1="30.48" x2="165.1" y2="2.54" width="0.1524" layer="91"/>
<junction x="165.1" y="2.54"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="J3" gate="J" pin="3"/>
<wire x1="76.2" y1="25.4" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<wire x1="73.66" y1="25.4" x2="73.66" y2="27.94" width="0.1524" layer="91"/>
<pinref part="J3" gate="J" pin="4"/>
<wire x1="73.66" y1="27.94" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J4" gate="J" pin="4"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="101.6" y1="27.94" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="4"/>
<wire x1="111.76" y1="27.94" x2="127" y2="27.94" width="0.1524" layer="91"/>
<junction x="111.76" y="27.94"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="154.94" y1="27.94" x2="154.94" y2="22.86" width="0.1524" layer="91"/>
<wire x1="154.94" y1="22.86" x2="134.62" y2="22.86" width="0.1524" layer="91"/>
<wire x1="134.62" y1="22.86" x2="134.62" y2="27.94" width="0.1524" layer="91"/>
<wire x1="134.62" y1="27.94" x2="127" y2="27.94" width="0.1524" layer="91"/>
<junction x="127" y="27.94"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="J4" gate="J" pin="3"/>
<wire x1="101.6" y1="25.4" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<wire x1="109.22" y1="25.4" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="109.22" y1="33.02" x2="111.76" y2="33.02" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="111.76" y1="33.02" x2="124.46" y2="33.02" width="0.1524" layer="91"/>
<junction x="111.76" y="33.02"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="124.46" y1="33.02" x2="127" y2="33.02" width="0.1524" layer="91"/>
<wire x1="154.94" y1="38.1" x2="154.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="154.94" y1="33.02" x2="127" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GPIO16" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="16"/>
<wire x1="17.78" y1="-12.7" x2="30.48" y2="-12.7" width="0.1524" layer="91"/>
<label x="20.32" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J3" gate="J" pin="1"/>
<wire x1="76.2" y1="20.32" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
<label x="68.58" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO13" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="13"/>
<wire x1="-12.7" y1="-25.4" x2="-25.4" y2="-25.4" width="0.1524" layer="91"/>
<label x="-22.86" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J3" gate="J" pin="2"/>
<wire x1="76.2" y1="22.86" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<label x="68.58" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="IC3" gate="A" pin="O1"/>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="96.52" y1="0" x2="104.14" y2="0" width="0.1524" layer="91"/>
<pinref part="LD17" gate="G$1" pin="C"/>
<wire x1="104.14" y1="0" x2="111.76" y2="0" width="0.1524" layer="91"/>
<junction x="104.14" y="0"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC3" gate="A" pin="O2"/>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="96.52" y1="-2.54" x2="104.14" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="LD18" gate="G$1" pin="C"/>
<wire x1="104.14" y1="-2.54" x2="111.76" y2="-2.54" width="0.1524" layer="91"/>
<junction x="104.14" y="-2.54"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="IC3" gate="A" pin="O3"/>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="96.52" y1="-5.08" x2="104.14" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="LD19" gate="G$1" pin="C"/>
<wire x1="104.14" y1="-5.08" x2="111.76" y2="-5.08" width="0.1524" layer="91"/>
<junction x="104.14" y="-5.08"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="IC3" gate="A" pin="O4"/>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="96.52" y1="-7.62" x2="104.14" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="LD20" gate="G$1" pin="C"/>
<wire x1="104.14" y1="-7.62" x2="111.76" y2="-7.62" width="0.1524" layer="91"/>
<junction x="104.14" y="-7.62"/>
</segment>
</net>
<net name="GPIO06" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="6"/>
<wire x1="-12.7" y1="-22.86" x2="-25.4" y2="-22.86" width="0.1524" layer="91"/>
<label x="-22.86" y="-22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I2"/>
<wire x1="71.12" y1="-2.54" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
<label x="58.42" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO05" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="5"/>
<wire x1="-12.7" y1="-20.32" x2="-25.4" y2="-20.32" width="0.1524" layer="91"/>
<label x="-22.86" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I1"/>
<wire x1="71.12" y1="0" x2="55.88" y2="0" width="0.1524" layer="91"/>
<label x="58.42" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO12" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="12"/>
<wire x1="17.78" y1="-10.16" x2="30.48" y2="-10.16" width="0.1524" layer="91"/>
<label x="20.32" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I3"/>
<wire x1="71.12" y1="-5.08" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<label x="58.42" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO25" class="0">
<segment>
<pinref part="P1" gate="P$1" pin="25"/>
<wire x1="15.24" y1="0" x2="30.48" y2="0" width="0.1524" layer="91"/>
<label x="20.32" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I4"/>
<wire x1="71.12" y1="-7.62" x2="55.88" y2="-7.62" width="0.1524" layer="91"/>
<label x="58.42" y="-7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="LD27" gate="G$1" pin="C"/>
<pinref part="LD25" gate="G$1" pin="A"/>
<wire x1="231.14" y1="43.18" x2="231.14" y2="40.64" width="0.1524" layer="91"/>
<wire x1="231.14" y1="40.64" x2="231.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="231.14" y1="40.64" x2="233.68" y2="40.64" width="0.1524" layer="91"/>
<junction x="231.14" y="40.64"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="233.68" y1="40.64" x2="233.68" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="LD28" gate="G$1" pin="C"/>
<pinref part="LD26" gate="G$1" pin="A"/>
<wire x1="231.14" y1="17.78" x2="231.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="231.14" y1="15.24" x2="231.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="231.14" y1="15.24" x2="233.68" y2="15.24" width="0.1524" layer="91"/>
<junction x="231.14" y="15.24"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="233.68" y1="15.24" x2="233.68" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="LD23" gate="G$1" pin="C"/>
<pinref part="LD21" gate="G$1" pin="A"/>
<wire x1="144.78" y1="40.64" x2="144.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="144.78" y1="38.1" x2="144.78" y2="35.56" width="0.1524" layer="91"/>
<junction x="144.78" y="38.1"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="147.32" y1="38.1" x2="144.78" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="LD23" gate="G$1" pin="A"/>
<pinref part="LD21" gate="G$1" pin="C"/>
<wire x1="137.16" y1="40.64" x2="137.16" y2="35.56" width="0.1524" layer="91"/>
<pinref part="J4" gate="J" pin="6"/>
<wire x1="101.6" y1="33.02" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="33.02" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="4"/>
<wire x1="106.68" y1="35.56" x2="111.76" y2="35.56" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="111.76" y1="35.56" x2="127" y2="35.56" width="0.1524" layer="91"/>
<junction x="111.76" y="35.56"/>
<wire x1="137.16" y1="35.56" x2="127" y2="35.56" width="0.1524" layer="91"/>
<junction x="137.16" y="35.56"/>
<junction x="127" y="35.56"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="LD24" gate="G$1" pin="C"/>
<pinref part="LD22" gate="G$1" pin="A"/>
<wire x1="144.78" y1="30.48" x2="144.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="144.78" y1="27.94" x2="144.78" y2="25.4" width="0.1524" layer="91"/>
<junction x="144.78" y="27.94"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="147.32" y1="27.94" x2="144.78" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="LD24" gate="G$1" pin="A"/>
<pinref part="LD22" gate="G$1" pin="C"/>
<wire x1="137.16" y1="30.48" x2="137.16" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J4" gate="J" pin="5"/>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="101.6" y1="30.48" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="111.76" y1="30.48" x2="127" y2="30.48" width="0.1524" layer="91"/>
<junction x="111.76" y="30.48"/>
<wire x1="137.16" y1="30.48" x2="127" y2="30.48" width="0.1524" layer="91"/>
<junction x="137.16" y="30.48"/>
<junction x="127" y="30.48"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="LD17" gate="G$1" pin="A"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="119.38" y1="0" x2="121.92" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="LD18" gate="G$1" pin="A"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-2.54" x2="121.92" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="LD19" gate="G$1" pin="A"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-5.08" x2="121.92" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="LD20" gate="G$1" pin="A"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-7.62" x2="121.92" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
