# Auto Music Box Source

【編集中】

## 1. Install

```bash
$ cd ~
$ git clone https://gitlab.com/fablab-kannai/music-box.git
$ cd music-box
$ ./install.sh
```

## 2. サブディレクトリ、および、プロジェクト共通のファイル

* [webapp/](webapp/): Webアプリ(サーバー)
* [motor_drivers/](motor_drivers/): モータードライバ・ライブラリ
* プロジェクト共通のスクリプトファイルおよび設定ファイル
  + [install.sh](#1-install): インストールスクリプト
  + [boot-music-box.sh](#21-boot-music-boxsh): 起動スクリプト(後述)
  + [activate-do.sh](#22-activate-dosh): (後述)
  + crontab-sample: crontabのサンプル(自動起動用)

## 2.1 boot-music-box.sh

起動時に実行されるスクリプト。
サーバーなどの自動起動処理を行う。

OS起動時にcronシステムにより、自動的に実行される。


## 2.2 activate-do.sh

Usage
```
$ activate-do.sh VENV_DIR COMMAND_LINE
```

VENV_DIRの python3 Virtualenv を activateしてから、
COMMAND_LINE を実行する。

boot-music-box.shや、スクリプト内で、
Virtualenv の Pythonプログラムを実行するときに使う。
