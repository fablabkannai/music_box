#!/bin/sh
#
# Music Box boot up script
#
# (c) 2020 FabLab Kannai
#

VENV_DIR=env1

SRC_DIR=$HOME/env1/music_box/src

#
# start pigpiod
#
sudo pigpiod
sleep 5

#
# send IP address info to ..
#
### TBD ###

#
# activate Python3 Virtualenv
#
. $HOME/$VENV_DIR/bin/activate

#
# start web server
#

cd $SRC_DIR/webapp

./run.sh

#
# start piano hat
#

python2 piano_hat.py &

### TBD ###
