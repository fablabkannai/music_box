#!/bin/sh
#
# Music Box Install script
#
# (c) 2020 FabLab Kannai
#

VENV_DIR=env1-music-box
BOOT_SH=boot-music-box.sh
BIN_FILES="$BOOT_SH activate-do.sh"
CONF_FILES="motor_drivers/music-box-servo.conf"
CRONTAB_SAMPLE=crontab-sample

#
# set variables
#
DIR0=`dirname $0`
cd $DIR0
SRC_DIR=`pwd`
echo "SRC_DIR=$SRC_DIR"
DIR2=`dirname $SRC_DIR`
GIT_NAME=`basename $DIR2`
echo "GIT_NAME=$GIT_NAME"

#
# python3 Virtualenv
#
if [ ! -d $HOME/$VENV_DIR ]; then
    cd $HOME
    echo "[`pwd`]"
    python3 -m venv $VENV_DIR
fi

. $HOME/$VENV_DIR/bin/activate
echo "VIRTUAL_ENV=$VIRTUAL_ENV"

#
# make directories
#
if [ ! -d $HOME/bin ]; then
    mkdir -pv $HOME/bin
fi

#
# copy files
#
cd $HOME/bin
echo "[`pwd`]"
for f in $BIN_FILES; do
    cp -fv $SRC_DIR/$f .
done

cd $HOME
echo "[`pwd`]"
for f in $CONF_FILES; do
    cp -fv $SRC_DIR/$f .
done

#
# setup crontab for auto-boot
#
CRONTAB_TMP=/tmp/crontab.tmp
crontab -l > $CRONTAB_TMP
if ! grep $BOOT_SH $CRONTAB_TMP; then
    cat $SRC_DIR/$CRONTAB_SAMPLE >> $CRONTAB_TMP
    echo "[$CRONTAB_TMP]"; echo ":"; cat $CRONTAB_TMP | tail -3
    # crontab $CRONTAB_TMP
    echo "[crontab -l]"
    echo ":"
    crontab -l | tail -3
fi
# rm -fv $CRONTAB_TMP

#
# Install linux packages
#
### TBD ###

#
# Install python packages
#
### TBD ###

