# Motor Driver for Music Box

オルゴール用モーター・ドライバー

* 本体: Raspberry Pi

* ステッピング・モーター・ドライバー: DRV8835, ULN2003
* ステッピング・モーター: 28BYJ-48-5V

* サーボ・モーター・ドライバ: PCA9685
* サーボ・モーター: SG-90

* プログラミング言語: Python3
* GPIO制御ライブラリ: pigpio

## 0. TL;DR

```bash
$ cd ~
$ git clone https://gitlab.com/fablabkannai/music_box.git
$ python3 -m venv env1
$ source ~/env1/bin/activate
(env1)$ cd ~/music_box/src/motor_driver
(env1)$ cp music-box-servo.conf /home/pi
(env1)$ pip install -r requirements.txt
(env1)$ sudo pigpiod
(env1)$ ./MusicBoxRotationMotor.py -h
(env1)$ ./MusicBoxServo.py -h
```

## 0.1 Simple Usage

```python
"""
ライブラリのインポート
"""
from MusicBoxRotationMotor import MusicBoxRotationMotor
from MusicBoxServo import MusicBoxServo
 :
"""
初期化
"""
mtr = MusicBoxRotationMotor(5, 6, 12, 13) # GPIO pins
servo = MusicBoxServo()
 :
"""
演奏開始
"""
mtr.set_speed(9)  # 0 <= speed <= 10
 :
servo.tap([1, 3])     # list of channel numbers
servo.tap([0, 2, 4])  # list of channel numbers
 :
"""
演奏終了
"""
mtr.set_speed(0)  # 0:stop
 :
"""
プログラム終了時
"""
mtr.end()
servo.end()
```


## 1 Sotfware Architecture

呼び出し階層
```
   ---------------------------------------
  |            (Player App)               |
  |---------------------------------------|
  | MusicBoxRotationMotor | MusicBoxServo |
  |-----------------------|---------------|
  |       StepMtrTh       | ServoPCA9685  |
  |-----------------------|---------------|
  |        StepMtr        | pigpioPCA9685 |
  |---------------------------------------|
  |               pigpio                  |
   ---------------------------------------
```


## 2. MusicBoxRotationMotor class

回転モーター(ステッピング・モーター)制御クラス


### 2.1 Simple Usage

```python
from MusicBoxRotationMotor import MusicBoxRotationMotor
 :
mtr = MusicBoxRotationMotor(pin1, pin2, pin3, pin4)
 :
mtr.set_speed(9)  # 0 <= speed <= 10
 :
mtr.set_speed(0)  # 0:stop
 :
mtr.end()
```

### 2.2 MusicBoxRotationMotor.pyファイル

``MusicBoxRotationMotor.py``は、ライブラリ兼コマンド。
コマンドとして実行すると、
対話的にステッピングモーターの動作確認を行える。

使い方は以下を参照

```bash
$ ./MusicBoxRotationMotor.py -h
```


## 3. MusicBoxServo class

複数のサーボ・モーターを同時制御するクラス


### 3.1 Simple Usage

```python
from MusicBoxServo import MusicBoxServo
 :
servo = MusicBoxServo()
 :
servo.tap([0, 2, 4])  # list of channel numbers
 :
servo.end()
```


### 3.2 設定ファイル: music-box-servo.conf

各チャンネルのサーボ・モーターの ON/OFF の値を設定するファイル

* フルパス名: /home/pi/music-box-servo.conf
* 書式: テキスト(CSV)
* コメント: ``#``ではじまる行はコメントと見なされる
* スペース、空行は無視される

(ex)
```
# ch,on,off
00,1600,1200
01,1600,1200
02,1600,1200
03,1600,1200
04,1600,1200
05,1600,1200
06,1600,1200
07,1600,1200
08,1200,1600
09,1200,1600
10,1200,1600
11,1200,1600
12,1200,1600
13,1200,1600
14,1200,1600
```

### 3.3 MusicBoxServo.py

``MusicBoxServo.py``は、ライブラリ兼コマンド。
コマンドとして実行すると、
対話的にサーボモーターの動作確認を行える。

使い方は以下を参照

```bash
$ ./MusicBoxServo.py -h
```

## A. Reference

* [github:ytani01/StepperMotor](https://github.com/ytani01/StepperMotor/)
* [github:ytani01/ServoPCA9685](https://github.com/ytani01/ServoPCA9685/)
