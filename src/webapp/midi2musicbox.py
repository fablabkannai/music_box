import sys
from websocket import create_connection
from mido import MidiFile, tick2second
import time
import threading
import re
import json

ws_midi     = create_connection("ws://localhost:8080/midi")
ws_musicbox = create_connection("ws://localhost:8080/musicbox")

# 48 1 octave down
# 60 center
# 72 1 octave up
# 84 2 octave up

base_note = 60
speed_rate = 1.0

flag = False
flag_running = False
flag_playing = False
midifile = ''

def midi2musicbox():
    global midifile, ws_musicbox, base_note, flag, flag_running, flag_playing
    
    while flag_running:
        while flag == False or midifile == '':
            if flag_running == False:
                return
            time.sleep(1)
            

        mid = MidiFile("./uploads/" + midifile)
        flag_playing = True
        print('play started '+ midifile)
        
        tempo = 500000 #microseconds per beat(120BPM)

        print('Ticks per beat: {}'.format(mid.ticks_per_beat))

        for i, track in enumerate(mid.tracks):
            if flag == False:
                print('play stopped')                                    
                flag_playing = False
                break
            
            for msg in track:
                if flag == False:
                    flag_playing = False
                    print('play stopped')
                    break

                if msg.type == 'set_tempo':
                    tempo = msg.tempo
                    print('Tempo is changed to {}'.format(tempo))
                if msg.time > 0:
                    delta = tick2second(msg.time, mid.ticks_per_beat, tempo)
                else:
                    delta = 0

                if msg.type == 'note_on':
                    print('MIDI note:{}, time:{}, delta_sec:{}'.format(msg.note, msg.time, delta))
                    ws_musicbox.send(b'{}'.format(int(msg.note - base_note)))
                    if delta > 0.0 and delta < 10.0:
                        time.sleep(delta / speed_rate)
                    

if __name__ == '__main__':

    flag_running = True
    th = threading.Thread(target=midi2musicbox)
    th.start()

    try:
        while True:
            recv_cmd = ws_midi.recv()
            recv_json = json.loads(recv_cmd)
            cmd = recv_json['cmd'].encode()
            opt = recv_json['opt'].encode()
            print('cmd ={},opt={}'.format(cmd,opt))

            if cmd == 'play':
                while flag_playing == True:
                    flag = False
                    time.sleep(0.1)
                midifile = opt
                flag = True
            elif cmd == 'stop':
                midifile = ''
                flag = False
            elif cmd == 'octave_up':
                base_note = base_note - 12
            elif cmd == 'octave_down':
                base_note = base_note + 12                
            elif cmd == 'speed_up':
                speed_rate = speed_rate * 1.2
            elif cmd == 'speed_down':
                speed_rate = speed_rate / 1.2                
            elif cmd == 'start_motor':
                print('start_motor')
            elif cmd == 'stop_motor':
                print('stop_motor')
            else:
                print('error {} is not supported'.format(cmd))
                        
    except KeyboardInterrupt:
        print('keyboardinterrupt')
        flag = False
        flag_running = False
        ws_midi.close()
        ws_musicbox.close()
        print('terminate')
        th.join()

        

