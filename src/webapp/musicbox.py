import sys
from websocket import create_connection
import time
import glob
from MusicBoxRotationMotor import MusicBoxRotationMotor
from MusicBoxServo import MusicBoxServo

#conf_file = './music-box-servo.conf'
conf_file = '/home/pi/music-box-servo.conf'
on_interval  = 0.2
off_interval = 0.2
debug = True

servoch_map = [0, -1, 1, -1, 2, 3, -1, 4, -1, 5, -1, 6, 7, -1, 8, -1, 9, 10, -1, 11, -1, 12, -1, 13, 14]

def main():
    mtr = MusicBoxRotationMotor(5, 6, 12, 25) # GPIO pins
    servo = MusicBoxServo(conf_file=conf_file, on_interval=on_interval, off_interval=off_interval, debug=debug)
    
    ws_musicbox = create_connection("ws://localhost:8080/musicbox")
    mtr.set_speed(10)

    try:    
        while True:
            key_id = int(ws_musicbox.recv())
            servo_ch = servoch_map[key_id]
            print(servo_ch)
            if servo_ch >= 0:
                # print('Playing Sound: {}'.format(files[servo_ch]))
                print('Playing Sound: {}'.format(servo_ch))
                servo.tap([servo_ch])
            else:
                print('not supported')
    except KeyboardInterrupt:
        mtr.set_speed(0)
        ws_musicbox.close()
        
        
if __name__ == '__main__':
    main()
