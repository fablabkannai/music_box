#!/usr/bin/env python

import glob
import os
import re
import signal
from sys import exit
from websocket import create_connection

# 48 1 octave down
# 60 center
# 72 1 octave up
# 84 2 octave up
base_note = 60

ws_musicbox = create_connection("ws://localhost:8080/musicbox")

try:
    import pygame
except ImportError:
    exit("This script requires the pygame module\nInstall with: sudo pip install pygame")

import pianohat


#BANK = os.path.join(os.path.dirname(__file__), "sounds")
BANK = os.path.join(os.path.dirname(__file__), "piano")

print("""
This example gives you a simple, ready-to-play instrument which uses .wav files.

For it to work, you must place directories of wav files in:

{}

We've supplied a piano and drums for you to get started with!

Press CTRL+C to exit.
""".format(BANK))

NOTE_OFFSET = 3
FILETYPES = ['*.wav', '*.ogg']
samples = []
files = []
octave = 0
octaves = 0

pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.mixer.init()
pygame.mixer.set_num_channels(32)

#patches = glob.glob(os.path.join(BANK, '*'))
patches = [BANK]
patch_index = 0

if len(patches) == 0:
    exit("Couldn't find any .wav files in: {}".format(BANK))


def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower() for text in re.split(_nsre, s)]


def load_samples(patch):
    global samples, files, octaves, octave
    files = []
    print('Loading Samples from: {}'.format(patch))
    for filetype in FILETYPES:
        files.extend(glob.glob(os.path.join(patch, filetype)))
    files.sort(key=natural_sort_key)
    octaves = len(files) / 12
#    print('play', files)
#    samples = [pygame.mixer.Sound(sample) for sample in files]
#    octave = int(octaves / 2)
    octave = 1


pianohat.auto_leds(True)


def handle_note(channel, pressed):
    if pressed:
        channel = channel + (12 * octave)
        print(octave)
        print('channel: {}'.format(channel))
        ws_musicbox.send(b'{}'.format(int(channel)))    

def handle_instrument(channel, pressed):
    global patch_index
    if pressed:
        patch_index += 1
        patch_index %= len(patches)
        print('Selecting Patch: {}'.format(patches[patch_index]))
        load_samples(patches[patch_index])


def handle_octave_up(channel, pressed):
    global octave
    if pressed and octave < octaves:
        octave += 1
        print('Selected Octave: {}'.format(octave))


def handle_octave_down(channel, pressed):
    global octave
    if pressed and octave > 0:
        octave -= 1
        print('Selected Octave: {}'.format(octave))


pianohat.on_note(handle_note)
pianohat.on_octave_up(handle_octave_up)
pianohat.on_octave_down(handle_octave_down)
pianohat.on_instrument(handle_instrument)

load_samples(patches[patch_index])

signal.pause()
