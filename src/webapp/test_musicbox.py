import sys
from websocket import create_connection
from mido import MidiFile, tick2second
import time

ws_musicbox = create_connection("ws://localhost:8080/play_musicbox")

# 48 1 octave down
# 60 center
# 72 1 octave up
# 84 2 octave up

while True:
    ws_musicbox.send(b'0')
    time.sleep(0.2)
    ws_musicbox.send(b'2')
    time.sleep(0.2)
    ws_musicbox.send(b'4')
    time.sleep(0.2)
    ws_musicbox.send(b'5')
    time.sleep(0.2)
    ws_musicbox.send(b'7')
    time.sleep(0.2)
    ws_musicbox.send(b'9')
    time.sleep(0.2)
    ws_musicbox.send(b'11')
    time.sleep(0.2)
    ws_musicbox.send(b'12')
    time.sleep(0.2)
