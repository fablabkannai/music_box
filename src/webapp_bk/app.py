import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.netutil
import urllib
import sqlite3
import os
import re
import json

num_keys = 15
#conf_file = './music-box-servo.conf'
conf_file = '/home/pi/music-box-servo.conf'

cl_midi = []
cl_musicbox = []
cl_params = []
 
class WebSocketHandler_MIDI(tornado.websocket.WebSocketHandler):
    def open(self):
        if self not in cl_midi:
            cl_midi.append(self)

    def on_message(self, message):
        print(message)
        for client in cl_midi:
            client.write_message(message)
            
    def on_close(self):
        if self in cl_midi:
            cl_midi.remove(self)

class WebSocketHandler_MusicBox(tornado.websocket.WebSocketHandler):
    def open(self):
        if self not in cl_musicbox:
            cl_musicbox.append(self)

    def on_message(self, message):
        print(message)
        for client in cl_musicbox:
            client.write_message(message)
            
    def on_close(self):
        if self in cl_musicbox:
            cl_musicbox.remove(self)

class WebSocketHandler_Params(tornado.websocket.WebSocketHandler):
    def open(self):
        param_list = load_conf(conf_file)

        if not param_list:
            response = json.dumps(
                {"cmd": "none"}
            )
        else:
            param_key_value = {}                
            for param_ in param_list:
                param_key_value['push_' + str(param_[0])] = param_[1]
                param_key_value['pull_' + str(param_[0])] = param_[2]
            val =  {"cmd": "load", "opt": param_key_value}
            response = json.dumps(val)
                
        print(response)
        self.write_message(response)
        
#        cursor.execute("SELECT * FROM params")                    
#        param_keys = cursor.fetchall()

#        if not param_keys:
#            response = json.dumps(
#                {"cmd": "none"}
#            )
#        else:
#            param_key_value = {}                
#            for param_key in param_keys:
#                param_key_value[param_key[0]] = param_key[1]
#            val =  {"cmd": "load", "opt": param_key_value}                
#            response = json.dumps(val)
                
#        print(response)
#        self.write_message(response)

    def on_message(self, message):
        recv_json = json.loads(message)
        cmd = recv_json['cmd'].encode()
        
        if cmd == 'load':
#            cursor.execute("SELECT * FROM params")                    
#            param_keys = cursor.fetchall()

#            if not param_keys:
#                response = json.dumps(
#                    {"cmd": "none"}
#                    )
#            else:
#                param_key_value = {}                
#                for param_key in param_keys:
#                    param_key_value[param_key[0]] = param_key[1]
#                val =  {"cmd": "load", "opt": param_key_value}                
#                response = json.dumps(val)

            param_list = load_conf(conf_file)

            if not param_list:
                response = json.dumps(
                    {"cmd": "none"}
                    )
            else:
                param_key_value = {}                
                for param_ in param_list:
                    param_key_value['push_' + str(param_[0])] = param_[1]
                    param_key_value['pull_' + str(param_[0])] = param_[2]
                val =  {"cmd": "load", "opt": param_key_value}
                response = json.dumps(val)
                
            print(response)
            self.write_message(response)
        elif cmd == 'save':
            params = recv_json['opt']
            param_list = []

            for i in range(num_keys):
                param_list.append([i, params[i]['push'], params[i]['pull']])
            
            save_conf(param_list, conf_file)
            

#            cursor.execute("SELECT * FROM params")                    
#            param_keys = cursor.fetchall()

#            #for empty
#            if not param_keys:
#                for i in range(num_keys):
#                    cursor.execute("INSERT INTO params VALUES ('push_" + str(i) + "','" + params[i]['push']  + "')")
#                    cursor.execute("INSERT INTO params VALUES ('pull_" + str(i) + "','" + params[i]['pull']  + "')")
#                    cursor.execute("INSERT INTO params VALUES ('interval_" + str(i) + "','" + params[i]['interval']  + "')")       
#            else:
#                for i in range(num_keys):                    
#                    cursor.execute("UPDATE params SET param_value = " + params[i]['push'] + " WHERE param_key = 'push_" + str(i) + "'" )
#                    cursor.execute("UPDATE params SET param_value = " + params[i]['pull'] + " WHERE param_key = 'pull_" + str(i) + "'" )
#                    cursor.execute("UPDATE params SET param_value = " + params[i]['interval'] + " WHERE param_key = 'interval_" + str(i) + "'" )

#            connection.commit()                
#            cursor.execute("SELECT * FROM params")                    
#            filelist = cursor.fetchall()
#            key_values = [[str(x[0]),str(x[1])] for x in filelist]
#            print(filelist)

#        elif cmd == 'reset':
#            for i in range(num_keys):            
#                cursor.execute("DELETE FROM params WHERE param_key = 'push_" + str(i) + "'" )
#                cursor.execute("DELETE FROM params WHERE param_key = 'pull_" + str(i) + "'" )
#                cursor.execute("DELETE FROM params WHERE param_key = 'interval_" + str(i) + "'" )
#            connection.commit()                            
#            cursor.execute("SELECT * FROM params")                    
#            filelist = cursor.fetchall()
#            key_values = [[str(x[0]),str(x[1])] for x in filelist]
#            print(filelist)

            
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        cursor.execute("SELECT * FROM midifiles")
        filelist = cursor.fetchall()
        filelist = [str(x[0]) for x in filelist]
        self.render('index.html', filelist=filelist, host=self.request.host)
 
class UploadHandler(tornado.web.RequestHandler):
    def post(self):
        if  self.request.files is not None:
            if not os.path.exists('uploads'):
                os.mkdir('uploads')
            uploadFile = self.request.files['file1'][0]
            output_file = open("uploads/" + uploadFile['filename'], 'wb')
            output_file.write(uploadFile['body'])
            try:
                cursor.execute("INSERT INTO midifiles VALUES ('" + uploadFile['filename'] + "')")
                cursor.execute("SELECT * FROM midifiles")
                connection.commit()                
                filelist = cursor.fetchall()
                filelist = [str(x[0]) for x in filelist]        
            except sqlite3.Error as e:
                print('sqlite3.Error occurred:', e.args[0])

            self.redirect('/')
                

class DeleteHandler(tornado.web.RequestHandler):
    def get(self):
        filename = self.get_argument('filename')
        print(filename)
        try:
            cursor.execute("DELETE FROM midifiles WHERE filename = '" + filename + "'")
            connection.commit()                
        except sqlite3.Error as e:
            print('sqlite3.Error occurred:', e.args[0])
        
                
BASE_DIR = os.path.dirname(__file__)
                
application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/upload", UploadHandler),
    (r"/delete", DeleteHandler),
    (r"/midi", WebSocketHandler_MIDI),
    (r"/musicbox", WebSocketHandler_MusicBox),
    (r"/params", WebSocketHandler_Params),    
],
                                      template_path=os.path.join(BASE_DIR, 'templates'),
                                      static_path=os.path.join(BASE_DIR, 'static'),
)
 
dbpath = 'DB.sqlite'
connection = sqlite3.connect(dbpath)

def load_conf(conf_file=None):
    param_list = []

    if conf_file is None:
        conf_file = 'music-box-servo.conf'

    with open(conf_file) as f:
        lines = f.readlines()

    for line in lines:
        col = line.replace(' ', '').rstrip('\n').split(',')

        if len(col) != 3:
            continue

        if col[0][0] == '#':
            continue

        [ch, on, off] = [int(s) for s in col]
        param_list.append([ch, on, off])
        
    return(param_list)

def save_conf(param_list, conf_file=None):

    if conf_file is None:
        conf_file = 'music-box-servo.conf'

    with open(conf_file,'w') as f:
        f.write('# ch,on,off\n')
        for param_ in param_list:
            f.write(str(param_[0]) + ',' + str(param_[1]) + ',' + str(param_[2]) + '\n')


if __name__ == "__main__":
    application.listen(8080)
    cursor = connection.cursor()

    try:
        cursor.execute("CREATE TABLE IF NOT EXISTS midifiles (filename TEXT UNIQUE)")
        cursor.execute("SELECT * FROM midifiles")
        connection.commit()
        filelist = cursor.fetchall()
        filelist = [str(x[0]) for x in filelist]
        print(filelist)

#        cursor.execute("CREATE TABLE IF NOT EXISTS params (param_key TEXT UNIQUE, param_value TEXT)")
#        cursor.execute("SELECT * FROM params")        
#        connection.commit()
#        filelist = cursor.fetchall()
#        key_values = [[str(x[0]),str(x[1])] for x in filelist]
#        print(filelist)

        param_list = load_conf(conf_file)
        print(param_list)
        
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
    except sqlite3.Error as e:
        print('sqlite3.Error occurred:', e.args[0])
    
    connection.commit()
    connection.close()
