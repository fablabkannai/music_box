import sys
from websocket import create_connection
from mido import MidiFile, tick2second
import time
import pygame
import glob


if __name__ == '__main__':
    ws_musicbox = create_connection("ws://localhost:8080/musicbox")

    pygame.mixer.pre_init(44100, -16, 1, 512)
    pygame.mixer.init()
    pygame.mixer.set_num_channels(32)
    
    files = sorted(glob.glob('piano/*.wav'))
    sounds = [pygame.mixer.Sound(x) for x in files]
    
    try:    
        while True:
            sound_id = int(ws_musicbox.recv()) + 40
            print(sound_id)
            if sound_id > 0 and sound_id < len(files):
                print('Playing Sound: {}'.format(files[sound_id]))
                sounds[sound_id].play(loops=0)
            else:
                print('error sound_id = {}'.format(sound_id))
    except KeyboardInterrupt:                
        ws_musicbox.close()
        

