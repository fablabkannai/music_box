#!/bin/bash

WEBAPP_HOME=~/MachineBuilding/music_box/src/webapp
cd $WEBAPP_HOME
python3 -m venv env1
source ~/env1/bin/activate
pip install -r requirements.txt
sudo pigpiod
cd -

ps ax |grep 'python app.py'|grep -v grep|awk '{ print "kill -9", $1 }' |sh
ps ax |grep 'python midi2musicbox.py'|grep -v grep|awk '{ print "kill -9", $1 }' |sh
ps ax |grep 'python musicbox.py'|grep -v grep|awk '{ print "kill -9", $1 }' |sh
ps ax |grep 'python musicbox_dummy.py'|grep -v grep|awk '{ print "kill -9", $1 }' |sh
ps ax |grep 'python piano_hat.py'|grep -v grep|awk '{ print "kill -9", $1 }' |sh

python app.py &> logs/app.log &
sleep 5
python midi2musicbox.py &> logs/midi2musicbox.log &
python musicbox.py &> logs/musicbox.log &
#python musicbox_dummy.py &> logs/musicbox.log &
python piano_hat.py &> logs/piano_hat.log &

